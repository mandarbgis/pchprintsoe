/*
 *
 *  *  PCHPrintSOE - Advanced printing SOE for ArcGIS Server
 *  *  Copyright (C) 2010-2012 Tom Schuller
 *  *
 *  *  This program is free software: you can redistribute it and/or modify
 *  *  it under the terms of the GNU Lesser General Public License as published by
 *  *  the Free Software Foundation, either version 3 of the License, or
 *  *  (at your option) any later version.
 *  *
 *  *  This program is distributed in the hope that it will be useful,
 *  *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  *  GNU Lesser General Public License for more details.
 *  *
 *  *  You should have received a copy of the GNU Lesser General Public License
 *  *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package lu.etat.pch.gis.beans.height;

import com.esri.arcgis.geometry.IPoint;
import com.esri.arcgis.interop.AutomationException;

import java.io.IOException;

/**
 * Created by IntelliJ IDEA.
 * User: schullto
 * Date: 17/10/11
 * Time: 09:53
 */
public class HeightResultBean {
    private double x;
    private double y;
    private double z;

    public HeightResultBean(double z) {
        this.z = z;
    }

    public HeightResultBean(double x, double y, double z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public HeightResultBean(IPoint pt) {
        try {
            this.x = pt.getX();
            this.y = pt.getY();
            this.z = pt.getZ();
        } catch (AutomationException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
