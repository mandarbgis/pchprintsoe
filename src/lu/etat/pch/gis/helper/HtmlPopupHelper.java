/*
 *
 *  *  PCHPrintSOE - Advanced printing SOE for ArcGIS Server
 *  *  Copyright (C) 2010-2012 Tom Schuller
 *  *
 *  *  This program is free software: you can redistribute it and/or modify
 *  *  it under the terms of the GNU Lesser General Public License as published by
 *  *  the Free Software Foundation, either version 3 of the License, or
 *  *  (at your option) any later version.
 *  *
 *  *  This program is distributed in the hope that it will be useful,
 *  *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  *  GNU Lesser General Public License for more details.
 *  *
 *  *  You should have received a copy of the GNU Lesser General Public License
 *  *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package lu.etat.pch.gis.helper;

import com.esri.arcgis.carto.FeatureLayer;
import com.esri.arcgis.geodatabase.*;
import com.esri.arcgis.system.ISet;
import com.esri.arcgis.system.MemoryBlobStream;

import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: schullto
 * Date: May 19, 2010
 * Time: 9:53:16 PM
 */
public class HtmlPopupHelper {
    private String calculateHtmOutputForRelatedFeatures(FeatureLayer featureLayer, IFeature feature, File htmlPopupOutputDirectory) throws IOException {
        boolean realtionsFound = false;
        FeatureClass featureClass = new FeatureClass(featureLayer.getFeatureClass());
        List<IRelationshipClass> relationshipClassList = new ArrayList<IRelationshipClass>();
        IRelationshipClass tmpRelationshipClass;
        IEnumRelationshipClass enumFLRelationshipClass = featureLayer.getRelationshipClasses();
        while ((tmpRelationshipClass = enumFLRelationshipClass.next()) != null) {
            relationshipClassList.add(tmpRelationshipClass);
        }
        IEnumRelationshipClass enumFCRelationshipClass = featureClass.getRelationshipClasses(esriRelRole.esriRelRoleAny);
        while ((tmpRelationshipClass = enumFCRelationshipClass.next()) != null) {
            relationshipClassList.add(tmpRelationshipClass);
        }
        if (relationshipClassList.size() == 0) return null;
        StringBuffer retBuffer = new StringBuffer();
        retBuffer.append("<Relations>");
        for (IRelationshipClass relationshipClass : relationshipClassList) {
            realtionsFound = true;
            retBuffer.append("<Relation name=\"" + relationshipClass.getForwardPathLabel() + "\">");
            ISet relateSet = relationshipClass.getObjectsRelatedToObject(feature);
            Object rbuf = relateSet.next();
            while (rbuf != null) {
                retBuffer.append("<RelFeature>");
                esri_Object esriObj = (esri_Object) rbuf;
                IFields fields = esriObj.getFields();
                retBuffer.append("<Fields>");
                for (int fieldIndex = 0; fieldIndex < fields.getFieldCount(); fieldIndex++) {
                    Field field = (Field) fields.getField(fieldIndex);
                    String fieldName = field.getName();
                    Object value = esriObj.getValue(fieldIndex);
                    if (value instanceof MemoryBlobStream) {
                        int contentTypeFieldIndex = fields.findField("CONTENT_TYPE");
                        String fileExt = ".bin";
                        if (contentTypeFieldIndex >= 0) {
                            Object contentType = esriObj.getValue(contentTypeFieldIndex);
                            System.out.println("contentType = " + contentType);
                            if (contentType.equals("image/jpeg")) fileExt = ".jpg";
                        }
                        MemoryBlobStream blobStream = (MemoryBlobStream) value;
                        File tmpFile = File.createTempFile("BLOB_layer_" + featureLayer.getName() + "_oid_" + feature.getOID() + "_" + fieldName + "_", fileExt, htmlPopupOutputDirectory);
                        String blobFileName = tmpFile.getAbsolutePath();
                        blobStream.saveToFile(tmpFile.getAbsolutePath());
                        retBuffer.append("<Field>")
                                .append("<FieldName>").append(fieldName).append("</FieldName>")
                                .append("<FieldType>link</FieldType>")
                                .append("<FieldValue>").append(blobFileName).append("</FieldValue>")
                                .append("</Field>");

                    } else {
                        retBuffer.append("<Field>")
                                .append("<FieldName>").append(fieldName).append("</FieldName>")
                                .append("<FieldValue>").append(value).append("</FieldValue>")
                                .append("</Field>");
                    }
                }
                retBuffer.append("</Fields>");
                retBuffer.append("</RelFeature>");
                rbuf = relateSet.next();
            }
            retBuffer.append("</Relation>");
        }
        retBuffer.append("</Relations>");
        if (realtionsFound)
            return retBuffer.toString();
        else return null;
    }

    public String calculateHtmlOutputForFeature(FeatureLayer featureLayer, IFeature feature, File htmlPopupOutputDirectory) throws IOException {
        String featureHtmlCode = "";
        System.out.println("featureLayer.isHTMLPopupEnabled() = " + featureLayer.isHTMLPopupEnabled());
        String xmlContent = null;
        String xslContent = null;
        if (featureLayer.isHTMLPopupEnabled()) {
            xmlContent = featureLayer.getHTMLOutput(feature);

            String xmlRelatesContent = calculateHtmOutputForRelatedFeatures(featureLayer, feature, htmlPopupOutputDirectory);
            //System.out.println("xmlRelatesContent = " + xmlRelatesContent);
            if (xmlRelatesContent != null) {
                xmlContent = xmlContent.substring(0, xmlContent.lastIndexOf("</FieldsDoc>")) + xmlRelatesContent + "</FieldsDoc>";
            }
            xslContent = featureLayer.getHTMLXSLStylesheet();
        }
        if (xmlContent != null) {
            if (xslContent != null) {
                TransformerFactory transformerFactory = TransformerFactory.newInstance();
                try {
                    Transformer transformer = transformerFactory.newTransformer(new StreamSource(new StringReader(xslContent)));
                    ByteArrayOutputStream bos = new ByteArrayOutputStream();
                    transformer.transform(new StreamSource(new StringReader(xmlContent)), new StreamResult(bos));
                    bos.close();
                    featureHtmlCode = new String(bos.toByteArray(), "utf-8");
                } catch (TransformerConfigurationException e) {
                    e.printStackTrace();
                } catch (TransformerException e) {
                    e.printStackTrace();
                }
            } else {
                return xmlContent;
            }
        }
        return featureHtmlCode;
    }
}
