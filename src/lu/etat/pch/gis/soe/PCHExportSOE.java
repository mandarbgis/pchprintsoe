/*
 *
 *  *  PCHPrintSOE - Advanced printing SOE for ArcGIS Server
 *  *  Copyright (C) 2010-2012 Tom Schuller
 *  *
 *  *  This program is free software: you can redistribute it and/or modify
 *  *  it under the terms of the GNU Lesser General Public License as published by
 *  *  the Free Software Foundation, either version 3 of the License, or
 *  *  (at your option) any later version.
 *  *
 *  *  This program is distributed in the hope that it will be useful,
 *  *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  *  GNU Lesser General Public License for more details.
 *  *
 *  *  You should have received a copy of the GNU Lesser General Public License
 *  *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package lu.etat.pch.gis.soe;

import com.esri.arcgis.carto.IMapServerDataAccess;
import com.esri.arcgis.carto.Map;
import com.esri.arcgis.carto.MapServer;
import com.esri.arcgis.geometry.IPoint;
import com.esri.arcgis.geometry.IPolyline;
import com.esri.arcgis.geometry.Point;
import com.esri.arcgis.geometry.Polyline;
import com.esri.arcgis.interop.AutomationException;
import com.esri.arcgis.interop.extn.ArcGISExtension;
import com.esri.arcgis.interop.extn.ServerObjectExtProperties;
import com.esri.arcgis.server.IServerObjectExtension;
import com.esri.arcgis.server.IServerObjectHelper;
import com.esri.arcgis.server.json.JSONArray;
import com.esri.arcgis.server.json.JSONException;
import com.esri.arcgis.server.json.JSONObject;
import com.esri.arcgis.server.json.JSONTokener;
import com.esri.arcgis.system.IObjectConstruct;
import com.esri.arcgis.system.IPropertySet;
import com.esri.arcgis.system.IRESTRequestHandler;
import com.esri.arcgis.system.ServerUtilities;
import lu.etat.pch.gis.beans.height.HeightResultBean;
import lu.etat.pch.gis.beans.htmlPopup.HtmlPopupQueryResult;
import lu.etat.pch.gis.beans.pk.PKResultBean;
import lu.etat.pch.gis.soe.tasks.*;
import lu.etat.pch.gis.soeClients.IPCHExportSOE;
import lu.etat.pch.gis.utils.RESTUtils;
import lu.etat.pch.gis.utils.SOELogger;
import lu.etat.pch.gis.utils.json.geometry.AgsJsonEnvelope;
import lu.etat.pch.gis.utils.json.geometry.AgsJsonGeometry;
import lu.etat.pch.gis.utils.json.geometry.AgsJsonPolyline;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

@ArcGISExtension
@ServerObjectExtProperties(
        displayName = "PCHExportSOE",
        description = "PCHExportSOE",
        allSOAPCapabilities = {
                "getPKsByCoord", "getPointForPK", "getLineForPKs",
                "getHeightForPoint", "getHeightForLine"
        }
//        , supportsMSD = true
)
public class PCHExportSOE implements IServerObjectExtension, IPCHExportSOE, IRESTRequestHandler, IObjectConstruct {
    private static final String TAG = "PCHExportSOE";
    private IServerObjectHelper soHelper;
    private SOELogger logger;
    private MapServer mapServer = null;

    private CalculatePKTask calculatePkTask;
    private HeightTask heightTask;
    private LegendTask legendTask;
    private MetadataTask metadataTask;
    private HtmlPopupQueryTask htmlPopupQueryTask;
    private IMapServerDataAccess mapServerDataAccess;

    public void init(IServerObjectHelper soh) throws IOException {
        this.soHelper = soh;
        this.logger = new SOELogger(ServerUtilities.getServerLogger(), 8000);
        logger.debug(TAG, "init....");
        this.mapServer = (MapServer) soHelper.getServerObject();
        this.mapServerDataAccess = (IMapServerDataAccess) soh.getServerObject();
        logger.debug(TAG, "init.finished");
    }

    public void shutdown() throws IOException {
        logger.debug(TAG, "shutdown...");
        this.soHelper = null;
        this.mapServer = null;
        logger.debug(TAG, "shutdown.finished");
    }

    public void construct(IPropertySet propertySet) throws IOException {
        logger.debug(TAG, "construct...");
        calculatePkTask = new CalculatePKTask(logger, mapServer, mapServer.getConfigurationName());
        heightTask = new HeightTask(logger, mapServer, mapServer.getConfigurationName());
        legendTask = new LegendTask(logger, mapServer, mapServer.getConfigurationName());
        metadataTask = new MetadataTask(logger, mapServer, mapServer.getConfigurationName());
        htmlPopupQueryTask = new HtmlPopupQueryTask(logger, mapServer, mapServer.getConfigurationName());
        htmlPopupQueryTask.cleanUpHtmlPopupOutputs();
        logger.debug(TAG, "construct.finished");
    }


    @Override
    public String getSchema() throws IOException {
        logger.debug(TAG, "getSchema()...");
        try {
            JSONObject pchExportSOE = RESTUtils.createResource("PCHExportSOE", "This is my first SOE", false);
            JSONArray operationArray = new JSONArray();
            operationArray.put(RESTUtils.createOperation("exportLegend", "width,height,dpi", "json,html"));
            operationArray.put(RESTUtils.createOperation("getPKsByCoord", "x,y,wkid,tolerance,maxRecords", "json,html,htm"));
            operationArray.put(RESTUtils.createOperation("getPointForPK", "route,pk", "json,html"));
            operationArray.put(RESTUtils.createOperation("queryHtmlPopup", new String[]{"layerId", "text", "geometry", "objectIds", "whereClause", "outFields", "returnGeometry"}, "json,html"));

            operationArray.put(RESTUtils.createOperation("getLineForPKs", "route,pkFrom,pkTo", "json,html"));
            operationArray.put(RESTUtils.createOperation("getHeightForPoint", "x,y", "json,html"));
            operationArray.put(RESTUtils.createOperation("getHeightForLine", "geometry", "json,html"));
            operationArray.put(RESTUtils.createOperation("getMetadataByID", "layer", "json,html"));
            pchExportSOE.put("operations", operationArray);

            JSONArray subResourceArray = new JSONArray();
            subResourceArray.put(RESTUtils.createResource("exportLegends", "Export legends into d:/temp/layers", false));
            pchExportSOE.put("resources", subResourceArray);
            logger.debug(TAG, "construct.finished");
            return pchExportSOE.toString();
        } catch (JSONException e) {
            logger.error(TAG, "PCHExportSOE.construct.JSONException", e);
        }
        return null;
    }

    public byte[] handleRESTRequest(String capabilities, String resourceName, String operationName, String operationInput, String outputFormat, String requestProperties, String[] responseProperties) throws IOException, AutomationException {
        String correctedOutputFormat = RESTUtils.getCorrectedOutputFormat(operationInput);
        if (correctedOutputFormat != null) outputFormat = correctedOutputFormat;
        try {
            logger.debug(TAG, "In handleRESTRequest(): " + ";\ncapabilities: " + capabilities + ";\nresourceName: " + resourceName + ";\noperationName: " + operationName + ";\noperationName length: " + operationName.length() + ";\noperationInput: " + operationInput + ";\noutputFormat: " + outputFormat
                    + ";\nresponseProperties length: " + responseProperties.length);

            if (operationName.equalsIgnoreCase("getPKsByCoord")) {
                String[] values = RESTUtils.getParameterValueFromOperationInput(operationInput, new String[]{"x", "y", "wkid", "tolerance", "maxRecords"});
                logger.warning(TAG, "Operation 'getPKsByCoord' with params: " + Arrays.toString(values) + " ...");
                try {
                    double xCoord = Double.parseDouble(values[0]);
                    double yCoord = Double.parseDouble(values[1]);
                    String wkid = values[2];
                    double tolerance = Double.parseDouble(values[3]);
                    int maxRecords = 50;
                    if (values[4] != null) {
                        maxRecords = Integer.parseInt(values[4]);
                    }
                    List<PKResultBean> retString = calculatePkTask.getPKsByCoordArray(xCoord, yCoord, wkid, tolerance, maxRecords);
                    logger.debug(TAG, "handleRESTRequest.format: >" + outputFormat + "<");
                    if (outputFormat.equalsIgnoreCase("htm")) {
                        StringBuffer sb = new StringBuffer();
                        sb.append("<ul>");
                        for (PKResultBean pkResultBean : retString) {
                            sb.append("<li><b>").append(pkResultBean.getRoute()).append("</b> PK ").append((int) pkResultBean.getPk()).append("m</li>");
                        }
                        sb.append("</ul>");
                        JSONObject answerObject = new JSONObject();
                        logger.warning(TAG, "getPKsByCoord.HTML: " + sb);
                        answerObject.put("html", sb.toString());
                        return answerObject.toString().getBytes();
                    } else {
                        JSONObject answerObject = new JSONObject();
                        answerObject.put("name", operationName);
                        answerObject.put("valid", "ok");
                        JSONArray array = new JSONArray();
                        for (PKResultBean pkResultBean : retString) {
                            array.put(pkResultBean.toJSON());
                        }
                        answerObject.put("results", array);
                        return answerObject.toString().getBytes();
                    }
                } catch (NumberFormatException e) {
                    logger.error(TAG, "PChExportSOE.handleRESTRequest.NumberFormatException", e);
                    return RESTUtils.sendError(0, "Operation " + "\"" + operationName + "\" NumberFormatException->Double",
                            values).getBytes();
                } catch (JSONException e) {
                    logger.error(TAG, "PChExportSOE.handleRESTRequest.JSONException", e);
                    return RESTUtils.sendError(0, "Operation " + "\"" + operationName + "\" JSONException",
                            new String[]{"no details", " specified"}).getBytes();
                }
            } else if (operationName.equalsIgnoreCase("queryHtmlPopup")) {
                String[] values = RESTUtils.getParameterValueFromOperationInput(operationInput, new String[]{"layerId", "text", "geometry", "objectIds", "whereClause", "outFields", "returnGeometry"});
                try {
                    if (values[0] != null) htmlPopupQueryTask.setLayerId(Integer.parseInt(values[0]));
                    htmlPopupQueryTask.setText(values[1]);
                    if (values[2] != null) {
                        JSONObject geomElem = new JSONObject(new JSONTokener(values[2]));
                        htmlPopupQueryTask.setGeometry(AgsJsonEnvelope.convertJsonToAgsJsonGeom(logger, geomElem).toArcObject());
                    }
                    htmlPopupQueryTask.setObjectIds(values[3]);
                    htmlPopupQueryTask.setWhereClause(values[4]);
                    htmlPopupQueryTask.setOutFields(values[5]);
                    //htmlPopupQueryTask.setReturnGeometry(values[6].equalsIgnoreCase("true"));
                    HtmlPopupQueryResult htmlPopupQueryResult = htmlPopupQueryTask.queryHtmlPopup();
                    return htmlPopupQueryResult.toJSON().toString().getBytes();
                } catch (JSONException e) {
                    logger.error(TAG, "PChExportSOE.handleRESTRequest.JSONException", e);
                    return RESTUtils.sendError(0, "Operation " + "\"" + operationName + "\" JSONException",
                            new String[]{"no details", " specified"}).getBytes();
                } catch (Exception e) {
                    logger.error(TAG, "PChExportSOE.handleRESTRequest.Exception", e);
                    return RESTUtils.sendError(0, "Operation " + "\"" + operationName + "\" Exception",
                            new String[]{"no details", " specified"}).getBytes();
                }
            } else if (operationName.equalsIgnoreCase("getPointForPK")) {
                String[] values = RESTUtils.getParameterValueFromOperationInput(operationInput, new String[]{"route", "pk"});
                logger.warning(TAG, "Operation '" + operationName + "' with params: " + Arrays.toString(values) + " ...");
                String route = values[0];
                double pk = Double.parseDouble(values[1]);
                Point point = calculatePkTask.getPointForPK(route, pk);
                try {
                    JSONObject answerObject = new JSONObject();
                    answerObject.put("name", operationName);
                    answerObject.put("route", route);
                    answerObject.put("pkFrom", pk);
                    answerObject.put("point", AgsJsonGeometry.convertGeomToJson(logger, point));
                    return answerObject.toString().getBytes();
                } catch (JSONException e) {
                    logger.error(TAG, "PChExportSOE.handleRESTRequest.JSONException", e);
                    return RESTUtils.sendError(0, "Operation " + "\"" + operationName + "\" JSONException",
                            new String[]{"no details", " specified"}).getBytes();
                }
            } else if (operationName.equalsIgnoreCase("getLineForPKs")) {
                String[] values = RESTUtils.getParameterValueFromOperationInput(operationInput, new String[]{"route", "pkFrom", "pkTo"});
                logger.warning(TAG, "Operation '" + operationName + "' with params: " + Arrays.toString(values) + " ...");
                String route = values[0];
                double pkFrom = Double.parseDouble(values[1]);
                double pkTo = Double.parseDouble(values[2]);
                Polyline polyline = calculatePkTask.getLineForPK(route, pkFrom, pkTo);
                try {
                    JSONObject answerObject = new JSONObject();
                    answerObject.put("name", operationName);
                    answerObject.put("route", route);
                    answerObject.put("pkFrom", pkFrom);
                    answerObject.put("pkTo", pkTo);
                    answerObject.put("line", AgsJsonGeometry.convertGeomToJson(logger, polyline));
                    return answerObject.toString().getBytes();
                } catch (JSONException e) {
                    logger.error(TAG, "PChExportSOE.handleRESTRequest.JSONException", e);
                    return RESTUtils.sendError(0, "Operation " + "\"" + operationName + "\" JSONException",
                            new String[]{"no details", " specified"}).getBytes();
                }
            } else if (operationName.equalsIgnoreCase("getHeightForPoint")) {
                String[] values = RESTUtils.getParameterValueFromOperationInput(operationInput, new String[]{"x", "y"});
                logger.warning(TAG, "Operation '" + operationName + "' with params: " + Arrays.toString(values) + " ...");
                try {
                    double xCoord = Double.parseDouble(values[0]);
                    double yCoord = Double.parseDouble(values[1]);
                    double height = getHeightForPoint(xCoord, yCoord);
                    JSONObject answerObject = new JSONObject();
                    answerObject.put("name", operationName);
                    answerObject.put("valid", "ok");
                    answerObject.put("height", height);
                    return answerObject.toString().getBytes();
                } catch (JSONException e) {
                    logger.error(TAG, "PChExportSOE.handleRESTRequest.JSONException", e);
                    return RESTUtils.sendError(0, "Operation " + "\"" + operationName + "\" JSONException",
                            new String[]{"no details", " specified"}).getBytes();
                }
            } else if (operationName.equalsIgnoreCase("getHeightForLine")) {
                String[] values = RESTUtils.getParameterValueFromOperationInput(operationInput, new String[]{"geometry"});
                logger.warning(TAG, "Operation '" + operationName + "' with params: " + Arrays.toString(values) + " ...");
                try {
                    if (values[0].startsWith("[")) {
                        values[0] = values[0].substring(1, values[0].length() - 1);
                    }
                    JSONObject jsonInputGeom = new JSONObject(values[0]);
                    AgsJsonPolyline jsonInputPolyline = new AgsJsonPolyline(logger, jsonInputGeom);
                    IPolyline inputLine = jsonInputPolyline.toArcObject();
                    double length2D = inputLine.getLength();
                    logger.error(TAG, "111x");
                    int stepValue = Math.max(5, (int) (inputLine.getLength() / 200));
                    logger.error(TAG, "stepValue: " + stepValue);
                    Polyline generated3DLine = (Polyline) heightTask.getHeightForLine(inputLine, stepValue);
                    logger.error(TAG, "111y");
                    JSONObject answerObject = new JSONObject();
                    answerObject.put("name", operationName);
                    answerObject.put("valid", "ok");
                    answerObject.put("length2D", length2D);
                    answerObject.put("length3D", generated3DLine.getLength());
                    logger.error(TAG, "111a");
                    JSONObject json3dPolyLine = AgsJsonGeometry.convertGeomToJson(logger, generated3DLine);
                    logger.error(TAG, "111b");
                    answerObject.put("polyline3D", json3dPolyLine);
                    // added for ESRI widget
                    answerObject.put("sampleDistance", stepValue);
                    answerObject.put("geometries", json3dPolyLine);
                    JSONArray elevArray = new JSONArray();
                    for (int i = 0; i < generated3DLine.getPointCount(); i++) {
                        Point tmpPt = (Point) generated3DLine.getPoint(i);
                        elevArray.put(tmpPt.getZ());
                    }
                    JSONArray elev2Array = new JSONArray();
                    elev2Array.put(elevArray);
                    answerObject.put("elevations", elevArray);
                    return answerObject.toString().getBytes();
                } catch (JSONException e) {
                    logger.error(TAG, "PChExportSOE.handleRESTRequest.JSONException", e);
                    return RESTUtils.sendError(0, "Operation " + "\"" + operationName + "\" JSONException",
                            new String[]{"JSONException", e.getMessage()}).getBytes();
                } catch (Exception e) {
                    logger.error(TAG, "PChExportSOE.handleRESTRequest.Exception", e);
                    return RESTUtils.sendError(0, "Operation " + "\"" + operationName + "\" Exception",
                            new String[]{"Exception", e.getMessage()}).getBytes();
                }
            } else {
                logger.warning(TAG, "Operation is NOT YET IMPLEMENTED: " + operationName + " !!!");
                return RESTUtils.sendError(0, "Operation " + "\"" + operationName + "\" NOT YET IMPLEMENTED on resource [" + resourceName + "].",
                        new String[]{"no details", " specified"}).getBytes();
            }
        } catch (AutomationException e) {
            logger.error(TAG, "PChExportSOE.handleRESTRequest.AutomationException", e);
        } catch (IOException e) {
            logger.error(TAG, "PChExportSOE.handleRESTRequest.IOException", e);
        }
        return null;
    }

    @Override
    public String getMetadataByID(int layerId) throws IOException {
        return metadataTask.getMetadataByID(layerId);
    }

    @Override
    public PKResultBean[] getPKsByCoord(double coordX, double coordY, String wkid, double tolerance, int maxRecords) throws IOException {
        return (PKResultBean[]) calculatePkTask.getPKsByCoordArray(coordX, coordY, wkid, tolerance, maxRecords).toArray();
    }

    @Override
    public double getHeightForPoint(double coordX, double coordY) throws IOException {
        return heightTask.getHeightForPoint(coordX, coordY);
    }

    @Override
    public HeightResultBean[] getHeightForLine(String lineGeom) throws IOException, JSONException {
        IPolyline inputLine = new AgsJsonPolyline(logger, new JSONObject(lineGeom)).toArcObject();
        int stepValue = Math.max(5, (int) (inputLine.getLength() / 200));
        Polyline polyline = (Polyline) heightTask.getHeightForLine(inputLine, stepValue);
        if (polyline.isZAware()) {
            HeightResultBean[] heightResultBeans = new HeightResultBean[polyline.getPointCount()];
            for (int i = 0; i < heightResultBeans.length; i++) {
                IPoint pt = polyline.getPoint(i);
                heightResultBeans[i] = new HeightResultBean(pt);
            }
            return heightResultBeans;
        }
        return null;
    }

    @Override
    public String exportLayers() {
        return legendTask.exportLayers();
    }

    @Override
    public int getLayerCount() throws IOException {
        Map map;
        logger.debug(TAG, "getLayerCount.start...");
        try {
            map = (Map) mapServer.getMap(mapServer.getDefaultMapName());
            logger.debug(TAG, "getLayerCount.map= " + map);
            return map.getLayerCount();
        } catch (AutomationException aEx) {
            logger.error(TAG, "PCHExportSOE.getLayerCount", aEx);
            logger.error(TAG, "getLayerCount.AutomationException for MAPcast: Is " + soHelper.getServerObject().getConfigurationName() + " a MSD mapService ??");
        }
        return 0;
    }
}
