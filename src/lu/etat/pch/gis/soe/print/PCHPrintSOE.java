/*
 *
 *  *  PCHPrintSOE - Advanced printing SOE for ArcGIS Server
 *  *  Copyright (C) 2010-2012 Tom Schuller
 *  *
 *  *  This program is free software: you can redistribute it and/or modify
 *  *  it under the terms of the GNU Lesser General Public License as published by
 *  *  the Free Software Foundation, either version 3 of the License, or
 *  *  (at your option) any later version.
 *  *
 *  *  This program is distributed in the hope that it will be useful,
 *  *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  *  GNU Lesser General Public License for more details.
 *  *
 *  *  You should have received a copy of the GNU Lesser General Public License
 *  *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package lu.etat.pch.gis.soe.print;

import com.esri.arcgis.carto.IMapServerDataAccess;
import com.esri.arcgis.carto.MapServer;
import com.esri.arcgis.interop.extn.ArcGISExtension;
import com.esri.arcgis.interop.extn.ServerObjectExtProperties;
import com.esri.arcgis.server.IServerObjectExtension;
import com.esri.arcgis.server.IServerObjectHelper;
import com.esri.arcgis.server.json.JSONArray;
import com.esri.arcgis.server.json.JSONException;
import com.esri.arcgis.server.json.JSONObject;
import com.esri.arcgis.system.IObjectConstruct;
import com.esri.arcgis.system.IPropertySet;
import com.esri.arcgis.system.IRESTRequestHandler;
import com.esri.arcgis.system.ServerUtilities;
import lu.etat.pch.gis.soe.tasks.ExportLayerTask;
import lu.etat.pch.gis.soe.tasks.print.PrintTask;
import lu.etat.pch.gis.soeClients.IPCHPrintSOE;
import lu.etat.pch.gis.utils.RESTUtils;
import lu.etat.pch.gis.utils.SOELogger;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.Properties;

/**
 * Created by IntelliJ IDEA.
 * User: schullto
 * Date: Mar 1, 2010
 * Time: 7:15:14 AM
 */
@ArcGISExtension
@ServerObjectExtProperties(
        displayName = "PChPrintSOE",
        description = "PChPrintSOE, multi-mapservices print. http://bitbucket.schuller.lu",
        allSOAPCapabilities = {"printMap", "printLayout"},
        properties = {"useProxy=false", "proxyHost=none", "proxyPort=8080", "noProxyHosts=none"}
//        ,supportsMSD = true
)
public class PCHPrintSOE implements IServerObjectExtension, IObjectConstruct, IRESTRequestHandler, IPCHPrintSOE {
    private static final String TAG = "PChPrintSOE";
    private IServerObjectHelper soHelper;
    private SOELogger logger;
    private MapServer mapServer = null;
    private IMapServerDataAccess mapServerDataAccess;

    private PrintTask printTask;
    private ExportLayerTask exportLayerTask;
    private String buildTimestamp = "??";

    public void init(IServerObjectHelper soh) throws IOException {
        this.soHelper = soh;
        this.logger = new SOELogger(ServerUtilities.getServerLogger(), 8001);
        logger.debug(TAG, "init...");
        this.mapServer = (MapServer) soHelper.getServerObject();
        logger.debug(TAG, "init.mapServer", mapServer);
        this.mapServerDataAccess = (IMapServerDataAccess) soh.getServerObject();
        logger.debug(TAG, "init.mapServerDataAccess", mapServerDataAccess);

        Properties prop = new Properties();
        try {
            InputStream in = getClass().getResourceAsStream("/pcharcgissoe.properties");
            prop.load(in);
            in.close();
        } catch (Exception ignored) {
        }
        buildTimestamp = ">" + prop.getProperty("buildTimestamp") + "<";
    }

    public void shutdown() throws IOException {
        this.soHelper = null;
        this.mapServer = null;
        logger.debug(TAG, "shutdown...");
        this.logger = null;
    }

    @Override
    public void construct(IPropertySet propertySet) throws IOException {
        logger.debug(TAG, "construct...starting");
        printTask = new PrintTask(logger, mapServer, mapServer.getConfigurationName());
        exportLayerTask = new ExportLayerTask(logger, mapServer, mapServer.getConfigurationName());
        exportLayerTask.exportLayers();
        printTask.cleanupOldExports();
        logger.debug(TAG, "construct ...FINISHED");
    }

    public byte[] handleRESTRequest(String capabilities, String resourceName, String operationName, String operationInput, String outputFormat, String requestProperties, String[] responseProperties) throws IOException {
        logger.debug(TAG, "In handleRESTRequest(): " + ";\ncapabilities: " + capabilities + ";\nresourceName: " + resourceName + ";\noperationName: " + operationName + ";\noperationName length: " + operationName.length() + ";\noperationInput: " + operationInput + ";\noutputFormat: " + outputFormat
                + ";\nresponseProperties length: " + responseProperties.length);
        logger.debug(TAG, "Operation is permitted", operationName);
        try {
            if (operationName.length() == 0) {
                return getResource(resourceName);
            } else {
                JSONObject jsonObject = new JSONObject(operationInput);
                logger.debug(TAG, "handleRESTRequest.jsonObject", jsonObject);
                String answer;
                if (operationName.equalsIgnoreCase("printMap")) {
                    logger.debug(TAG, "handleRESTRequest.printMap", jsonObject);
                    answer = printMap(jsonObject);
                    logger.debug(TAG, "handleRESTRequest.printMap.answer", answer);
                } else if (operationName.equalsIgnoreCase("printLayout")) {
                    logger.debug(TAG, "handleRESTRequest.printLayout: " + jsonObject);
                    answer = printLayout(jsonObject);
                    logger.debug(TAG, "handleRESTRequest.printLayout.answer", answer);
                } else if (operationName.equalsIgnoreCase("layerFile")) {
                    String layerId = jsonObject.getString("layerId");
                    answer = exportLayerTask.getLayerLyrFile(layerId);
                } else {
                    logger.warning(TAG, "Operation is NOT YET IMPLEMENTED: " + operationName + " !!!");
                    answer = RESTUtils.sendError(0, "Operation " + "\"" + operationName + "\" NOT YET IMPLEMENTED on resource [" + resourceName + "].",
                            new String[]{"details", "operation is not supported"});
                }
                logger.debug(TAG, "handleRESTRequest.answer", answer);
                return answer.getBytes();
            }
        } catch (JSONException e) {
            logger.error(TAG, "handleRESTRequest-JSONException", e);
            return RESTUtils.sendError(0, "Operation " + "\"" + operationName + "\" ERROR [" + resourceName + "].",
                    new String[]{"JSONException", e.getMessage()}).getBytes();
        }
    }

    private byte[] getResource(String resourceName) {
        logger.debug(TAG, "getResource()...");
        try {
            if (resourceName.equalsIgnoreCase("") || resourceName.length() == 0) {
                return getRootResourceDescription().toString().getBytes();
            } else if (resourceName.equalsIgnoreCase("documentation")) {
                logger.debug(TAG, "documentationUrl...");
                URL url = PCHPrintSOE.class.getClassLoader().getResource("docs/index.html");
                logger.debug(TAG, "documentationUrl: " + url);
                String retString = loadUrl(url);
                return retString.getBytes();
            }
        } catch (Exception e) {
            logger.error(TAG, "getResource.Exception", e);
        }
        return null;
    }

    private String loadUrl(URL url) {
        logger.debug(TAG, "url: " + url);
        if (url != null) {
            try {
                InputStream is = url.openStream();
                InputStreamReader ins = new InputStreamReader(is);
                BufferedReader buffReader = new BufferedReader(ins);
                StringBuffer sb = new StringBuffer();
                String fileLine;
                while ((fileLine = buffReader.readLine()) != null) {
                    logger.debug(TAG, "fileLine", fileLine);
                    sb.append(fileLine);
                }
                is.close();
                return sb.toString();
            } catch (IOException e) {
                logger.error(TAG, "getResource.IOException", e);
            }
        }
        return "<h1>!!! no help-file found !!!</h1><h2>developed by <a href=\"mailto:tom.schuller@pch.etat.lu\">tom.schuller@pch.etat.lu</a></h2>";
    }

    private JSONObject getRootResourceDescription() {
        return RESTUtils.createResource("PCHPrintSOE", "PCHPrintSOE, multi-mapservices print [" + buildTimestamp + "]", false);
    }

    @Override
    public String getSchema() throws IOException {
        logger.debug(TAG, "getSchema()...");
        try {
            JSONObject pchExportSOE = getRootResourceDescription();
            JSONArray operationArray = new JSONArray();
            operationArray.put(RESTUtils.createOperation("printMap", "mapExtent,printOutput,mapElements,mapServices", "json"));
            operationArray.put(RESTUtils.createOperation("printLayout", "mapExtent,printOutput,mapElements,layoutElements,mapServices", "json"));
            operationArray.put(RESTUtils.createOperation("layerFile", "layerId", "json"));
            pchExportSOE.put("operations", operationArray);

            JSONArray subResourceArray = new JSONArray();
            JSONObject helpResource = RESTUtils.createResource("documentation", "documentation concerning the usage for this SOE", false);
            subResourceArray.put(helpResource);
            pchExportSOE.put("resources", subResourceArray);
            logger.debug(TAG, "getSchema().finished");
            return pchExportSOE.toString();
        } catch (JSONException e) {
            logger.error(TAG, "getSchema().JSONException", e);
        }
        return null;
    }

    @Override
    public String printMap(JSONObject json) {
        return printTask.printMap(json, false);
    }

    @Override
    public String printLayout(JSONObject json) {
        return printTask.printLayout(json, false);
    }

    @Override
    public String printMap(String jsonString) {
        try {
            return printMap(new JSONObject(jsonString));
        } catch (JSONException e) {
            logger.error(TAG, "printMap(String)", e);
            return null;
        }
    }

    @Override
    public String printLayout(String jsonString) {
        try {
            return printLayout(new JSONObject(jsonString));
        } catch (JSONException e) {
            logger.error(TAG, "printLayout(String)", e);
            return null;
        }
    }
}