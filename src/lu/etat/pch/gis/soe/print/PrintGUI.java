/*
 *
 *  *  PCHPrintSOE - Advanced printing SOE for ArcGIS Server
 *  *  Copyright (C) 2010-2012 Tom Schuller
 *  *
 *  *  This program is free software: you can redistribute it and/or modify
 *  *  it under the terms of the GNU Lesser General Public License as published by
 *  *  the Free Software Foundation, either version 3 of the License, or
 *  *  (at your option) any later version.
 *  *
 *  *  This program is distributed in the hope that it will be useful,
 *  *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  *  GNU Lesser General Public License for more details.
 *  *
 *  *  You should have received a copy of the GNU Lesser General Public License
 *  *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package lu.etat.pch.gis.soe.print;

import com.esri.arcgis.beans.TOC.TOCBean;
import com.esri.arcgis.beans.map.MapBean;
import com.esri.arcgis.beans.pagelayout.PageLayoutBean;
import com.esri.arcgis.beans.toolbar.ToolbarBean;
import com.esri.arcgis.carto.MapDocument;
import com.esri.arcgis.carto.MapServer;
import com.esri.arcgis.controls.*;
import com.esri.arcgis.interop.AutomationException;
import com.esri.arcgis.server.IServerObjectHelper;
import com.esri.arcgis.server.json.JSONArray;
import com.esri.arcgis.server.json.JSONException;
import com.esri.arcgis.server.json.JSONObject;
import com.esri.arcgis.system.AoInitialize;
import com.esri.arcgis.systemUI.esriCommandStyles;
import lu.etat.pch.gis.soe.tasks.print.PrintTask;
import lu.etat.pch.gis.utils.LicenceUtils;
import lu.etat.pch.gis.utils.SOELogger;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.net.UnknownHostException;
import java.util.StringTokenizer;

/**
 * Created with IntelliJ IDEA.
 * User: schullto
 * Date: 29/03/12
 * Time: 16:16
 */
public class PrintGUI {
    private JButton printButton;
    private JTextArea taInput;
    private JTextArea taOutput;
    private JTextField tfMxdFile;
    public JPanel mainPanel;
    private JTextArea taJson;
    private JButton btJson;
    private JTabbedPane tabbedPane;
    private JPanel mapMainManel;
    private JPanel mapTopPanel;
    private JPanel mapTocPanel;
    private PageLayoutBean pageLayout;
    private MapBean map;
    private TOCBean toc;
    private ToolbarBean toolbar;
    private ToolbarMenu popupMenu;

    private static IServerObjectHelper soHelper;

    public static void main(String[] args) {
        try {
            AoInitialize aoInit = LicenceUtils.initArcEngine();
        } catch (IOException e) {
            e.printStackTrace();
        }
        PrintGUI printGUI = new PrintGUI();

        JFrame myFrame = new JFrame("PchPrintSOE tester");
        printGUI.taInput.setText("{\n" +
                "  \"layoutElements\": [\n" +
                "    {\n" +
                "      \"height\": null,\n" +
                "      \"anchor\": null,\n" +
                "      \"symbol\": {\n" +
                "        \"style\": \"esriSFSSolid\",\n" +
                "        \"color\": {\n" +
                "          \"green\": 218,\n" +
                "          \"alpha\": 255,\n" +
                "          \"red\": 229,\n" +
                "          \"blue\": 190\n" +
                "        },\n" +
                "        \"type\": \"esriSFS\",\n" +
                "        \"outline\": null\n" +
                "      },\n" +
                "      \"xOffset\": null,\n" +
                "      \"name\": \"??? ?? ????? ????\",\n" +
                "      \"yOffset\": null,\n" +
                "      \"geometry\": {\n" +
                "        \"ymin\": 0.075,\n" +
                "        \"ymax\": 29.625,\n" +
                "        \"xmax\": 20.925,\n" +
                "        \"xmin\": 0.075\n" +
                "      },\n" +
                "      \"visible\": true,\n" +
                "      \"width\": null,\n" +
                "      \"order\": 1\n" +
                "    },\n" +
                "    {\n" +
                "      \"height\": null,\n" +
                "      \"anchor\": null,\n" +
                "      \"symbol\": {\n" +
                "        \"border\": {\n" +
                "          \"style\": \"esriSLSSolid\",\n" +
                "          \"color\": {\n" +
                "            \"green\": 0,\n" +
                "            \"alpha\": 255,\n" +
                "            \"red\": 0,\n" +
                "            \"blue\": 0\n" +
                "          },\n" +
                "          \"type\": \"esriSLS\",\n" +
                "          \"width\": 3\n" +
                "        },\n" +
                "        \"rotation\": 0,\n" +
                "        \"mapElements\": null,\n" +
                "        \"type\": \"pchMapFrame\",\n" +
                "        \"referenceScale\": -1,\n" +
                "        \"extent\": null,\n" +
                "        \"mapServices\": null\n" +
                "      },\n" +
                "      \"xOffset\": null,\n" +
                "      \"name\": \"mainMapFrame\",\n" +
                "      \"yOffset\": null,\n" +
                "      \"geometry\": null,\n" +
                "      \"visible\": true,\n" +
                "      \"width\": null,\n" +
                "      \"order\": 5\n" +
                "    },\n" +
                "    {\n" +
                "      \"height\": null,\n" +
                "      \"anchor\": \"bottommid\",\n" +
                "      \"symbol\": {\n" +
                "        \"unitLabelPosition\": null,\n" +
                "        \"unitLabelSymbol\": null,\n" +
                "        \"labelGap\": null,\n" +
                "        \"barColor\": null,\n" +
                "        \"type\": \"pchScaleBar\",\n" +
                "        \"scaleBarType\": \"AlternatingScaleBar\",\n" +
                "        \"barHeight\": 5,\n" +
                "        \"units\": \"km\",\n" +
                "        \"divisions\": null,\n" +
                "        \"divisionsBeforeZero\": null,\n" +
                "        \"labelFrequency\": null,\n" +
                "        \"labelPosition\": null,\n" +
                "        \"division\": null,\n" +
                "        \"backgroundColor\": {\n" +
                "          \"green\": 255,\n" +
                "          \"alpha\": 255,\n" +
                "          \"red\": 255,\n" +
                "          \"blue\": 255\n" +
                "        },\n" +
                "        \"numberFormat\": null,\n" +
                "        \"resizeHint\": null,\n" +
                "        \"labelSymbol\": null,\n" +
                "        \"subdivisions\": null,\n" +
                "        \"unitLabel\": null,\n" +
                "        \"unitLabelGap\": null\n" +
                "      },\n" +
                "      \"xOffset\": 0,\n" +
                "      \"name\": \"????? ???\",\n" +
                "      \"yOffset\": 0,\n" +
                "      \"geometry\": {\n" +
                "        \"y\": 1,\n" +
                "        \"x\": 13\n" +
                "      },\n" +
                "      \"visible\": true,\n" +
                "      \"width\": null,\n" +
                "      \"order\": 10\n" +
                "    },\n" +
                "    {\n" +
                "      \"height\": null,\n" +
                "      \"anchor\": \"topleft\",\n" +
                "      \"symbol\": {\n" +
                "        \"numericFormat\": {\n" +
                "          \"showPlusSign\": false,\n" +
                "          \"useSeparator\": false,\n" +
                "          \"zeroPad\": false,\n" +
                "          \"roundingOption\": 0,\n" +
                "          \"roundingValue\": 3,\n" +
                "          \"alignmentOption\": null,\n" +
                "          \"alignmentWidth\": null\n" +
                "        },\n" +
                "        \"pageUnitLabel\": \"\",\n" +
                "        \"type\": \"pchScaleText\",\n" +
                "        \"separator\": \":\",\n" +
                "        \"pageUnits\": \"cm\",\n" +
                "        \"style\": 0,\n" +
                "        \"mapUnitLabel\": \"\",\n" +
                "        \"mapUnits\": \"km\",\n" +
                "        \"backgroundColor\": {\n" +
                "          \"green\": 255,\n" +
                "          \"alpha\": 255,\n" +
                "          \"red\": 255,\n" +
                "          \"blue\": 255\n" +
                "        }\n" +
                "      },\n" +
                "      \"xOffset\": 0,\n" +
                "      \"name\": \"????? ????\",\n" +
                "      \"yOffset\": 0,\n" +
                "      \"geometry\": {\n" +
                "        \"y\": 27.7,\n" +
                "        \"x\": 6\n" +
                "      },\n" +
                "      \"visible\": true,\n" +
                "      \"width\": null,\n" +
                "      \"order\": 10\n" +
                "    },\n" +
                "    {\n" +
                "      \"height\": null,\n" +
                "      \"anchor\": \"bottomright\",\n" +
                "      \"symbol\": {\n" +
                "        \"pictureUrl\": \"http://94.23.2.120/insdi_gis/assets/images/mask_b_1.png\",\n" +
                "        \"type\": \"pchPictureElement\"\n" +
                "      },\n" +
                "      \"xOffset\": 0,\n" +
                "      \"name\": \"???\",\n" +
                "      \"yOffset\": 0,\n" +
                "      \"geometry\": {\n" +
                "        \"y\": 1,\n" +
                "        \"x\": 20\n" +
                "      },\n" +
                "      \"visible\": true,\n" +
                "      \"width\": null,\n" +
                "      \"order\": 10\n" +
                "    },\n" +
                "    {\n" +
                "      \"height\": \"4.95\",\n" +
                "      \"anchor\": \"topleft\",\n" +
                "      \"symbol\": {\n" +
                "        \"pictureUrl\": \"http://94.23.2.120/data/legend/legend300.jpg\",\n" +
                "        \"type\": \"pchPictureElement\"\n" +
                "      },\n" +
                "      \"xOffset\": -5.2,\n" +
                "      \"name\": \"??????? ????\",\n" +
                "      \"yOffset\": -18,\n" +
                "      \"geometry\": {\n" +
                "        \"ymin\": 9.7,\n" +
                "        \"ymax\": 28.7,\n" +
                "        \"xmax\": 4.8,\n" +
                "        \"xmin\": 0.7999999999999998\n" +
                "      },\n" +
                "      \"visible\": true,\n" +
                "      \"width\": \"19\",\n" +
                "      \"order\": 10\n" +
                "    },\n" +
                "    {\n" +
                "      \"height\": null,\n" +
                "      \"anchor\": null,\n" +
                "      \"symbol\": {\n" +
                "        \"border\": {\n" +
                "          \"style\": \"esriSLSSolid\",\n" +
                "          \"color\": {\n" +
                "            \"green\": 0,\n" +
                "            \"alpha\": 255,\n" +
                "            \"red\": 0,\n" +
                "            \"blue\": 0\n" +
                "          },\n" +
                "          \"type\": \"esriSLS\",\n" +
                "          \"width\": 2\n" +
                "        },\n" +
                "        \"rotation\": 0,\n" +
                "        \"mapElements\": null,\n" +
                "        \"type\": \"pchMapFrame\",\n" +
                "        \"referenceScale\": -1,\n" +
                "        \"extent\": null,\n" +
                "        \"mapServices\": null\n" +
                "      },\n" +
                "      \"xOffset\": null,\n" +
                "      \"name\": \"mainMapFrame\",\n" +
                "      \"yOffset\": null,\n" +
                "      \"geometry\": null,\n" +
                "      \"visible\": true,\n" +
                "      \"width\": null,\n" +
                "      \"order\": 1\n" +
                "    }\n" +
                "  ],\n" +
                "  \"mapServices\": [\n" +
                "    {\n" +
                "      \"alpha\": 1,\n" +
                "      \"type\": \"AGS\",\n" +
                "      \"name\": \"Earth Image\",\n" +
                "      \"definitionExpression\": null,\n" +
                "      \"token\": null,\n" +
                "      \"visibleIds\": \"*\",\n" +
                "      \"url\": \"https://server.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer\"\n" +
                "    }\n" +
                "  ],\n" +
                "  \"printOutput\": {\n" +
                "    \"format\": \"jpg\",\n" +
                "    \"borderWidth\": [\n" +
                "      6,\n" +
                "      1,\n" +
                "      2,\n" +
                "      1\n" +
                "    ],\n" +
                "    \"pageUnits\": \"cm\",\n" +
                "    \"referenceScale\": 0,\n" +
                "    \"refererWebsite\": \"http://www.web2gis.com\",\n" +
                "    \"border\": null,\n" +
                "    \"exportSettings\": {\n" +
                "      \"exportMeasureInfo\": false,\n" +
                "      \"exportPDFLayersAndFeatureAttributes\": 0,\n" +
                "      \"exportPictureSymbolOptions\": 2,\n" +
                "      \"imageCompression\": 0,\n" +
                "      \"polyginizeMarkers\": false,\n" +
                "      \"compressed\": true,\n" +
                "      \"embedFonts\": true,\n" +
                "      \"colorspace\": 0\n" +
                "    },\n" +
                "    \"resolution\": 160,\n" +
                "    \"mapRotation\": 0,\n" +
                "    \"width\": 21,\n" +
                "    \"toRemoveLayoutElements\": [\"*\"],\n" +
                "    \"height\": 29.7\n" +
                "  },\n" +
                "  \"mapGrids\": [\n" +
                "  ],\n" +
                "  \"mapExtent\": {\n" +
                "    \"ymin\": 3773725.720164609,\n" +
                "    \"spatialReference\": {\"wkid\": 102100},\n" +
                "    \"ymax\": 3853825.720164609,\n" +
                "    \"xmax\": 6149075,\n" +
                "    \"xmin\": 6107075\n" +
                "  },\n" +
                "  \"mapElements\": [],\n" +
                "  \"command\": \"layout\"\n" +
                "}");
        myFrame.getContentPane().add(printGUI.mainPanel);
        myFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        myFrame.setSize(700, 600);
        myFrame.setVisible(true);
        System.out.println("myFrame = " + myFrame);
    }

    public PrintGUI() {
        printButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String mxdFileName = tfMxdFile.getText();
                String input = taInput.getText().trim();
                try {
                    File mxdFile = new File(mxdFileName);
                    if (!mxdFile.canRead()) {
                        MapDocument mapDocument = new MapDocument();
                        File tmpFile = File.createTempFile("PrintSOE_PrintGUI_", ".mxd");
                        tmpFile.deleteOnExit();
                        mxdFileName = tmpFile.toString();
                        System.out.println("creating TEMP mxdDocument for layout= " + mxdFileName);
                        mapDocument.esri_new(mxdFileName);
                        mapDocument.save(false, false);
                        mapDocument.close();
                    }

                    MapServer mapServer = new MapServer();
                    mapServer.connect(mxdFileName);
                    System.out.println("loaded.mxdFileName = " + mxdFileName);
                    String name = mapServer.getDefaultMapName();
                    mapServer.getMap(mapServer.getDefaultMapName()).clearLayers();

                    SOELogger soeLogger = new SOELogger(taOutput, 9000);

                    PrintTask printTask = new PrintTask(soeLogger, mapServer, "printGUI");
                    JSONObject jsonObject;
                    boolean gpTask = true;
                    if (input.contains("handleRESTRequest")) {
                        jsonObject = getParamsFromLog(input);
                        gpTask = false;
                    } else if (input.startsWith("{") && input.endsWith("}")) {
                        jsonObject = new JSONObject(input);
                    } else {
                        jsonObject = getUrlParams(input);
                        gpTask = false;
                    }
                    if (jsonObject.has("input")) {
                        String inputStr = jsonObject.getString("input");
                        System.out.println("input = " + inputStr);
                        inputStr = inputStr.replace("\\\"", "\"");
                        System.out.println("input = " + inputStr);
                        jsonObject = new JSONObject(inputStr);
                    }

                    taJson.setEditable(false);
                    taJson.setText(jsonObject.toString(4));

                    String answer = null;
                    if (jsonObject.getString("command").equals("map"))
                        answer = printTask.printMap(jsonObject, gpTask);
                    else if (jsonObject.getString("command").equals("layout")) {
                        for (int i = 0; i < 1; i++) {
                            answer = printTask.printLayout(jsonObject, gpTask);
                            System.out.println("answer = " + answer);
                        }
                    }
                    System.out.println("answer = " + answer);
                    String newText = taOutput.getText() + "\n\nanswer = " + answer;
                    taOutput.setText(newText);
                    taOutput.setCaretPosition(newText.length());
                    tabbedPane.setSelectedIndex(2);

                    String documentPath = answer + ".mxd";
                    if (pageLayout!=null && pageLayout.checkMxFile(documentPath)) {
                        pageLayout.loadMxFile(documentPath, null);
                    }else if(map.checkMxFile(documentPath)) {
                        map.loadMxFile(documentPath,null,null);
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        });
        btJson.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String input = taInput.getText().trim();
                JSONObject jsonObject;
                if (input.contains("handleRESTRequest"))
                    jsonObject = getParamsFromLog(input);
                else if (input.startsWith("{") && input.endsWith("}")) {
                    jsonObject = new JSONObject(input);
                } else
                    jsonObject = getUrlParams(input);
                if (jsonObject.has("input")) {
                    String inputStr = jsonObject.getString("input");
                    System.out.println("input = " + inputStr);
                    inputStr = inputStr.replace("\\\"", "\"");
                    System.out.println("input = " + inputStr);
                    jsonObject = new JSONObject(inputStr);
                }

                taJson.setEditable(false);
                taJson.setText(jsonObject.toString(2));
                tabbedPane.setSelectedIndex(1);
            }
        });
        //not 'yet' working under 10.3 pageLayout = new PageLayoutBean();
        map = new MapBean();
        toc = new TOCBean();
        toolbar = new ToolbarBean();

        mapTopPanel.setLayout(new BorderLayout());
        mapTopPanel.add(toolbar, BorderLayout.CENTER);

        mapMainManel.setLayout(new BorderLayout());
        mapMainManel.add(pageLayout!=null?pageLayout:map, BorderLayout.CENTER);
        mapTocPanel.setLayout(new BorderLayout());
        mapTocPanel.add(toc, BorderLayout.WEST);

        try {
            toc.setLabelEdit(esriTOCControlEdit.esriTOCControlManual);

            toolbar.addItem(new ControlsOpenDocCommand(), 0, -1, false, 0, esriCommandStyles.esriCommandStyleIconOnly);
            toolbar.addItem(new ControlsSelectTool(), 0, -1, false, 0, esriCommandStyles.esriCommandStyleIconOnly);
            toolbar.addItem(new ControlsPageZoomInTool(), 0, -1, false, 0, esriCommandStyles.esriCommandStyleIconOnly);
            toolbar.addItem(new ControlsPageZoomOutTool(), 0, -1, false, 0, esriCommandStyles.esriCommandStyleIconOnly);
            toolbar.addItem(new ControlsPagePanTool(), 0, -1, false, 0, esriCommandStyles.esriCommandStyleIconOnly);
            toolbar.addItem(new ControlsPageZoomWholePageCommand(), 0, -1, false, 0, esriCommandStyles.esriCommandStyleIconOnly);
            toolbar.addItem(new ControlsPageZoomPageToLastExtentBackCommand(), 0, -1, false, 0, esriCommandStyles.esriCommandStyleIconOnly);
            toolbar.addItem(new ControlsPageZoomPageToLastExtentForwardCommand(), 0, -1, false, 0, esriCommandStyles.esriCommandStyleIconOnly);
            toolbar.addItem(new ControlsMapZoomInTool(), 0, -1, false, 0, esriCommandStyles.esriCommandStyleIconOnly);
            toolbar.addItem(new ControlsMapZoomOutTool(), 0, -1, false, 0, esriCommandStyles.esriCommandStyleIconOnly);
            toolbar.addItem(new ControlsMapPanTool(), 0, -1, false, 0, esriCommandStyles.esriCommandStyleIconOnly);
            toolbar.addItem(new ControlsMapFullExtentCommand(), 0, -1, false, 0, esriCommandStyles.esriCommandStyleIconOnly);

            toc.setBuddyControl(pageLayout);
            toolbar.setBuddyControl(pageLayout);
        } catch (AutomationException e) {
            e.printStackTrace();
        } catch (UnknownHostException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static JSONObject getParamsFromLog(String logEntry) {
        try {
            int posiStart = logEntry.lastIndexOf("operationInput");
            int posiEnd = logEntry.lastIndexOf("outputFormat");
            String params = logEntry.substring(posiStart + 15, posiEnd - 1);
            params = params.replaceAll("&quot;", "\"");
            JSONObject retObj = new JSONObject(params);
            if (logEntry.contains("operationName: printMap")) retObj.put("command", "map");
            if (logEntry.contains("operationName: printLayout")) retObj.put("command", "layout");
            return retObj;
        } catch (StringIndexOutOfBoundsException ex) {
            //try 10.1
            String params = logEntry.substring(logEntry.indexOf('{'), logEntry.lastIndexOf('}') + 1);
            params = params.replaceAll("&quot;", "\"");
            JSONObject retObj = new JSONObject(params);
            if (logEntry.contains("handleRESTRequest.printMap")) retObj.put("command", "map");
            if (logEntry.contains("handleRESTRequest.printLayout")) retObj.put("command", "layout");
            return retObj;
        }
    }

    private static JSONObject getUrlParams(String url) {
        JSONObject jsonObject = new JSONObject();
        if (url.contains("/printLayout?")) jsonObject.put("command", "layout");
        if (url.contains("/printMap?")) jsonObject.put("command", "map");
        String params = url.substring(url.indexOf("?") + 1);
        StringTokenizer st = new StringTokenizer(params, "&", false);
        while (st.hasMoreTokens()) {
            String param = st.nextToken();
            int posi = param.indexOf("=");
            System.out.println("posi = " + posi);
            String paramName = param.substring(0, posi);
            String paramValue = param.substring(posi + 1);
            System.out.println("paramName = " + paramName);
            System.out.println("paramValue = " + paramValue);
            if (paramName.equals("f") && paramValue.equals("json")) {
                //do nothing, ignore
            } else {
                if (paramValue.startsWith("["))
                    try {
                        jsonObject.put(paramName, new JSONArray(paramValue));
                    } catch (JSONException ex) {
                        System.err.println(paramName + " >> " + ex.getMessage());
                    }
                else {
                    try {
                        jsonObject.put(paramName, new JSONObject(paramValue));
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }
        }
        return jsonObject;
    }


}
