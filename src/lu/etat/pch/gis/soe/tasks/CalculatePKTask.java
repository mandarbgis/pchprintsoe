/*
 *
 *  *  PCHPrintSOE - Advanced printing SOE for ArcGIS Server
 *  *  Copyright (C) 2010-2012 Tom Schuller
 *  *
 *  *  This program is free software: you can redistribute it and/or modify
 *  *  it under the terms of the GNU Lesser General Public License as published by
 *  *  the Free Software Foundation, either version 3 of the License, or
 *  *  (at your option) any later version.
 *  *
 *  *  This program is distributed in the hope that it will be useful,
 *  *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  *  GNU Lesser General Public License for more details.
 *  *
 *  *  You should have received a copy of the GNU Lesser General Public License
 *  *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package lu.etat.pch.gis.soe.tasks;

import com.esri.arcgis.carto.FeatureLayer;
import com.esri.arcgis.carto.ILayer;
import com.esri.arcgis.carto.MapServer;
import com.esri.arcgis.geodatabase.*;
import com.esri.arcgis.geometry.*;
import com.esri.arcgis.interop.AutomationException;
import com.esri.arcgis.interop.Cleaner;
import lu.etat.pch.gis.beans.pk.PKResultBean;
import lu.etat.pch.gis.utils.SOELogger;

import java.io.IOException;
import java.net.UnknownHostException;
import java.util.*;

/**
 * Created by IntelliJ IDEA.
 * User: schullto
 * Date: May 11, 2010
 * Time: 1:48:46 PM
 */
public class CalculatePKTask extends AbstractTask {
    private static final String TAG = "CalculatePKTask";
    private int routeSystemLayerIndex = 1;
    private static Map<String, Polyline> cachedRoutes = new HashMap<String, Polyline>();

    public CalculatePKTask(SOELogger logger, MapServer mapServer, String mapServiceName) {
        super(logger, mapServer, mapServiceName);
    }

    public Polyline getCachedPolyline(String routeName) {
        routeName = routeName.toUpperCase();
        if (cachedRoutes.containsKey(routeName)) {
            return cachedRoutes.get(routeName);
        }
        try {
            ILayer layer = map.getLayer(routeSystemLayerIndex);
            FeatureLayer fLayer = (FeatureLayer) layer;
            logger.debug(TAG, "getPointForPK.Got featureLayer", fLayer);
            QueryFilter queryFilter = new QueryFilter();
            queryFilter.setWhereClause("DSG = '" + routeName + "'");
            IFeatureCursor featureCursor = fLayer.search(queryFilter, true);
            IFeature feature = featureCursor.nextFeature();
            if (feature != null) {
                IGeometry geom = feature.getShape();
                if (geom instanceof Polyline) {
                    Polyline polyline = (Polyline) geom;
                    cachedRoutes.put(routeName, polyline);
                    return polyline;
                }
            }
            Cleaner.release(featureCursor);
        } catch (AutomationException e) {
            e.printStackTrace();
        } catch (UnknownHostException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public Point getPointForPK(String routeName, double pk) throws IOException {
        Polyline polyline = getCachedPolyline(routeName);
        if (polyline != null) {
            IGeometryCollection geomCol = polyline.getPointsAtM(pk, 0);
            if (geomCol.getGeometryCount() > 0) {
                IGeometry pkPointGeom = geomCol.getGeometry(0);
                logger.debug(TAG, "getPointForPK.Got pkPointGeom", pkPointGeom);
                if (pkPointGeom instanceof Point) {
                    return (Point) pkPointGeom;
                }
            }
        }
        return null;
    }

    public Polyline getLineForPK(String routeName, double pkFrom, double pkTo) throws IOException {
        Polyline polyline = getCachedPolyline(routeName);
        if (polyline != null) {
            IGeometryCollection geomCol = polyline.getSubcurveBetweenMs(Math.min(pkFrom, pkTo), Math.max(pkFrom, pkTo));
            if (geomCol instanceof Polyline) {
                Polyline retPoly = (Polyline) geomCol;
                if (pkFrom > pkTo)
                    retPoly.reverseOrientation();
                return retPoly;
            }
        }
        return null;
    }

    public List<PKResultBean> getPKsByCoordArray(double coordX, double coordY, String wkid, double tolerance, int maxRecords) throws IOException {
        Point searchPoint = new Point();
        searchPoint.putCoords(coordX, coordY);
        ISpatialReference spatialReference = null;
        IGeoTransformation transformation = null;

        if (wkid != null && wkid.trim().length() > 0) {
            int wkidId = Integer.parseInt(wkid);
            SpatialReferenceEnvironment spatialReferenceEnvironment = new SpatialReferenceEnvironment();
            spatialReference = spatialReferenceEnvironment.createSpatialReference(wkidId);
            searchPoint.setSpatialReferenceByRef(spatialReference);
            transformation = (IGeoTransformation) spatialReferenceEnvironment.createGeoTransformation(esriSRGeoTransformation3Type.esriSRGeoTransformation_Luxembourg_1930_To_WGS_1984_2);
        }
        logger.debug(TAG, "Got searchPoint", searchPoint);

        ILayer layer = map.getLayer(routeSystemLayerIndex);
        FeatureLayer fLayer = (FeatureLayer) layer;
        logger.debug(TAG, "Got featureLayer", fLayer);

        searchPoint.projectEx(fLayer.getSpatialReference(), esriTransformDirection.esriTransformReverse, transformation, true, 0, 0);
        searchPoint.setSpatialReferenceByRef(fLayer.getSpatialReference());

        Envelope pEnv = new Envelope();
        pEnv.setXMax(searchPoint.getX() + tolerance);
        pEnv.setXMin(searchPoint.getX() - tolerance);
        pEnv.setYMax(searchPoint.getY() + tolerance);
        pEnv.setYMin(searchPoint.getY() - tolerance);
        pEnv.setSpatialReferenceByRef(searchPoint.getSpatialReference());

        SpatialFilter spatialFilter = new SpatialFilter();
        spatialFilter.setGeometryByRef(pEnv);
        spatialFilter.setSpatialRel(esriSpatialRelEnum.esriSpatialRelIntersects);
        spatialFilter.setGeometryField("SHAPE");

        int dsgFieldIndex = fLayer.getFeatureClass().findField("DSG");
        logger.debug(TAG, "Got dsgFieldIndex", dsgFieldIndex);

        try {
            IFeatureCursor feaCur = fLayer.search(spatialFilter, true);
            IFeature feature;
            ArrayList<PKResultBean> retList = new ArrayList<PKResultBean>();
            while ((feature = feaCur.nextFeature()) != null) {
                String routeName = (String) feature.getValue(dsgFieldIndex);
                logger.debug(TAG, "Got routeName", routeName);
                IGeometry geometry = feature.getShape();
                if (geometry instanceof IPolyline) {
                    IPolyline polyline = (IPolyline) geometry;
                    if (((IMAware) geometry).isMAware()) {
                        // polyline has m-values
                        Point outPoint = new Point();
                        double[] distanceAlongCurve = new double[1];
                        double[] distanceFromCurve = new double[1];
                        boolean[] isOnRightSide = new boolean[1];

                        // query the distance of the point from the start of the polyloie
                        polyline.queryPointAndDistance(esriSegmentExtension.esriNoExtension, searchPoint, false, outPoint, distanceAlongCurve, distanceFromCurve, isOnRightSide);
                        Object mArrayObject = ((IMSegmentation) polyline).getMsAtDistance(distanceAlongCurve[0], false);
                        double[] mArray = (double[]) mArrayObject;

                        // print Ms
                        if (mArray.length > 0) {
                            Line distLine = new Line();
                            distLine.setFromPoint(searchPoint);
                            distLine.setToPoint(outPoint);
                            //retVal = new MeasureItem(routeName, outPoint.getX(), outPoint.getY(), mArray[0], distanceFromCurve[0]);
                            PKResultBean pkResultBean = new PKResultBean();
                            pkResultBean.setRoute(routeName);
                            pkResultBean.setPk(mArray[0]);
                            pkResultBean.setOffset(distLine.getLength());
                            logger.debug(TAG, "outPoint.getSpatialReference().getName()", outPoint.getSpatialReference().getName());
                            if (spatialReference != null)
                                //outPoint.project(spatialReference);
                                outPoint.projectEx(spatialReference, esriTransformDirection.esriTransformForward, transformation, false, 0, 0);

                            pkResultBean.setX(outPoint.getX());
                            pkResultBean.setY(outPoint.getY());
                            retList.add(pkResultBean);
                        }
                    }
                }
            }
            Cleaner.release(feaCur);
            Collections.sort(retList, new Comparator<PKResultBean>() {
                @Override
                public int compare(PKResultBean o1, PKResultBean o2) {
                    return (int) ((o1.getOffset() - o2.getOffset()) * 100);
                }
            });
            if (retList.size() > maxRecords)
                return retList.subList(0, maxRecords);
            return retList;
        } catch (AutomationException e) {
            logger.error(TAG, "getPKsByCoordArray", e);
        }
        return null;
    }
}
