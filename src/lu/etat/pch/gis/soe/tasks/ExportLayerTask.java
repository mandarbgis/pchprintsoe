/*
 *
 *  *  PCHPrintSOE - Advanced printing SOE for ArcGIS Server
 *  *  Copyright (C) 2010-2012 Tom Schuller
 *  *
 *  *  This program is free software: you can redistribute it and/or modify
 *  *  it under the terms of the GNU Lesser General Public License as published by
 *  *  the Free Software Foundation, either version 3 of the License, or
 *  *  (at your option) any later version.
 *  *
 *  *  This program is distributed in the hope that it will be useful,
 *  *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  *  GNU Lesser General Public License for more details.
 *  *
 *  *  You should have received a copy of the GNU Lesser General Public License
 *  *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package lu.etat.pch.gis.soe.tasks;

import com.esri.arcgis.carto.*;
import com.esri.arcgis.interop.AutomationException;
import com.esri.arcgis.server.json.JSONObject;
import lu.etat.pch.gis.utils.SOELogger;

import java.io.File;
import java.io.IOException;

/**
 * Created by IntelliJ IDEA.
 * User: schullto
 * Date: May 6, 2010
 * Time: 6:17:09 PM
 */
public class ExportLayerTask extends AbstractTask {
    private static final String TAG = "ExportLayerTask";
    public static final String lyrSubDirectoryName = "lyr";
    public static final String lyrFileExtension = ".lyr";
    private File physicalLayerFileDirectory;


    public ExportLayerTask(SOELogger logger, MapServer mapServer, String mapServiceName) {
        super(logger, mapServer, mapServiceName);
        try {
            physicalLayerFileDirectory = getPhysicalMapServiceOutputFolder(lyrSubDirectoryName);
            logger.warning(TAG, "ExportLayerTask().physicalLayerFileDirectory: " + physicalLayerFileDirectory);
        } catch (IOException e) {
            logger.error(TAG, "ExportLayerTask().physicalLayerFileDirectory-ERROR", e);
        }
    }

    public void exportLayers() {
        if (!physicalLayerFileDirectory.canWrite()) {
            logger.error(TAG, "exportLayers: READ-ONLY or non-valid physicalLayerFileDirectory: " + physicalLayerFileDirectory);
            return;
        }
        File noExportLayers = new File(physicalLayerFileDirectory, "layersExport.no");
        if (noExportLayers.exists()) {
            logger.warning(TAG, "exportLayers: NOT exporting layers!! found file: " + noExportLayers);
            return;
        }
        noExportLayers = new File(physicalLayerFileDirectory, "exportLayers.no");
        if (noExportLayers.exists()) {
            logger.warning(TAG, "exportLayers: NOT exporting layers!! found file: " + noExportLayers);
            return;
        }


        try {
            String mxdFilePath = getMapServiceMxdFile();
            if (mxdFilePath == null) {
                logger.error(TAG, "exportLayers.mxdFilePath.isNULL !!");
                return;
            }
            MapReader mapReader = new MapReader();
            mapReader.open(mxdFilePath);
            IMap map = mapReader.getMap(0);
            if (map.getLayerCount() > 0) {
                logger.debug(TAG, "exportLayers.mapName: " + map.getName());
                GroupLayer myGroupLayer = new GroupLayer();
                myGroupLayer.setName(mapServiceName.replace("/", "_"));
                int layerIndex = 0;
                for (int i = 0; i < map.getLayerCount(); i++) {
                    ILayer layer = map.getLayer(i);
                    logger.debug(TAG, "exportLayers.layer", layer);
                    layerIndex = exportLayerFile(layerIndex, layer);
                    myGroupLayer.add(layer);
                    layerIndex++;
                }
                writeLayerFile(myGroupLayer, new File(physicalLayerFileDirectory, "layers_ALL" + lyrFileExtension));
            } else {
                logger.debug(TAG, "exportLayers: No layers found, nothing to do.");
            }
            mapReader.close();
        } catch (AutomationException e) {
            logger.error(TAG, "exportLayers", e);
        } catch (IOException e) {
            logger.error(TAG, "exportLayers", e);
        }
    }

    private int exportLayerFile(int layerIndex, ILayer myLayer) throws IOException {
        boolean layerVisibility = myLayer.isVisible();
        if (!layerVisibility) myLayer.setVisible(true);
        if (myLayer instanceof ILayerGeneralProperties) {
            ILayerGeneralProperties layerGeneralProperties = (ILayerGeneralProperties) myLayer;
            layerGeneralProperties.setLayerDescription("" + layerIndex);
        }
        String layerName = myLayer.getName();
        layerName = layerName.replaceAll("[^a-zA-Z0-9]", "_");
        logger.debug(TAG, "exportLayerFile[" + layerIndex + "]: " + layerName + "         " + myLayer);
        writeLayerFile(myLayer, new File(physicalLayerFileDirectory, layerIndex + "_" + layerName + lyrFileExtension));
        writeLayerFile(myLayer, new File(physicalLayerFileDirectory, layerIndex + lyrFileExtension));

        if (myLayer instanceof FDOGraphicsLayer) {
            FDOGraphicsLayer fdoGraphicsLayer = (FDOGraphicsLayer) myLayer;
            writeFDOGraphicLayerFile(fdoGraphicsLayer, layerIndex);
            for (int i = 0; i < fdoGraphicsLayer.getCount(); i++) {
                layerIndex++;
            }
        } else if (myLayer instanceof ICompositeLayer) {
            ICompositeLayer groupLayer = (ICompositeLayer) myLayer;
            logger.debug(TAG, "exportLayerFile.groupLayer found [" + layerName + "] with [" + groupLayer.getCount() + "] childs");
            for (int i = 0; i < groupLayer.getCount(); i++) {
                layerIndex++;
                layerIndex = exportLayerFile(layerIndex, groupLayer.getLayer(i));
            }
        }
        if (!layerVisibility) myLayer.setVisible(false);
        return layerIndex;
    }

    private void writeFDOGraphicLayerFile(FDOGraphicsLayer fdoGraphicsLayer, int layerIndex) throws IOException {
        fdoGraphicsLayer.setVisible(true);
        int subLayerCount = fdoGraphicsLayer.getCount();
        boolean[] origState = new boolean[subLayerCount];
        for (int i = 0; i < subLayerCount; i++) {
            origState[i] = fdoGraphicsLayer.getLayer(i).isVisible();
        }
        String layerName = fdoGraphicsLayer.getName();
        layerName = layerName.replaceAll("[^a-zA-Z0-9]", "_");
        writeLayerFile(fdoGraphicsLayer, new File(physicalLayerFileDirectory, layerIndex + "_" + layerName + lyrFileExtension));
        writeLayerFile(fdoGraphicsLayer, new File(physicalLayerFileDirectory, layerIndex + lyrFileExtension));
        for (int i = 0; i < subLayerCount; i++) {
            for (int j = 0; j < subLayerCount; j++) {
                ILayer subLayer = fdoGraphicsLayer.getLayer(j);
                subLayer.setVisible(i == j);
            }
            String subLayerName = layerName + " [" + i + "]";
            int subLayerIndex = layerIndex + 1 + i;
            writeLayerFile(fdoGraphicsLayer, new File(physicalLayerFileDirectory, subLayerIndex + "_" + subLayerName + lyrFileExtension));
            writeLayerFile(fdoGraphicsLayer, new File(physicalLayerFileDirectory, subLayerIndex + lyrFileExtension));
        }
        for (int i = 0; i < subLayerCount; i++) {
            fdoGraphicsLayer.getLayer(i).setVisible(origState[i]);
        }
    }


    private void writeLayerFile(ILayer myLayer, File lyrFile) throws IOException {
        logger.debug(TAG, "writeLayerFile.lyrFile", lyrFile);
        if (lyrFile.exists()) {
            boolean deleted = lyrFile.delete();
            logger.debug(TAG, "writeLayerFile.lyrFileAlreadyExisted.deleted", deleted);
        }

        ILayerFile layerFile = new LayerFile();
        layerFile.esri_new(lyrFile.getAbsolutePath());
        layerFile.replaceContents(myLayer);
        layerFile.save();
        layerFile.close();

        logger.debug(TAG, "lyrFile written", lyrFile);
    }

    public String getLayerLyrFile(String layerId) {
        String virtualLayerFileDirectory = getVirtualMapServiceOutputFolder(lyrSubDirectoryName);
        logger.debug(TAG, "getLayerLyrFile.virtualLayerFileDirectory", virtualLayerFileDirectory);
        logger.debug(TAG, "getLayerLyrFile.layerId", layerId);
        logger.debug(TAG, "getLayerLyrFile.lyrFileExtension", lyrFileExtension);
        String retFile = virtualLayerFileDirectory + "/" + layerId + lyrFileExtension;
        JSONObject answerObject = new JSONObject();
        answerObject.put("layerFile", retFile);
        return answerObject.toString();
    }
}
