/*
 *
 *  *  PCHPrintSOE - Advanced printing SOE for ArcGIS Server
 *  *  Copyright (C) 2010-2012 Tom Schuller
 *  *
 *  *  This program is free software: you can redistribute it and/or modify
 *  *  it under the terms of the GNU Lesser General Public License as published by
 *  *  the Free Software Foundation, either version 3 of the License, or
 *  *  (at your option) any later version.
 *  *
 *  *  This program is distributed in the hope that it will be useful,
 *  *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  *  GNU Lesser General Public License for more details.
 *  *
 *  *  You should have received a copy of the GNU Lesser General Public License
 *  *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package lu.etat.pch.gis.soe.tasks;

import com.esri.arcgis.carto.FeatureLayer;
import com.esri.arcgis.carto.ILayer;
import com.esri.arcgis.carto.MapServer;
import com.esri.arcgis.geodatabase.IMetadata;
import com.esri.arcgis.geodatabase.IXmlPropertySet2;
import com.esri.arcgis.system.IName;
import lu.etat.pch.gis.utils.SOELogger;

import java.io.IOException;

/**
 * Created by IntelliJ IDEA.
 * User: schullto
 * Date: May 12, 2010
 * Time: 8:03:15 AM
 */
public class MetadataTask extends AbstractTask {
    private static final String TAG = "MetadataTask";

    public MetadataTask(SOELogger logger, MapServer mapServer, String mapServiceName) {
        super(logger, mapServer, mapServiceName);
    }

    public String getMetadataByID(int layerId) throws IOException {
        logger.debug(TAG, "getMetadataByID.layerId", layerId);
        ILayer layer = map.getLayer(layerId);
        logger.debug(TAG, "getMetadataByID.layer", layer);
        if (layer instanceof FeatureLayer) {
            FeatureLayer fLayer = (FeatureLayer) layer;
            logger.debug(TAG, "getMetadataByID.fLayer", fLayer);
            IName fLayerDSName = fLayer.getDataSourceName();
            logger.debug(TAG, "getMetadataByID.fLayerDSName", fLayerDSName);
            if (fLayerDSName instanceof IMetadata) {
                IMetadata metatdaDataSource = (IMetadata) fLayerDSName;
                logger.debug(TAG, "getMetadataByID.metatdaDataSource", metatdaDataSource);
                IXmlPropertySet2 pPropertySet = (IXmlPropertySet2) metatdaDataSource.getMetadata();
                logger.debug(TAG, "getMetadataByID.pPropertySet", pPropertySet);
                if (pPropertySet != null)
                    return pPropertySet.getXml("");
            }
        }
        return "";
    }

}
