/*
 *
 *  *  PCHPrintSOE - Advanced printing SOE for ArcGIS Server
 *  *  Copyright (C) 2010-2012 Tom Schuller
 *  *
 *  *  This program is free software: you can redistribute it and/or modify
 *  *  it under the terms of the GNU Lesser General Public License as published by
 *  *  the Free Software Foundation, either version 3 of the License, or
 *  *  (at your option) any later version.
 *  *
 *  *  This program is distributed in the hope that it will be useful,
 *  *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  *  GNU Lesser General Public License for more details.
 *  *
 *  *  You should have received a copy of the GNU Lesser General Public License
 *  *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package lu.etat.pch.gis.soeClients;

import com.esri.arcgis.interop.extn.ArcGISExtension;
import com.esri.arcgis.server.json.JSONException;
import lu.etat.pch.gis.beans.height.HeightResultBean;
import lu.etat.pch.gis.beans.pk.PKResultBean;

import java.io.IOException;

/**
 * Created by IntelliJ IDEA.
 * User: schullto
 * Date: Feb 23, 2010
 * Time: 9:28:41 PM
 */
@ArcGISExtension
public interface IPCHExportSOE {
    public String getMetadataByID(int layerId) throws IOException;

    public PKResultBean[] getPKsByCoord(double coordX, double coordY, String wkid, double tolerance, int maxRecords) throws IOException;

    public double getHeightForPoint(double coordX, double coordY) throws IOException;

    public HeightResultBean[] getHeightForLine(String lineGeom) throws IOException, JSONException;

    public String exportLayers();

    public int getLayerCount() throws IOException;
}
