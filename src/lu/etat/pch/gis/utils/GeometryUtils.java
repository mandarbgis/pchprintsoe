/*
 *
 *  *  PCHPrintSOE - Advanced printing SOE for ArcGIS Server
 *  *  Copyright (C) 2010-2012 Tom Schuller
 *  *
 *  *  This program is free software: you can redistribute it and/or modify
 *  *  it under the terms of the GNU Lesser General Public License as published by
 *  *  the Free Software Foundation, either version 3 of the License, or
 *  *  (at your option) any later version.
 *  *
 *  *  This program is distributed in the hope that it will be useful,
 *  *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  *  GNU Lesser General Public License for more details.
 *  *
 *  *  You should have received a copy of the GNU Lesser General Public License
 *  *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package lu.etat.pch.gis.utils;

import com.esri.arcgis.geometry.*;

import java.io.IOException;

/**
 * Created by IntelliJ IDEA.
 * User: schullto
 * Date: 3/15/11
 * Time: 6:49 AM
 */
public class GeometryUtils {
    public static IGeometry convertEnvelope2Polygon(Envelope geom) throws IOException {
        Polygon polygon = new Polygon();
        polygon.addPoint(geom.getUpperLeft(), null, null);
        polygon.addPoint(geom.getUpperRight(), null, null);
        polygon.addPoint(geom.getLowerRight(), null, null);
        polygon.addPoint(geom.getLowerLeft(), null, null);
        polygon.addPoint(geom.getUpperLeft(), null, null);
        return polygon;
    }

    public static IEnvelope convertPoint2Envelope(IPoint pt, double buffer) throws IOException {
        double x = pt.getX();
        double y = pt.getY();
        double offset = buffer / 2;
        Polygon polygon = new Polygon();
        polygon.addPoint(createPoint(x - offset, y - offset), null, null);
        polygon.addPoint(createPoint(x - offset, y + offset), null, null);
        polygon.addPoint(createPoint(x + offset, y + offset), null, null);
        polygon.addPoint(createPoint(x + offset, y - offset), null, null);
        polygon.addPoint(createPoint(x - offset, y - offset), null, null);
        return polygon.getEnvelope();
    }

    public static Point createPoint(double x, double y) throws IOException {
        Point pt = new Point();
        pt.putCoords(x, y);
        return pt;
    }
}
