/*
 *
 *  *  PCHPrintSOE - Advanced printing SOE for ArcGIS Server
 *  *  Copyright (C) 2010-2012 Tom Schuller
 *  *
 *  *  This program is free software: you can redistribute it and/or modify
 *  *  it under the terms of the GNU Lesser General Public License as published by
 *  *  the Free Software Foundation, either version 3 of the License, or
 *  *  (at your option) any later version.
 *  *
 *  *  This program is distributed in the hope that it will be useful,
 *  *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  *  GNU Lesser General Public License for more details.
 *  *
 *  *  You should have received a copy of the GNU Lesser General Public License
 *  *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package lu.etat.pch.gis.utils;

import java.io.*;

/**
 * implements some features found RESTUtils, but can handle different URL "schemas"
 * Delegates work to RESTUtils for http-urls/schemas, but also handles classpath and file-resources
 * (see definied constants/schema-prefixes)
 *
 * @author abajramovic
 */
public class IOUtils {
    private static final String TAG = "IOUtils";
    public static String CLASSPATH_PREFIX = "classpath:";
    public static String FILE_PREFIX = "file:";
    public static String HTTP_PREFIX = "http:";
    public static String HTTPS_PREFIX = "https:";

    /**
     * always returns a copy of the resource, you the consumer doesn't have to worry
     * about deleting something
     *
     * @param logger
     * @param url
     * @param target
     */
    public static void getResourceAsFile(SOELogger logger, String url, File target, String referer, String authentication401) {
        if (url == null) {
            return;
        }
        try {
            if (url.startsWith(HTTP_PREFIX) || url.toLowerCase().startsWith(HTTPS_PREFIX)) {
                RESTUtils.downloadFile(logger, url, target, referer, authentication401);
            } else if (url.startsWith(CLASSPATH_PREFIX)) {
                FileOutputStream out = new FileOutputStream(target, false);
                InputStream in = IOUtils.class.getResourceAsStream(stripResourceUrl(url));
                copyAndCloseStreams(in, out);
            } else if (url.startsWith(FILE_PREFIX)) {
                //we need to copy the file, so the consumer can savely delete the (temp) file
                FileInputStream in = new FileInputStream(stripResourceUrl(url));
                FileOutputStream out = new FileOutputStream(target, false);
                copyAndCloseStreams(in, out);
            }
        } catch (IOException ex) {
            throw new RuntimeException(ex);
        }
    }

    public static boolean resourceExists(SOELogger logger, String url, String authentication401) {
        if (url == null) {
            return false;
        }
        try {
            if (url.toLowerCase().startsWith(HTTP_PREFIX) || url.toLowerCase().startsWith(HTTPS_PREFIX)) {
                return RESTUtils.urlExists(logger, url, null, authentication401);
            } else if (url.toLowerCase().startsWith(CLASSPATH_PREFIX)) {
                InputStream in = IOUtils.class.getResourceAsStream(stripResourceUrl(url));
                if (in == null) {
                    return false;
                } else {
                    in.close();
                    return true;
                }
            } else if (url.startsWith(FILE_PREFIX)) {
                return new File(stripResourceUrl(url)).exists();
            } else {
                return false;
            }
        } catch (IOException ex) {
            logger.error(TAG, "could not open Resource at " + url + " cause:" + ex.getMessage());
            return false;
        }

    }

    private static String stripResourceUrl(String url) {
        if (url.toLowerCase().startsWith(CLASSPATH_PREFIX)) {
            return url.substring(CLASSPATH_PREFIX.length(), url.lastIndexOf(".lyr") + 4);
        } else if (url.toLowerCase().startsWith(FILE_PREFIX)) {
            return url.substring(FILE_PREFIX.length(), url.lastIndexOf(".lyr") + 4);
        } else {
            return url;
        }
    }

    private static void copyAndCloseStreams(InputStream in, OutputStream out) throws IOException {
        int b;
        try {
            while ((b = in.read()) > -1) {
                out.write(b);
            }
        } finally {
            out.close();
            in.close();
        }
    }
}
