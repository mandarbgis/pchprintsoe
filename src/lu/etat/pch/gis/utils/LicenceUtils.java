/*
 *
 *  *  PCHPrintSOE - Advanced printing SOE for ArcGIS Server
 *  *  Copyright (C) 2010-2012 Tom Schuller
 *  *
 *  *  This program is free software: you can redistribute it and/or modify
 *  *  it under the terms of the GNU Lesser General Public License as published by
 *  *  the Free Software Foundation, either version 3 of the License, or
 *  *  (at your option) any later version.
 *  *
 *  *  This program is distributed in the hope that it will be useful,
 *  *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  *  GNU Lesser General Public License for more details.
 *  *
 *  *  You should have received a copy of the GNU Lesser General Public License
 *  *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package lu.etat.pch.gis.utils;

import com.esri.arcgis.system.AoInitialize;
import com.esri.arcgis.system.EngineInitializer;
import com.esri.arcgis.system.esriLicenseStatus;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Method;
import java.net.URL;
import java.net.URLClassLoader;

/**
 * Created by IntelliJ IDEA.
 * User: schullto
 * Date: May 21, 2010
 * Time: 6:33:41 PM
 */
public class LicenceUtils {
    public static int esriLicenseProductCodeArcInfo = 60;
    public static int esriLicenseProductCodeArcEditor = 50;
    public static int esriLicenseProductCodeArcView = 40;
    public static int esriLicenseProductCodeArcServer = 30;
    public static int esriLicenseProductCodeEngineGeoDB = 20;
    public static int esriLicenseProductCodeEngine = 10;

    //new in ArcObject10.1
    public static int esriLicenseProductCodeAdvanced = 60;
    public static int esriLicenseProductCodeStandard = 50;
    public static int esriLicenseProductCodeBasic = 40;

    public static AoInitialize initArcEngine(int licenceCode) throws IOException {
        bootstrapArcobjectsJar();
        EngineInitializer.initializeVisualBeans();
        final AoInitialize aoInit = new AoInitialize();
        aoInit.initialize(licenceCode);
        return aoInit;
    }

    public static AoInitialize initArcEngine() throws IOException {
        bootstrapArcobjectsJar();
        EngineInitializer.initializeVisualBeans();
        final AoInitialize aoInit = new AoInitialize();

        int[] licenceCodes = new int[]{
                esriLicenseProductCodeArcInfo,
                esriLicenseProductCodeArcEditor,
                esriLicenseProductCodeArcView,
                esriLicenseProductCodeArcServer,
                esriLicenseProductCodeEngineGeoDB,
                esriLicenseProductCodeEngine,
        };

        for (int licenceCode : licenceCodes) {
            if (aoInit.isProductCodeAvailable(licenceCode) == esriLicenseStatus.esriLicenseAvailable) {
                System.out.println("initialized with licenceCode = " + licenceCode);
                aoInit.initialize(licenceCode);
                return aoInit;
            }
        }
        System.err.println("No valid licence found !!");
        return aoInit;
    }

    public static void bootstrapArcobjectsJar() {
        String arcObjectsHome = System.getenv("AGSENGINEJAVA");
        if (arcObjectsHome == null) {
            arcObjectsHome = System.getenv("AGSDESKTOPJAVA");
        }
        if (arcObjectsHome == null) {
            if (System.getProperty("os.name").toLowerCase().indexOf("win") > -1) {
                System.err.println("You must have ArcGIS Engine Runtime or ArcGIS Desktop installed in order to execute this sample.");
                System.err.println("Install one of the products above, then re-run this sample.");
                System.err.println("Exiting execution of this sample...");
                System.exit(0);
            } else {
                System.err.println("You must have ArcGIS Engine Runtime installed in order to execute this sample.");
                System.err.println("Install the product above, then re-run this sample.");
                System.err.println("Exiting execution of this sample...");
                System.exit(0);
            }
        }
        String jarPath = arcObjectsHome + "java" + File.separator + "lib" + File.separator + "arcobjects.jar";
        File jarFile = new File(jarPath);
        if (!jarFile.exists()) {
            System.err.println("The arcobjects.jar was not found in the following location: " + jarFile.getParent());
            System.err.println("Verify that arcobjects.jar can be located in the specified folder.");
            System.err.println("If not present, try uninstalling your ArcGIS software and reinstalling it.");
            System.err.println("Exiting execution of this sample...");
            System.exit(0);
        }

        URLClassLoader sysloader = (URLClassLoader) ClassLoader.getSystemClassLoader();
        Class<URLClassLoader> sysclass = URLClassLoader.class;
        try {
            Method method = sysclass.getDeclaredMethod("addURL", new Class[]{URL.class});
            method.setAccessible(true);
            method.invoke(sysloader, new Object[]{jarFile.toURI().toURL()});
        } catch (Throwable throwable) {
            throwable.printStackTrace();
            System.err.println("Could not add arcobjects.jar to system classloader");
            System.err.println("Exiting execution of this sample...");
            System.exit(0);
        }
    }

}
