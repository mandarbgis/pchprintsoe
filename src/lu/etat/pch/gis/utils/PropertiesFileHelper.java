/*
 *
 *  *  PCHPrintSOE - Advanced printing SOE for ArcGIS Server
 *  *  Copyright (C) 2010-2012 Tom Schuller
 *  *
 *  *  This program is free software: you can redistribute it and/or modify
 *  *  it under the terms of the GNU Lesser General Public License as published by
 *  *  the Free Software Foundation, either version 3 of the License, or
 *  *  (at your option) any later version.
 *  *
 *  *  This program is distributed in the hope that it will be useful,
 *  *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  *  GNU Lesser General Public License for more details.
 *  *
 *  *  You should have received a copy of the GNU Lesser General Public License
 *  *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package lu.etat.pch.gis.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

/**
 * User: schullto
 * Date: 03/02/2015
 * Time: 5:28 PM
 */
public class PropertiesFileHelper {
    private static final String TAG = PropertiesFileHelper.class.getName();
    private static final String propFileName = new String("\\pchprintsoe.properties");
    private static Properties config = null;

    public String getValue(SOELogger logger, String key) {
        Properties config = getPropertiesFromFile(logger);
        String value = config.getProperty(key);
        if (value != null) {
            return value;
        }
        logger.debug(TAG, "getValue.valueIsNULLforKey", key);
        logger.debug(TAG, "getValue.propFile", new File(propFileName));
        return null;
    }

    private Properties getPropertiesFromFile(SOELogger logger) {
        if (config == null) {
            logger.debug(TAG,"getPropertiesFromFile.loadingProperties...");
            try {
                config = new Properties();
                File propFile = new File(propFileName);
                logger.debug(TAG,"getPropertiesFromFile.propFile: ",propFile);
                if (propFile.canRead()) {
                    FileInputStream fis = new FileInputStream(propFile);
                    Properties tmpProp = new Properties();
                    tmpProp.load(fis);
                    for (Object key : tmpProp.keySet()) {
                        String value = tmpProp.getProperty(key.toString());
                        config.put(key.toString(), value);
                    }
                    fis.close();
                }
                propFile = null;
                //from "http://downloads2.esri.com/resources/arcgisdesktop/layers/Bing_Maps_Aerial.lyr";
                //from "http://downloads2.esri.com/resources/arcgisdesktop/layers/Bing_Maps_Road.lyr";
                //from "http://downloads2.esri.com/resources/arcgisdesktop/layers/Bing_Maps_Hybrid.lyr";
                config.put("BING_http://bing.com_aerial", "classpath:/layerfiles/Bing_Maps_Aerial.lyr");
                config.put("BING_http://bing.com_road", "classpath:/layerfiles/Bing_Maps_Road.lyr");
                config.put("BING_http://bing.com_hybrid", "classpath:/layerfiles/Bing_Maps_Hybrid.lyr");
                config.put("BING_http://bing.com_aerialWithLabels", "classpath:/layerfiles/Bing_Maps_Hybrid.lyr");
                //from "https://ago-item-storage.s3.amazonaws.com/3daa73d933b7415997c37145a6094fe0/OpenStreetMap.lyr?AWSAccessKeyId=AKIAJS2Y2E72HYCOE7BA&Expires=1322158388&Signature=R8ywdRWSP76nkP%2FRmuJRQzMXlPI%3D";
                config.put("OSM_", "classpath:/layerfiles/OpenStreetMap.lyr");
                logger.debug(TAG,"getPropertiesFromFile.LOADED.configSize: ",config.size());
            } catch (IOException ex) {
            }
        }
        return config;
    }
}
