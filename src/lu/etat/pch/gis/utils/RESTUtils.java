/*
 *
 *  *  PCHPrintSOE - Advanced printing SOE for ArcGIS Server
 *  *  Copyright (C) 2010-2012 Tom Schuller
 *  *
 *  *  This program is free software: you can redistribute it and/or modify
 *  *  it under the terms of the GNU Lesser General Public License as published by
 *  *  the Free Software Foundation, either version 3 of the License, or
 *  *  (at your option) any later version.
 *  *
 *  *  This program is distributed in the hope that it will be useful,
 *  *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  *  GNU Lesser General Public License for more details.
 *  *
 *  *  You should have received a copy of the GNU Lesser General Public License
 *  *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package lu.etat.pch.gis.utils;

/**
 * Created by IntelliJ IDEA.
 * User: schullto
 * Date: Apr 08, 2010
 * Time: 6:01:55 AM
 */


import com.esri.arcgis.server.json.JSONArray;
import com.esri.arcgis.server.json.JSONException;
import com.esri.arcgis.server.json.JSONObject;
import com.esri.arcgis.server.json.JSONTokener;

import javax.net.ssl.*;
import java.io.*;
import java.net.*;
import java.security.SecureRandom;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;

public class RESTUtils {
    private static int connectTimeOut = 5000;
    private static final String TAG = "RESTUtils";

    public static void main(String[] args) {
        try {
            String url = "http://bitbucket.schuller.lu";
            downloadFile(new SOELogger(1234), url, new File("c:\\test.lyr"), url, null);
            //System.out.println("content = " + content);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public static String sendError(int code, String message, String details[]) {
        try {
            JSONObject errorObject = new JSONObject();
            JSONObject error = new JSONObject();
            error.put("code", code);
            error.put("message", message);
            if (details != null) {
                JSONArray detailsArray = new JSONArray();
                for (String detail : details) {
                    detailsArray.put(detail);
                }
                error.put("details", detailsArray);
            }
            errorObject.put("warning", error);
            return errorObject.toString();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String getCorrectedOutputFormat(String operationInputJSON) {
        JSONTokener tokener = new JSONTokener(operationInputJSON);
        JSONObject parameterObject = new JSONObject(tokener);
        if (parameterObject.has("f"))
            return (String) parameterObject.get("f");
        return null;
    }

    public static String[] getParameterValueFromOperationInput(String operationInputJSON, String parameterNames[]) {
        try {
            JSONTokener tokener = new JSONTokener(operationInputJSON);
            String parameterValues[] = new String[parameterNames.length];
            JSONObject parameterObject = new JSONObject(tokener);
            for (int i = 0; i < parameterValues.length; i++) {
                if (parameterObject.has(parameterNames[i]))
                    parameterValues[i] = parameterObject.getString(parameterNames[i]);
            }
            return parameterValues;
        } catch (JSONException e) {
            sendError(0, "Incorrect parameter name provided.", new String[]{
                    (new StringBuilder()).append("Cause: ").append(e.getCause().getMessage()).toString(), e.getMessage()
            });
        }
        return null;
    }

    public static JSONObject createOperation(String operationName, String parameterList, String supportedOutputFormatsList) {
        try {
            JSONObject operation = new JSONObject();
            if (operationName != null && operationName.length() > 0)
                operation.put("name", operationName);
            else
                throw new Exception("Operation must have valid name");
            if (parameterList != null && parameterList.length() > 0) {
                JSONArray operationParamArray = new JSONArray();
                String parameters[] = parameterList.split(",");
                for (String parameter : parameters) {
                    operationParamArray.put(parameter.trim());
                }
                operation.put("parameters", operationParamArray);
            } else {
                throw new Exception("Operation must have parameters. If your operation does not requires params, please convert it to a sub-resource!"
                );
            }
            if (supportedOutputFormatsList != null && supportedOutputFormatsList.length() > 0) {
                JSONArray outputFormatsArray = new JSONArray();
                String outputFormats[] = supportedOutputFormatsList.split(",");
                int len$ = outputFormats.length;
                for (int i$ = 0; i$ < len$; i$++) {
                    String outputFormat = outputFormats[i$];
                    outputFormatsArray.put(outputFormat.trim());
                }

                operation.put("supportedOutputFormats", outputFormatsArray);
            } else {
                throw new Exception("Operation must have supported output formats specified!");
            }
            return operation;
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

    public static JSONObject createOperation(String operationName, String[] parameterList, String supportedOutputFormatsList) {
        StringBuffer paramNames = new StringBuffer();
        for (String paramName : parameterList) {
            paramNames.append(paramName).append(",");
        }
        return createOperation(operationName, paramNames.toString(), supportedOutputFormatsList);
    }

    public static JSONObject createResource(String name, String description, boolean isCollection) {
        try {
            JSONObject json = new JSONObject();
            if (name.length() > 0 && name != null) {
                json.put("name", name);
            } else {
                throw new Exception("Resource must have a valid name.");
            }
            json.put("description", description);
            json.put("isCollection", isCollection);
            return json;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static boolean urlExists(SOELogger logger, String url, String refererWebsite, String authorization401) {
        logger.debug(TAG, "urlExists.... url=" + url);
        if (url.startsWith(IOUtils.CLASSPATH_PREFIX)) {
            return IOUtils.resourceExists(logger, url, authorization401);
        }
        String content = downloadContent(logger, url, refererWebsite, authorization401);
        if (content != null && content.trim().length() > 0) {
            logger.debug(TAG, "urlExists.content.size: " + content.length());
            boolean errorCode400 = content.toLowerCase().contains("\"code\":400,");
            logger.debug(TAG, "urlExists.content.errorCode400: " + errorCode400);
            if (errorCode400) return false;
            boolean errorCode500 = content.toLowerCase().contains("\"code\":500,");
            logger.debug(TAG, "urlExists.content.errorCode500: " + errorCode500);
            if (errorCode500) return false;
            boolean errorNotFound = content.toLowerCase().contains("extension not found");
            logger.debug(TAG, "urlExists.content.errorNotFound", errorNotFound);
            if (errorNotFound) return false;
            return true;
        } else {
            logger.debug(TAG, "urlExists.content: " + content + "  -> returning false");
            return false;
        }
    }

    public static String downloadContent(SOELogger logger, String url, String referer, String authorization401) {
        logger.debug(TAG, "downloadContent.authorization401", authorization401);
        logger.debug(TAG, "downloadContent.url", url);

        // ====
        try {
            SSLContext ctx = SSLContext.getInstance("TLS");
            ctx.init(new KeyManager[0], new TrustManager[]{new X509TrustManager() {
                @Override
                public void checkClientTrusted(X509Certificate[] arg0, String arg1) throws CertificateException {
                }

                @Override
                public void checkServerTrusted(X509Certificate[] arg0, String arg1) throws CertificateException {
                }

                @Override
                public X509Certificate[] getAcceptedIssuers() {
                    return null;
                }
            }}, new SecureRandom());
            SSLContext.setDefault(ctx);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        //==end test

        if (url.startsWith(IOUtils.CLASSPATH_PREFIX)) {
            StringWriter sw = new StringWriter();
            PrintWriter pw = new PrintWriter(sw);
        }

        String useSystemProxies = System.getProperty("java.net.useSystemProxies");
        if (url != null) {
            BufferedInputStream bis = null;
            try {
                HttpURLConnection con = (HttpURLConnection) new URL(url).openConnection();
                con.setConnectTimeout(connectTimeOut);
                //test
                if (con instanceof HttpsURLConnection) {
                    HttpsURLConnection httpsCon = (HttpsURLConnection) con;
                    httpsCon.setHostnameVerifier(new HostnameVerifier() {
                        @Override
                        public boolean verify(String arg0, SSLSession arg1) {
                            return true;
                        }
                    });
                }

                //end test
                if (referer != null && referer.trim().length() > 0) {
                    logger.debug(TAG, "downloadContent.referer: Adding referer >" + referer + "<");
                    con.addRequestProperty("REFERER", referer);
                }
                if (authorization401 != null) {
                    con.setRequestProperty("Authorization", authorization401);
                }

                bis = new BufferedInputStream(con.getInputStream());
                BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
                StringBuilder sb = new StringBuilder();
                String inputLine;
                while ((inputLine = in.readLine()) != null)
                    sb.append(inputLine);
                in.close();
                String retContent = sb.toString();
                logger.debug(TAG, "downloadContent.NOProxy.retContent", retContent);
                logger.debug(TAG, "downloadContent.NOProxy.retContent.size", retContent.length());
                return retContent;
            } catch (Exception ex) {
                logger.debug(TAG, "downloadContent.IOException", ex.getMessage());
                if (ex.getMessage().contentEquals("Server returned HTTP response code 500")) {
                    return null;
                }
                if (useSystemProxies == null || useSystemProxies.equalsIgnoreCase("false"))
                    System.setProperty("java.net.useSystemProxies", "true");
                else
                    System.setProperty("java.net.useSystemProxies", "false");
                logger.debug(TAG, "downloadContent.useSystemProxies=" + System.getProperty("java.net.useSystemProxies"));
                try {
                    URLConnection con = (URLConnection) new URL(url).openConnection();
                    con.setConnectTimeout(connectTimeOut);
                    if (referer != null) {
                        logger.debug(TAG, "downloadContent.referer: Adding referer >" + referer + "<");
                        con.addRequestProperty("REFERER", referer);
                    }
                    if (authorization401 != null) {
                        con.setRequestProperty("Authorization", authorization401);
                    }
                    bis = new BufferedInputStream(con.getInputStream());
                    BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
                    StringBuilder sb = new StringBuilder();
                    String inputLine;
                    while ((inputLine = in.readLine()) != null)
                        sb.append(inputLine);
                    in.close();
                    String retContent = sb.toString();
                    logger.debug(TAG, "downloadContent.2Proxy.retContent: " + retContent);
                    logger.debug(TAG, "downloadContent.2Proxy.retContent.size: " + retContent.length());
                    if (useSystemProxies != null) {
                        System.setProperty("java.net.useSystemProxies", useSystemProxies);
                    }
                    return retContent;
                } catch (SocketTimeoutException e) {
                    //logger.error(TAG,"downloadContent.noProxy.SocketTimeoutException", e);
                } catch (IOException e) {
                    //logger.error(TAG,"downloadContent.noProxy.IOException", e);
                }
            } finally {
                if (bis != null)
                    try {
                        bis.close();
                    } catch (IOException e) {
                        logger.error(TAG, "downloadContent.IOException2", e);
                    }
            }
        }
        logger.warning(TAG, "downloadContent.url = null, return empty string");
        if (useSystemProxies == null)
            System.setProperty("java.net.useSystemProxies", "false");
        else {
            System.setProperty("java.net.useSystemProxies", useSystemProxies);
        }
        return null;
    }

    public static void downloadFile(SOELogger logger, String fileUrl, File destination, String referer, String authorization401) {
        logger.debug(TAG, "RESTUtils.downloadFile.fileUrl: " + fileUrl);
        if (fileUrl.startsWith(IOUtils.CLASSPATH_PREFIX) || fileUrl.startsWith(IOUtils.FILE_PREFIX)) {
            IOUtils.getResourceAsFile(logger, fileUrl, destination, referer, authorization401);
            return;
        }
        if (fileUrl.indexOf("&amp;") >= 0) {
            fileUrl = fileUrl.replaceAll("&amp;", "&");
            logger.debug(TAG, "downloadFile.fileUrl_&amp_replaced: " + fileUrl);
        }
        logger.debug(TAG, "RESTUtils.downloadFile.destination: " + destination);
        String useSystemProxies = System.getProperty("java.net.useSystemProxies");
        logger.debug(TAG, "RESTUtils.downloadFile.useSystemProxies: " + useSystemProxies);
        if (fileUrl != null && destination != null) {
            BufferedInputStream bis = null;
            BufferedOutputStream bos = null;
            try {
                URL url = new URL(fileUrl);
                HttpURLConnection con = (HttpURLConnection) url.openConnection();
                if (authorization401 != null) {
                    con.setRequestProperty("Authorization", authorization401);
                }
                con.setConnectTimeout(connectTimeOut);
                if (referer != null && referer.trim().length() > 0) {
                    logger.debug(TAG, "downloadFile.referer: Adding referer >" + referer + "<");
                    con.addRequestProperty("REFERER", referer);
                }
                bis = new BufferedInputStream(con.getInputStream());
                bos = new BufferedOutputStream(new FileOutputStream(destination));
                int i;
                while ((i = bis.read()) != -1) {
                    bos.write(i);
                }
            } catch (FileNotFoundException e) {
                logger.error(TAG, "downloadFile.FileNotFoundException", e);
            } catch (MalformedURLException e) {
                logger.error(TAG, "downloadFile.MalformedURLException", e);
            } catch (IOException e) {
                logger.warning(TAG, "downloadFile.tryingWithInverseProxySettings");
                try {
                    if (useSystemProxies == null || useSystemProxies.equalsIgnoreCase("true"))
                        System.setProperty("java.net.useSystemProxies", "true");
                    else
                        System.setProperty("java.net.useSystemProxies", "false");
                    logger.debug(TAG, "downloadFile.useSystemProxies=" + System.getProperty("java.net.useSystemProxies"));
                    URL url = new URL(fileUrl);
                    HttpURLConnection con = (HttpURLConnection) url.openConnection();
                    con.setConnectTimeout(connectTimeOut);
                    if (referer != null && referer.trim().length() > 0) {
                        logger.debug(TAG, "downloadFile.referer: Adding referer >" + referer + "<");
                        con.addRequestProperty("REFERER", referer);
                    }
                    if (authorization401 != null) {
                        con.setRequestProperty("Authorization", authorization401);
                    }
                    bis = new BufferedInputStream(con.getInputStream());
                    bos = new BufferedOutputStream(new FileOutputStream(destination));
                    int i;
                    while ((i = bis.read()) != -1) {
                        bos.write(i);
                    }
                } catch (FileNotFoundException e1) {
                    logger.error(TAG, "downloadFile.noProxy.FileNotFoundException", e1);
                } catch (IOException e1) {
                    logger.error(TAG, "downloadFile.noProxy.IOException", e1);
                }
            } finally {
                if (bis != null)
                    try {
                        bis.close();
                    } catch (IOException ioe) {
                        ioe.printStackTrace();
                    }
                if (bos != null)
                    try {
                        bos.close();
                    } catch (IOException ioe) {
                        ioe.printStackTrace();
                    }
            }
        }
        if (useSystemProxies == null)
            System.setProperty("java.net.useSystemProxies", "false");
        else {
            System.setProperty("java.net.useSystemProxies", useSystemProxies);
        }
    }
}
  