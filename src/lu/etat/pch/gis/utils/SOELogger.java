/*
 *
 *  *  PCHPrintSOE - Advanced printing SOE for ArcGIS Server
 *  *  Copyright (C) 2010-2012 Tom Schuller
 *  *
 *  *  This program is free software: you can redistribute it and/or modify
 *  *  it under the terms of the GNU Lesser General Public License as published by
 *  *  the Free Software Foundation, either version 3 of the License, or
 *  *  (at your option) any later version.
 *  *
 *  *  This program is distributed in the hope that it will be useful,
 *  *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  *  GNU Lesser General Public License for more details.
 *  *
 *  *  You should have received a copy of the GNU Lesser General Public License
 *  *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package lu.etat.pch.gis.utils;

import com.esri.arcgis.carto.ILayer;
import com.esri.arcgis.carto.IMap;
import com.esri.arcgis.carto.Map;
import com.esri.arcgis.carto.MapFrame;
import com.esri.arcgis.geodatabase.IGPMessages;
import com.esri.arcgis.geometry.IEnvelope;
import com.esri.arcgis.geometry.IGeometry;
import com.esri.arcgis.geometry.Point;
import com.esri.arcgis.geometry.Polygon;
import com.esri.arcgis.interop.AutomationException;
import com.esri.arcgis.system.ILog;
import com.esri.arcgis.system.tagRECT;

import javax.swing.*;
import java.io.*;
import java.util.Date;

/**
 * Created by IntelliJ IDEA.
 * User: schullto
 * Date: Feb 23, 2010
 * Time: 8:53:38 PM
 */
public class SOELogger implements Serializable {
    public static boolean doOutput = true;
    public final static int ERROR = 1;
    public final static int WARNING = 2;
    public final static int NORMAL = 3;
    public final static int DETAILED = 4;
    public final static int DEBUG = 5;
    private ILog serverLog = null;
    private int serverLogCode;
    private IGPMessages gpMessages = null;
    private JTextArea textArea = null;

    public SOELogger(int serverLogCode) {
        this.serverLogCode = serverLogCode;
    }

    public SOELogger(ILog serverLog, int serverLogCode) {
        this(serverLogCode);
        this.serverLog = serverLog;
    }

    public SOELogger(IGPMessages messages, int serverLogCode) {
        this(serverLogCode);
        this.gpMessages = messages;
    }

    public SOELogger(JTextArea textArea, int serverLogCode) {
        this(serverLogCode);
        this.textArea = textArea;
        textArea.setText("");
    }

    private void log(int level, String msg) {
        if (doOutput) {
            if (gpMessages != null) {
                try {
                    gpMessages.addMessage(msg);
                } catch (AutomationException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (textArea != null) {
                String textAreaStr = textArea.getText();
                if (level == ERROR) {
                    textAreaStr += "ERROR: " + msg + "\n";
                } else {
                    textAreaStr += msg + "\n";
                }
                textArea.setText(textAreaStr);
                textArea.setCaretPosition(textAreaStr.length());
            }
            if (serverLog == null) {
                if (level == ERROR) {
                    System.err.println(serverLogCode + "  " + new Date() + "  " + "\t: " + msg);
                } else {
                    System.out.println(serverLogCode + "  " + new Date() + "  " + "\t: " + msg);
                }
            } else {
                try {
                    this.serverLog.addMessage(level, serverLogCode, msg);
                } catch (AutomationException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public void error(String prefix, String label, Object suffix) {
        label = prefix + "." + label;
        if (suffix == null) suffix = "<null>";
        else suffix = "'" + toString(suffix) + "'";
        log(ERROR, label + ": " + suffix);
    }

    public void error(String prefix, String label) {
        log(ERROR, prefix + "." + label);
    }

    public void warning(String prefix, String label, Object suffix) {
        label = prefix + "." + label;
        if (suffix == null) suffix = "<null>";
        else suffix = "'" + toString(suffix) + "'";
        log(WARNING, label + ": " + suffix);
    }

    public void warning(String prefix, String label) {
        log(WARNING, prefix + "." + label);
    }

    public void debug(String prefix, String label, Object suffix) {
        label = prefix + "." + label;
        if (suffix == null) suffix = "<null>";
        else suffix = "'" + toString(suffix) + "'";
        log(DEBUG, label + ": " + suffix);
    }

    public void debug(String prefix, String label) {
        log(DEBUG, prefix + "." + label);
    }

    public static String toString(Exception e) {
        StringBuilder sb = new StringBuilder();
        sb.append(e.getMessage());
        sb.append("[");
        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw);
        e.printStackTrace(pw);
        sb.append(sw.toString());
        sb.append("]");
        return sb.toString();
    }

    public static String toString(ILayer layer) throws IOException {
        return layer.getName() + "[" + layer.getClass() + "]";
    }

    public static String toString(IGeometry geom) throws IOException {
        if (geom instanceof IEnvelope) {
            IEnvelope env = (IEnvelope) geom;
            if (env.isEmpty())
                return "Envelope[<empty>]";
            else
                return "Envelope[" + env.getXMin() + ";" + env.getYMin() + " " + env.getXMax() + ";" + env.getYMax() + "]";
        } else if (geom instanceof Point) {
            Point point = (Point) geom;
            return "Point[" + point.getX() + ";" + point.getY() + "]";
        } else if (geom instanceof Polygon) {
            Polygon poly = (Polygon) geom;
            IEnvelope env = poly.getEnvelope();
            return "Polygon[" + toString(env) + " pointCount=" + poly.getPointCount() + "]";
        }
        return "??SOELogger.toString(IGeometry)??-" + geom;
    }

    public static String toString(MapFrame mapFrame) throws IOException {
        IGeometry geom = null;
        Double mapScale = null;
        try {
            geom = mapFrame.getGeometry();
        } catch (AutomationException ignored) {
        }
        try {
            mapScale = mapFrame.getMapScale();
        } catch (AutomationException ignored) {
        }
        //return "MapFrame[name='" + mapFrame.getName() + "' scale='" + mapScale + "'  map='" + toString(mapFrame.getMap()) + "' geometry='" + toString(geom) + "']";
        return "MapFrame[scale='" + mapScale + "'  map='" + toString(mapFrame.getMap()) + "' geometry='" + toString(geom) + "']";
    }

    public static String toString(IMap imap) throws IOException {
        Map map = (Map) imap;
        if (map == null) return "<null>";
        System.out.println("map = " + map);
        return "Map[ scale=" + map.getMapScale() + " extent='" + toString(map.getExtent()) + "' units=" + map.getMapUnits() + " distanceUnits=" + map.getDistanceUnits() + " layerCount=" + map.getLayerCount() + "]";
    }

    public static String toString(tagRECT rect) {
        return "tagRECT[t=" + rect.top + " b=" + rect.bottom + " l=" + rect.left + " r=" + rect.right + "]";
    }

    public static String toString(File file) {
        return "File[absPath=" + file.getAbsolutePath() + " canRead=" + file.canRead() + "]";
    }

    private static String toString(Object suffix) {
        if (suffix == null) return "<null>";
        try {
            if (suffix instanceof Exception)
                return toString((Exception) suffix);
            else if (suffix instanceof ILayer)
                return toString((ILayer) suffix);
            else if (suffix instanceof IGeometry)
                return toString((IGeometry) suffix);
            else if (suffix instanceof MapFrame)
                return toString((MapFrame) suffix);
            else if (suffix instanceof IMap)
                return toString((IMap) suffix);
            else if (suffix instanceof tagRECT)
                return toString((tagRECT) suffix);
            else if (suffix instanceof File)
                return toString((File) suffix);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return suffix.toString();
    }
}
