/*
 *
 *  *  PCHPrintSOE - Advanced printing SOE for ArcGIS Server
 *  *  Copyright (C) 2010-2012 Tom Schuller
 *  *
 *  *  This program is free software: you can redistribute it and/or modify
 *  *  it under the terms of the GNU Lesser General Public License as published by
 *  *  the Free Software Foundation, either version 3 of the License, or
 *  *  (at your option) any later version.
 *  *
 *  *  This program is distributed in the hope that it will be useful,
 *  *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  *  GNU Lesser General Public License for more details.
 *  *
 *  *  You should have received a copy of the GNU Lesser General Public License
 *  *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package lu.etat.pch.gis.utils.json.format;

import com.esri.arcgis.carto.*;
import com.esri.arcgis.server.json.JSONObject;
import com.esri.arcgis.support.ms.stdole.StdFont;
import com.esri.arcgis.system.NumericFormat;
import lu.etat.pch.gis.utils.SOELogger;
import lu.etat.pch.gis.utils.json.AgsJsonRGBColor;

import java.io.IOException;

/**
 * Created with IntelliJ IDEA.
 * User: schullto
 * Date: 3/31/12
 * Time: 6:51 AM
 */
public class AgsJsonGridLabelFormat {
    private String fontName;
    private Double fontSize;
    private AgsJsonRGBColor color;
    private Boolean labelOrientationTop;
    private Boolean labelOrientationLeft;
    private Boolean labelOrientationRight;
    private Boolean labelOrientationBottom;
    private AgsJsonNumericFormat format;
    private Double labelOffset;

    public AgsJsonGridLabelFormat(String fontName, Double fontSize, AgsJsonRGBColor color) {
        this.fontName = fontName;
        this.fontSize = fontSize;
        this.color = color;
    }

    public AgsJsonGridLabelFormat(SOELogger logger, JSONObject jsonObject) {
        if (!jsonObject.isNull("fontName")) fontName = jsonObject.getString("fontName");
        if (!jsonObject.isNull("fontSize")) fontSize = jsonObject.getDouble("fontSize");
        if (!jsonObject.isNull("color")) color = new AgsJsonRGBColor(logger, jsonObject.get("color"));
        if (!jsonObject.isNull("labelOrientationTop")) labelOrientationTop = jsonObject.getBoolean("labelOrientationTop");
        if (!jsonObject.isNull("labelOrientationLeft")) labelOrientationLeft = jsonObject.getBoolean("labelOrientationLeft");
        if (!jsonObject.isNull("labelOrientationRight")) labelOrientationRight = jsonObject.getBoolean("labelOrientationRight");
        if (!jsonObject.isNull("labelOrientationBottom")) labelOrientationBottom = jsonObject.getBoolean("labelOrientationBottom");
        if (!jsonObject.isNull("format")) format = new AgsJsonNumericFormat(logger, jsonObject.getJSONObject("format"));
        if (!jsonObject.isNull("labelOffset")) labelOffset = jsonObject.getDouble("labelOffset");
    }

    public IGridLabel toArcObjects(IMapGrid mapGrid) {
        try {
            IGridLabel2 gridLabel;
            if(mapGrid instanceof Graticule) {
                gridLabel =new DMSGridLabel();
            }else{
                gridLabel =new FormattedGridLabel();
            }
            StdFont font = new StdFont();
            if (fontName != null) font.setName(fontName);
            gridLabel.setFont(font);
            if (fontSize != null) gridLabel.setFontSize(fontSize);
            if (color != null) gridLabel.setColor(color.toArcObject());
            if (labelOrientationTop != null)
                gridLabel.setLabelAlignment(esriGridAxisEnum.esriGridAxisTop, labelOrientationTop);
            if (labelOrientationLeft != null)
                gridLabel.setLabelAlignment(esriGridAxisEnum.esriGridAxisLeft, labelOrientationLeft);
            if (labelOrientationRight != null)
                gridLabel.setLabelAlignment(esriGridAxisEnum.esriGridAxisRight, labelOrientationRight);
            if (labelOrientationBottom != null)
                gridLabel.setLabelAlignment(esriGridAxisEnum.esriGridAxisBottom, labelOrientationBottom);

            if(labelOffset!=null) gridLabel.setLabelOffset(labelOffset);
            if(format !=null && gridLabel instanceof FormattedGridLabel) {
                FormattedGridLabel formattedGridLabel= (FormattedGridLabel) gridLabel;
                formattedGridLabel.setFormat(format.toArcObjects((NumericFormat) formattedGridLabel.getFormat()));
            }
            return (IGridLabel) gridLabel;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
}
