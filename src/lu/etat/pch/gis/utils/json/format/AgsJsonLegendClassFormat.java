/*
 *
 *  *  PCHPrintSOE - Advanced printing SOE for ArcGIS Server
 *  *  Copyright (C) 2010-2012 Tom Schuller
 *  *
 *  *  This program is free software: you can redistribute it and/or modify
 *  *  it under the terms of the GNU Lesser General Public License as published by
 *  *  the Free Software Foundation, either version 3 of the License, or
 *  *  (at your option) any later version.
 *  *
 *  *  This program is distributed in the hope that it will be useful,
 *  *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  *  GNU Lesser General Public License for more details.
 *  *
 *  *  You should have received a copy of the GNU Lesser General Public License
 *  *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package lu.etat.pch.gis.utils.json.format;

import com.esri.arcgis.carto.ILegendClassFormat;
import com.esri.arcgis.display.ITextSymbol;
import com.esri.arcgis.server.json.JSONObject;
import lu.etat.pch.gis.utils.SOELogger;
import lu.etat.pch.gis.utils.json.graphics.AgsJsonTextSymbol;

import java.io.IOException;

/**
 * Created with IntelliJ IDEA.
 * User: schullto
 * Date: 10/1/12
 * Time: 5:12 PM
 */
public class AgsJsonLegendClassFormat {
    private AgsJsonTextSymbol labelSymbol;
    private AgsJsonTextSymbol descriptionSymbol;
    private Double patchWidth;
    private Double patchHeight;
    //private ILinePatch linePatch;
    //private IArePatch areaPatch;

    public AgsJsonLegendClassFormat(SOELogger logger, JSONObject jsonObject) {
        if (!jsonObject.isNull("labelSymbol"))
            labelSymbol = new AgsJsonTextSymbol(logger, jsonObject.getJSONObject("labelSymbol"));
        if (!jsonObject.isNull("descriptionSymbol"))
            descriptionSymbol = new AgsJsonTextSymbol(logger, jsonObject.getJSONObject("descriptionSymbol"));
        if (!jsonObject.isNull("patchWidth")) patchWidth = jsonObject.getDouble("patchWidth");
        if (!jsonObject.isNull("patchHeight")) patchHeight = jsonObject.getDouble("patchHeight");
    }

    public ILegendClassFormat toArcObjects(ILegendClassFormat legendClassFormat) {
        if (legendClassFormat == null) return null;
        try {
            if (labelSymbol != null)
                legendClassFormat.setLabelSymbol((ITextSymbol) labelSymbol.toArcObject().get(0));
            if (descriptionSymbol != null)
                legendClassFormat.setDescriptionSymbol((ITextSymbol) descriptionSymbol.toArcObject().get(0));
            if (patchWidth != null)
                legendClassFormat.setPatchHeight(patchWidth);
            if (patchHeight != null)
                legendClassFormat.setPatchHeight(patchHeight);
            return legendClassFormat;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public JSONObject toJSON() {
        JSONObject jsonObject = new JSONObject();
        if (labelSymbol != null)
            jsonObject.put("labelSymbol", labelSymbol.toJSON());
        if (descriptionSymbol != null)
            jsonObject.put("descriptionSymbol", descriptionSymbol.toJSON());
        if (patchWidth != null)
            jsonObject.put("patchWidth", patchWidth);
        if (patchHeight != null)
            jsonObject.put("patchHeight", patchHeight);
        return jsonObject;
    }
}
