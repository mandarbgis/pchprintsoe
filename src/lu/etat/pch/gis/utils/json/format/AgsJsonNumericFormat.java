/*
 *
 *  *  PCHPrintSOE - Advanced printing SOE for ArcGIS Server
 *  *  Copyright (C) 2010-2012 Tom Schuller
 *  *
 *  *  This program is free software: you can redistribute it and/or modify
 *  *  it under the terms of the GNU Lesser General Public License as published by
 *  *  the Free Software Foundation, either version 3 of the License, or
 *  *  (at your option) any later version.
 *  *
 *  *  This program is distributed in the hope that it will be useful,
 *  *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  *  GNU Lesser General Public License for more details.
 *  *
 *  *  You should have received a copy of the GNU Lesser General Public License
 *  *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package lu.etat.pch.gis.utils.json.format;

import com.esri.arcgis.server.json.JSONObject;
import com.esri.arcgis.system.NumericFormat;
import com.esri.arcgis.system.esriNumericAlignmentEnum;
import com.esri.arcgis.system.esriRoundingOptionEnum;
import lu.etat.pch.gis.utils.SOELogger;

import java.io.IOException;

/**
 * Created by IntelliJ IDEA.
 * User: schullto
 * Date: 2/27/12
 * Time: 5:56 PM
 */
public class AgsJsonNumericFormat {
    private SOELogger logger;
    private int roundingOption = 0;
    private int roundingValue = 6;
    private int alignmentOption = esriNumericAlignmentEnum.esriAlignLeft;
    private int alignmentWidth = 12;
    private boolean showPlusSign = false;
    private boolean useSeparator = false;
    private boolean zeroPad = false;

    public AgsJsonNumericFormat(SOELogger logger, JSONObject jsonObject) {
        this.logger = logger;
        if (jsonObject == null) return;
        if (!jsonObject.isNull("roundingOption")) roundingOption = jsonObject.getInt("roundingOption");
        if (!jsonObject.isNull("roundingValue")) roundingValue = jsonObject.getInt("roundingValue");
        if (!jsonObject.isNull("alignmentOption")) alignmentOption = jsonObject.getInt("alignmentOption");
        if (!jsonObject.isNull("alignmentWidth")) alignmentWidth = jsonObject.getInt("alignmentWidth");
        if (!jsonObject.isNull("showPlusSign")) showPlusSign = jsonObject.getBoolean("showPlusSign");
        if (!jsonObject.isNull("useSeparator")) useSeparator = jsonObject.getBoolean("useSeparator");
        if (!jsonObject.isNull("zeroPad")) zeroPad = jsonObject.getBoolean("zeroPad");
    }

    public NumericFormat toArcObjects(NumericFormat nFormat) throws IOException {
        if (nFormat == null) nFormat = new NumericFormat();
        nFormat.setRoundingOption(roundingOption);
        nFormat.setRoundingValue(roundingValue);
        nFormat.setAlignmentOption(alignmentOption);
        nFormat.setAlignmentWidth(alignmentWidth);
        nFormat.setShowPlusSign(showPlusSign);
        nFormat.setUseSeparator(useSeparator);
        nFormat.setZeroPad(zeroPad);
        return nFormat;
    }

    public JSONObject toJSON() {
        JSONObject retObject = new JSONObject();
        retObject.put("roundingOption", roundingOption);
        retObject.put("roundingValue", roundingValue);
        retObject.put("alignmentOption", alignmentOption);
        retObject.put("alignmentWidth", alignmentWidth);
        retObject.put("showPlusSign", showPlusSign);
        retObject.put("useSeparator", useSeparator);
        retObject.put("zeroPad", zeroPad);
        return retObject;
    }
}
