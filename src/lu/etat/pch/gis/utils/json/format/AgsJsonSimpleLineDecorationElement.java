/*
 *
 *  *  PCHPrintSOE - Advanced printing SOE for ArcGIS Server
 *  *  Copyright (C) 2010-2012 Tom Schuller
 *  *
 *  *  This program is free software: you can redistribute it and/or modify
 *  *  it under the terms of the GNU Lesser General Public License as published by
 *  *  the Free Software Foundation, either version 3 of the License, or
 *  *  (at your option) any later version.
 *  *
 *  *  This program is distributed in the hope that it will be useful,
 *  *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  *  GNU Lesser General Public License for more details.
 *  *
 *  *  You should have received a copy of the GNU Lesser General Public License
 *  *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package lu.etat.pch.gis.utils.json.format;

import com.esri.arcgis.display.SimpleLineDecorationElement;
import com.esri.arcgis.server.json.JSONArray;
import com.esri.arcgis.server.json.JSONObject;
import lu.etat.pch.gis.utils.SOELogger;
import lu.etat.pch.gis.utils.json.AgsJsonRGBColor;
import lu.etat.pch.gis.utils.json.graphics.AgsJsonMarkerSymbol;
import lu.etat.pch.gis.utils.json.graphics.AgsJsonPchCharacterMarkerSymbol;
import lu.etat.pch.gis.utils.json.graphics.MarkerSymbols;

import java.io.IOException;

/**
 * Created with IntelliJ IDEA.
 * User: schullto
 * Date: 6/15/12
 * Time: 6:37 AM
 */
public class AgsJsonSimpleLineDecorationElement {
    private SOELogger logger;
    private double[] positions = new double[0];
    private boolean flipAll;
    private boolean flipFirst;
    private boolean rotate;
    private AgsJsonMarkerSymbol markerSymbol;
    private boolean positionAsRatio=true;

    public AgsJsonSimpleLineDecorationElement(SOELogger logger, JSONObject jsonObject) {
        this.logger = logger;
        if (jsonObject == null) return;
        if (!jsonObject.isNull("positions")) {
            Object positionsObj = jsonObject.get("positions");
            if (positionsObj instanceof Number) {
                positions = new double[]{((Number) positionsObj).doubleValue()};
            } else if (positionsObj instanceof JSONArray) {
                JSONArray positionsArray = (JSONArray) positionsObj;
                positions = new double[positionsArray.length()];
                for (int i = 0; i < positionsArray.length(); i++) {
                    positions[i] = positionsArray.getDouble(i);
                }
            } else
                System.out.println("positions = " + positions);
        }
        if (!jsonObject.isNull("flipAll")) flipAll = jsonObject.getBoolean("flipAll");
        if (!jsonObject.isNull("flipFirst")) flipFirst = jsonObject.getBoolean("flipFirst");
        if (!jsonObject.isNull("rotate")) rotate = jsonObject.getBoolean("rotate");
        if (!jsonObject.isNull("positionAsRatio")) rotate = jsonObject.getBoolean("positionAsRatio");
        if (!jsonObject.isNull("markerSymbol"))
            markerSymbol = MarkerSymbols.getSymbol(logger, jsonObject.getJSONObject("markerSymbol"));
    }

    public SimpleLineDecorationElement toArcObject() throws IOException {
        SimpleLineDecorationElement simpleLineDecorationElement = new SimpleLineDecorationElement();
        for (double position : positions) {
            simpleLineDecorationElement.addPosition(position);
        }
        simpleLineDecorationElement.setFlipAll(flipAll);
        simpleLineDecorationElement.setFlipFirst(flipFirst);
        simpleLineDecorationElement.setRotate(rotate);
        simpleLineDecorationElement.setPositionAsRatio(positionAsRatio);
        if (markerSymbol != null) {
            simpleLineDecorationElement.setMarkerSymbol(markerSymbol.toMarkerArcObject());
        } else {
            AgsJsonPchCharacterMarkerSymbol arrowMarker = new AgsJsonPchCharacterMarkerSymbol(logger, new AgsJsonRGBColor(logger, 255, 0, 0), 24);
            simpleLineDecorationElement.setMarkerSymbol(arrowMarker.toMarkerArcObject());
        }
        return simpleLineDecorationElement;
    }

    public JSONObject toJSON() {
        JSONObject retObject = new JSONObject();
        if(positions.length>0) {
            JSONArray positionArray = new JSONArray();
            for (double position : positions) {
                positionArray.put(position);
            }
            retObject.put("positions", positionArray);
        }
        retObject.put("flipAll", flipAll);
        retObject.put("flipFirst", flipFirst);
        retObject.put("rotate", rotate);
        retObject.put("positionAsRatio",positionAsRatio);
        retObject.put("markerSymbol", markerSymbol.toJSON());
        return retObject;
    }
}
