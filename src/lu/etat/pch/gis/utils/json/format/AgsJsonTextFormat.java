/*
 *
 *  *  PCHPrintSOE - Advanced printing SOE for ArcGIS Server
 *  *  Copyright (C) 2010-2012 Tom Schuller
 *  *
 *  *  This program is free software: you can redistribute it and/or modify
 *  *  it under the terms of the GNU Lesser General Public License as published by
 *  *  the Free Software Foundation, either version 3 of the License, or
 *  *  (at your option) any later version.
 *  *
 *  *  This program is distributed in the hope that it will be useful,
 *  *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  *  GNU Lesser General Public License for more details.
 *  *
 *  *  You should have received a copy of the GNU Lesser General Public License
 *  *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package lu.etat.pch.gis.utils.json.format;

import com.esri.arcgis.display.RgbColor;
import com.esri.arcgis.display.TextSymbol;
import com.esri.arcgis.server.json.JSONObject;
import com.esri.arcgis.support.ms.stdole.StdFont;
import lu.etat.pch.gis.utils.SOELogger;
import lu.etat.pch.gis.utils.json.AgsJsonRGBColor;

import java.io.IOException;

/**
 * Created by IntelliJ IDEA.
 * User: schullto
 * Date: 14/03/12
 * Time: 21:57
 */
public class AgsJsonTextFormat {
    private static final String TAG = "AgsJsonTextFormat";
    public static final String FONT_STYLE_ITALIC = "italic";
    public static final String FONT_STYLE_NORMAL = "normal";
    public static final String FONT_STYLE_OBLIQUE = "oblique";
    public static final String FONT_WEIGHT_BOLD = "bold";
    public static final String FONT_WEIGHT_BOLDER = "bolder";
    public static final String FONT_WEIGHT_LIGHTER = "lighter";
    public static final String FONT_WEIGHT_NORMAL = "normal";
    public static final String FONT_DECORATION_LINE_THROUGH = "line-through";
    public static final String FONT_DECORATION_UNDERLINE = "underline";
    public static final String FONT_DECORATION_NONE = "none";
    private SOELogger logger;
    private String decoration = "none";
    private String family = "Arial";
    private Integer size = 12;
    private AgsJsonRGBColor color;
    private String style = "normal";
    private String weight = "normal";

    public AgsJsonTextFormat(SOELogger logger) {
        this.logger = logger;
    }

    public AgsJsonTextFormat(SOELogger logger, JSONObject jsonObject) {
        this(logger);
        if (!jsonObject.isNull("decoration")) this.decoration = jsonObject.getString("decoration");
        if (!jsonObject.isNull("family")) this.family = jsonObject.getString("family");
        if (!jsonObject.isNull("size")) this.size = jsonObject.getInt("size");
        if (!jsonObject.isNull("color")) this.color = new AgsJsonRGBColor(logger, jsonObject.get("color"));
        if (!jsonObject.isNull("style")) this.style = jsonObject.getString("style");
        if (!jsonObject.isNull("weight")) this.weight = jsonObject.getString("weight");
    }

    public TextSymbol toArcObjects(TextSymbol textSymbol) throws IOException {
        StdFont font = new StdFont();
        if (decoration != null) {
            if (decoration.equals(FONT_DECORATION_UNDERLINE)) font.setUnderline(true);
            else if (decoration.equals(FONT_DECORATION_LINE_THROUGH)) font.setStrikethrough(true);
        }
        if (family != null) font.setName(family);
        if (size != null) font.setSize(size);
        if (weight != null) {
            if (weight.equals(FONT_WEIGHT_BOLDER) || weight.equals(FONT_WEIGHT_BOLD)) {
                font.setBold(true);
            }
        }
        if (style != null) {
            if (style.equals(FONT_STYLE_ITALIC)) font.setItalic(true);
        }

        if (color != null) {
            RgbColor rgbColor = color.toArcObject();
            if (rgbColor == null)
                logger.error(TAG, "toArcObject=NULL: " + color.toJSON().toString());
            else textSymbol.setColor(rgbColor);
        }
        textSymbol.setFont(font);
        if (size != null) textSymbol.setSize(size);
        return textSymbol;
    }

    public JSONObject toJSON() {
        JSONObject retObject = new JSONObject();
        if (decoration != null) retObject.put("decoration", decoration);
        if (family != null) retObject.put("family", family);
        if (size != null) retObject.put("size", size);
        if (style != null) retObject.put("style", style);
        if (weight != null) retObject.put("weight", weight);
        return retObject;
    }

    public String getDecoration() {
        return decoration;
    }

    public void setDecoration(String decoration) {
        this.decoration = decoration;
    }

    public String getFamily() {
        return family;
    }

    public void setFamily(String family) {
        this.family = family;
    }

    public Integer getSize() {
        return size;
    }

    public void setSize(Integer size) {
        this.size = size;
    }

    public AgsJsonRGBColor getColor() {
        return color;
    }

    public void setColor(AgsJsonRGBColor color) {
        this.color = color;
    }

    public String getStyle() {
        return style;
    }

    public void setStyle(String style) {
        this.style = style;
    }

    public String getWeight() {
        return weight;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }
}
