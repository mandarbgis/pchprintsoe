/*
 *
 *  *  PCHPrintSOE - Advanced printing SOE for ArcGIS Server
 *  *  Copyright (C) 2010-2012 Tom Schuller
 *  *
 *  *  This program is free software: you can redistribute it and/or modify
 *  *  it under the terms of the GNU Lesser General Public License as published by
 *  *  the Free Software Foundation, either version 3 of the License, or
 *  *  (at your option) any later version.
 *  *
 *  *  This program is distributed in the hope that it will be useful,
 *  *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  *  GNU Lesser General Public License for more details.
 *  *
 *  *  You should have received a copy of the GNU Lesser General Public License
 *  *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package lu.etat.pch.gis.utils.json.geometry;

import com.esri.arcgis.geometry.*;
import com.esri.arcgis.server.json.JSONArray;
import com.esri.arcgis.server.json.JSONException;
import com.esri.arcgis.server.json.JSONObject;
import lu.etat.pch.gis.utils.SOELogger;

import java.io.IOException;

/**
 * Created by IntelliJ IDEA.
 * User: schullto
 * Date: Jul 6, 2010
 * Time: 6:59:19 PM
 */
public abstract class AgsJsonGeometry {
    private static final String TAG = "AgsJsonGeometry";
    protected SOELogger logger;

    //todo: support for SpatialReference wkid

    public abstract IGeometry toArcObject() throws IOException;

    public abstract JSONObject toJSON() throws JSONException;

    protected AgsJsonGeometry(SOELogger logger) {
        this.logger = logger;
    }

    public static AgsJsonGeometry convertJsonToAgsJsonGeom(SOELogger logger, JSONObject graphicGeomObj) throws JSONException, IOException {
        AgsJsonGeometry geom = null;
        if (!graphicGeomObj.isNull("x")) {
            geom = new AgsJsonPoint(logger, graphicGeomObj);
        } else if (!graphicGeomObj.isNull("xmin")) {
            geom = new AgsJsonEnvelope(logger, graphicGeomObj);
        } else if (!graphicGeomObj.isNull("paths")) {
            geom = new AgsJsonPolyline(logger, graphicGeomObj);
        } else if (!graphicGeomObj.isNull("rings")) {
            geom = new AgsJsonPolygon(logger, graphicGeomObj);
        } else {
            logger.error(TAG, "convertJsonToAgsJsonGeom: unknown jsonObject !!! ", graphicGeomObj.toString());
        }
        return geom;
    }

    public static JSONObject convertGeomToJson(SOELogger logger, IGeometry geom) throws IOException, JSONException {
        return convertGeomToJSON(logger, geom, true);
    }

    private static JSONObject convertGeomToJSON(SOELogger logger, IGeometry geom, boolean includeSpatialRef)
            throws IOException, JSONException {
        if (geom instanceof Point) {
            Point point = (Point) geom;

            JSONObject pointJson = new JSONObject();
            if (!Double.isNaN(point.getX())) pointJson.put("x", point.getX());
            if (!Double.isNaN(point.getY())) pointJson.put("y", point.getY());
            if (point.isZAware()) {
                if (!Double.isNaN(point.getZ())) pointJson.put("z", point.getZ());
            }
            if (includeSpatialRef && point.getSpatialReference() != null)
                pointJson.put("spatialReference", convertSpatialReferenceToJSON(point.getSpatialReference()));
            return pointJson;
        } else if (geom instanceof IEnvelope) {
            IEnvelope env = (IEnvelope) geom;
            if(env.isEmpty()) {
                logger.error(TAG,"envelope is empty!");
                return null;
            }
            JSONObject envJson = new JSONObject();
            if (!Double.isNaN(env.getXMin())) envJson.put("xmin", env.getXMin());
            if (!Double.isNaN(env.getYMin())) envJson.put("ymin", env.getYMin());
            if (!Double.isNaN(env.getXMax())) envJson.put("xmax", env.getXMax());
            if (!Double.isNaN(env.getYMax())) envJson.put("ymax", env.getYMax());
            if (includeSpatialRef && env.getSpatialReference() != null)
                envJson.put("spatialReference", convertSpatialReferenceToJSON(env.getSpatialReference()));
            return envJson;
        } else if (geom instanceof Polyline) {
            Polyline polyline = (Polyline) geom;
            JSONObject polylineJson = new JSONObject();
            JSONArray pathArrayJson = new JSONArray();
            for (int pathIndex = 0; pathIndex < polyline.getGeometryCount(); pathIndex++) {
                IGeometry geomPath = polyline.getGeometry(pathIndex);
                if (geomPath instanceof Path) {
                    Path path = (Path) geomPath;
                    JSONArray pointArray = new JSONArray();
                    for (int i = 0; i < path.getPointCount(); i++) {
                        Point point = (Point) path.getPoint(i);
                        JSONArray coordsArray = new JSONArray();
                        coordsArray.put(point.getX());
                        coordsArray.put(point.getY());
                        if (point.isZAware() && !Double.isNaN(point.getZ())) {
                            polylineJson.put("hasZ", true);
                            coordsArray.put(point.getZ());
                        }
                        /*
                        if (point.isMAware() && !Double.isNaN(point.getM())) {
                            polylineJson.put("hasM",true);
                            coordsArray.put(point.getM());
                        }
                        */

                        pointArray.put(coordsArray);
                    }
                    pathArrayJson.put(pointArray);
                }
            }
            polylineJson.put("paths", pathArrayJson);
            if (includeSpatialRef && polyline.getSpatialReference() != null)
                polylineJson.put("spatialReference", convertSpatialReferenceToJSON(polyline.getSpatialReference()));
            return polylineJson;
        } else if (geom instanceof Polygon) {
            Polygon polygon = (Polygon) geom;
            JSONObject polygonJson = new JSONObject();
            JSONArray ringArrayJson = new JSONArray();
            for (int pathIndex = 0; pathIndex < polygon.getGeometryCount(); pathIndex++) {
                IGeometry geomPath = polygon.getGeometry(pathIndex);
                if (geomPath instanceof Ring) {
                    Ring ring = (Ring) geomPath;
                    JSONArray pointArray = new JSONArray();
                    for (int i = 0; i < ring.getPointCount(); i++) {
                        IPoint point = ring.getPoint(i);
                        JSONArray coordsArray = new JSONArray();
                        coordsArray.put(point.getX());
                        coordsArray.put(point.getY());
                        pointArray.put(coordsArray);
                    }
                    ringArrayJson.put(pointArray);
                }
            }
            polygonJson.put("rings", ringArrayJson);
            if (includeSpatialRef && polygon.getSpatialReference() != null)
                polygonJson.put("spatialReference", convertSpatialReferenceToJSON(polygon.getSpatialReference()));
            return polygonJson;
        } else {
            logger.error(TAG, "convertGeomToJson.UNKNOWN format: ", geom);
            return null;
        }
    }

    public static JSONObject convertSpatialReferenceToJSON(ISpatialReference spatialReference) throws IOException, JSONException {
        JSONObject srObject = new JSONObject();
        srObject.put("wkid", spatialReference.getFactoryCode());
        return srObject;
    }

    public static String convertGeometryTypeToJsonString(int geometryType) {
        switch (geometryType) {
            case esriGeometryType.esriGeometryPoint:
                return "esriGeometryPoint";
            case esriGeometryType.esriGeometryMultipoint:
                return "esriGeometryMultipoint";
            case esriGeometryType.esriGeometryPolyline:
                return "esriGeometryPolyline";
            case esriGeometryType.esriGeometryPolygon:
                return "esriGeometryPolygon";
            case esriGeometryType.esriGeometryEnvelope:
                return "esriGeometryEnvelope";
            default:
                return "??esriGeometryType??";
        }
    }
}
