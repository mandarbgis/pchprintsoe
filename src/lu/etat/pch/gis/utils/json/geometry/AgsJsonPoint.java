/*
 *
 *  *  PCHPrintSOE - Advanced printing SOE for ArcGIS Server
 *  *  Copyright (C) 2010-2012 Tom Schuller
 *  *
 *  *  This program is free software: you can redistribute it and/or modify
 *  *  it under the terms of the GNU Lesser General Public License as published by
 *  *  the Free Software Foundation, either version 3 of the License, or
 *  *  (at your option) any later version.
 *  *
 *  *  This program is distributed in the hope that it will be useful,
 *  *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  *  GNU Lesser General Public License for more details.
 *  *
 *  *  You should have received a copy of the GNU Lesser General Public License
 *  *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package lu.etat.pch.gis.utils.json.geometry;

import com.esri.arcgis.geometry.IPoint;
import com.esri.arcgis.geometry.Point;
import com.esri.arcgis.server.json.JSONException;
import com.esri.arcgis.server.json.JSONObject;
import lu.etat.pch.gis.utils.SOELogger;

import java.io.IOException;

/**
 * Created by IntelliJ IDEA.
 * User: schullto
 * Date: Mar 4, 2010
 * Time: 5:15:29 PM
 */

public class AgsJsonPoint extends AgsJsonGeometry {
    private static final String TAG = "AgsJsonPoint";
    private double x, y;
    private Double z, m;

    public AgsJsonPoint(SOELogger logger, double x, double y) {
        super(logger);
        this.x = x;
        this.y = y;
    }

    public AgsJsonPoint(SOELogger logger, JSONObject json) {
        super(logger);
        try {
            this.x = json.getDouble("x");
            this.y = json.getDouble("y");
            if (!json.isNull("z")) this.z = json.getDouble("z");
            if (!json.isNull("m")) this.m = json.getDouble("m");
        } catch (JSONException e) {
            logger.error(TAG, "AgsJsonPoint(JSONObject)", e);
        }
    }

    public JSONObject toJSON() throws JSONException {
        JSONObject grapchics1Geom = new JSONObject();
        grapchics1Geom.put("x", x);
        grapchics1Geom.put("y", y);
        if (z != null) grapchics1Geom.put("z", z);
        if (m != null) grapchics1Geom.put("m", m);
        return grapchics1Geom;
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    public Double getZ() {
        return z;
    }

    public void setZ(Double z) {
        this.z = z;
    }

    public Double getM() {
        return m;
    }

    public void setM(Double m) {
        this.m = m;
    }

    public IPoint toArcObject() throws IOException {
        Point pt = new Point();
        pt.putCoords(x, y);
        if (z != null) pt.setZ(z);
        if (m != null) pt.setZ(m);
        return pt;
    }
}
