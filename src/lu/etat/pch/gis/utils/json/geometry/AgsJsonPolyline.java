/*
 *
 *  *  PCHPrintSOE - Advanced printing SOE for ArcGIS Server
 *  *  Copyright (C) 2010-2012 Tom Schuller
 *  *
 *  *  This program is free software: you can redistribute it and/or modify
 *  *  it under the terms of the GNU Lesser General Public License as published by
 *  *  the Free Software Foundation, either version 3 of the License, or
 *  *  (at your option) any later version.
 *  *
 *  *  This program is distributed in the hope that it will be useful,
 *  *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  *  GNU Lesser General Public License for more details.
 *  *
 *  *  You should have received a copy of the GNU Lesser General Public License
 *  *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package lu.etat.pch.gis.utils.json.geometry;

import com.esri.arcgis.geodatabase.JSONDeserializerGdb;
import com.esri.arcgis.geometry.IPolyline;
import com.esri.arcgis.geometry.Point;
import com.esri.arcgis.geometry.Polyline;
import com.esri.arcgis.geometry.esriGeometryType;
import com.esri.arcgis.interop.AutomationException;
import com.esri.arcgis.server.json.JSONArray;
import com.esri.arcgis.server.json.JSONException;
import com.esri.arcgis.server.json.JSONObject;
import com.esri.arcgis.system.JSONReader;
import lu.etat.pch.gis.utils.SOELogger;

import java.io.IOException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: schullto
 * Date: Mar 4, 2010
 * Time: 5:51:11 PM
 */
public class AgsJsonPolyline extends AgsJsonGeometry {
    private static final String TAG = "AgsJsonPolyline";
    private List<AgsJsonPath> paths=new ArrayList<AgsJsonPath>();
    private Polyline polyline;

    public AgsJsonPolyline(SOELogger logger) {
        super(logger);
    }

    public AgsJsonPolyline(SOELogger logger, JSONObject json) {
        super(logger);
        if (json != null) {
            if (!json.isNull("paths")) {
                try {
                    JSONReader reader = new JSONReader();
                    reader.readFromString(json.toString());
                    JSONDeserializerGdb ds = new JSONDeserializerGdb();
                    ds.initDeserializer(reader, null);
                    polyline = (Polyline) ds.readGeometry(esriGeometryType.esriGeometryPolyline);
                } catch (AutomationException e) {
                    logger.error(TAG, "AgsJsonPolyline(JSONObject).AutomationException", e);
                } catch (UnknownHostException e) {
                    logger.error(TAG, "AgsJsonPolyline(JSONObject).UnknownHostException", e);
                } catch (IOException e) {
                    logger.error(TAG, "AgsJsonPolyline(JSONObject),IOException", e);
                } catch (Exception e) {
                    logger.error(TAG, "AgsJsonPolyline(JSONObject).Exception", e);
                }
            } else {
                throw new JSONException("Input JSON does not contain path information");
            }
        } else {
            throw new JSONException("JSON is null");
        }
    }

    public AgsJsonPoint addPoint(int ringIndex, double x, double y) throws Exception {
        AgsJsonPoint agsJsonPoint = new AgsJsonPoint(logger, x, y);
        addPoint(ringIndex, agsJsonPoint);
        return agsJsonPoint;
    }

    public void addPoint(int ringIndex, AgsJsonPoint agsJsonPoint) throws Exception {
        if (ringIndex < paths.size()) {
            paths.get(ringIndex).addPoint(agsJsonPoint);
        } else if (ringIndex == paths.size()) {
            //add a new ring
            AgsJsonPath agsJsonPath = new AgsJsonPath();
            agsJsonPath.addPoint(agsJsonPoint);
            paths.add(agsJsonPath);
        } else {
            throw new Exception("invalid pathIndex");
        }
    }

    private class AgsJsonPath {
        List<AgsJsonPoint> agsJsonPointList;

        private AgsJsonPath() {
            agsJsonPointList = new ArrayList<AgsJsonPoint>();
        }

        public void addPoint(AgsJsonPoint agsJsonPoint) {
            agsJsonPointList.add(agsJsonPoint);
        }

        public List<AgsJsonPoint> getPoints() {
            return agsJsonPointList;
        }

        public int getPointCount() {
            return agsJsonPointList.size();
        }
    }

    private void geom2json(Polyline polyline) throws Exception {
        paths.clear();
        for (int i = 0; i < polyline.getPointCount(); i++) {
            Point pt = (Point) polyline.getPoint(i);
            AgsJsonPoint jsonPt = addPoint(0, pt.getX(), pt.getY());
//                        if(pt.isZAware()) jsonPt.setZ(pt.getZ());
//                        if(pt.isMAware()) jsonPt.setM(pt.getM());
        }
    }


    public JSONObject toJSON() throws JSONException {
        try {
            geom2json(polyline);
            JSONObject geomObject = new JSONObject();
            JSONArray ringArray = new JSONArray();
            for (AgsJsonPath agsJsonPath : paths) {
                if (agsJsonPath.getPointCount() > 1) {
                    JSONArray pointsArray = new JSONArray();
                    for (AgsJsonPoint agsJsonPoint : agsJsonPath.getPoints()) {
                        JSONArray coordArray = new JSONArray();
                        coordArray.put(agsJsonPoint.getX());
                        coordArray.put(agsJsonPoint.getY());
                        pointsArray.put(coordArray);
                    }
                    ringArray.put(pointsArray);
                }
            }
            geomObject.put("paths", ringArray);
            return geomObject;
        } catch (Exception e) {
            logger.error(TAG,"toJson().Exception",e);
        }
        return null;
    }

    public IPolyline toArcObject() throws JSONException, IOException {
        if(polyline!=null) return polyline;
        Polyline polyline = new Polyline();
        for (AgsJsonPath agsJsonPath : paths) {
            List<AgsJsonPoint> pathPoints = agsJsonPath.getPoints();
            for (AgsJsonPoint agsJsonPoint : pathPoints) {
                polyline.addPoint(agsJsonPoint.toArcObject(), null, null);
            }
        }
        return polyline;
    }
}
