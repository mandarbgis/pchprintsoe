/*
 *
 *  *  PCHPrintSOE - Advanced printing SOE for ArcGIS Server
 *  *  Copyright (C) 2010-2012 Tom Schuller
 *  *
 *  *  This program is free software: you can redistribute it and/or modify
 *  *  it under the terms of the GNU Lesser General Public License as published by
 *  *  the Free Software Foundation, either version 3 of the License, or
 *  *  (at your option) any later version.
 *  *
 *  *  This program is distributed in the hope that it will be useful,
 *  *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  *  GNU Lesser General Public License for more details.
 *  *
 *  *  You should have received a copy of the GNU Lesser General Public License
 *  *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package lu.etat.pch.gis.utils.json.graphics;

import com.esri.arcgis.display.ISymbol;
import com.esri.arcgis.display.esriSimpleFillStyle;
import com.esri.arcgis.display.esriSimpleLineStyle;
import com.esri.arcgis.display.esriSimpleMarkerStyle;
import com.esri.arcgis.server.json.JSONArray;
import com.esri.arcgis.server.json.JSONException;
import com.esri.arcgis.server.json.JSONObject;
import lu.etat.pch.gis.utils.SOELogger;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by IntelliJ IDEA.
 * User: schullto
 * Date: Mar 4, 2010
 * Time: 6:02:22 PM
 */
public abstract class AbstractAgsJsonSymbol {
    private static final String TAG = "AbstractAgsJsonSymbol";
    SOELogger logger;
    public static Map<String, Integer> esriStyleMap;
    protected String type;

    protected AbstractAgsJsonSymbol(SOELogger logger) {
        this.logger = logger;
    }

    public abstract JSONObject toJSON() throws JSONException;

    public abstract List<ISymbol> toArcObject() throws IOException;

    public static int getEsriStyleCode(String style) throws JSONException {
        if (esriStyleMap.containsKey(style)) {
            return esriStyleMap.get(style);
        }
        throw new JSONException("AbstractAgsJsonSymbol.getEsriStyleCode !! unknown style: " + style);
    }

    public static AbstractAgsJsonSymbol convertJsonToAgsJsonSymbol(SOELogger logger, JSONObject jsonObject) {
        if (!jsonObject.isNull("source")) {
            List<AbstractAgsJsonSymbol> retList = new ArrayList<AbstractAgsJsonSymbol>();
            Object sourceObj = jsonObject.get("source");
            if (sourceObj instanceof JSONArray) {
                JSONArray jsonArray = (JSONArray) sourceObj;
                for (int i = 0; i < jsonArray.length(); i++) {
                    retList.add(convertJsonToAgsJsonSymbol(logger, (JSONObject) jsonArray.get(i)));
                }
            }
            return new AgsJsonCompositeSymbol(logger, retList);
        }
        String type = jsonObject.getString("type");
        if (MarkerSymbols.isMarkerSymbol(type)) {
            return (AbstractAgsJsonSymbol) MarkerSymbols.getSymbol(logger, jsonObject);
        } else if (LineSymbols.isLineSymbol(type)) {
            return (AbstractAgsJsonSymbol) LineSymbols.getSymbol(logger, jsonObject);
        } else if (type.equalsIgnoreCase("esriPMS")) {
            return new AgsJsonPictureMarkerSymbol(logger, jsonObject);
        } else if (type.equalsIgnoreCase("esriSFS")) {
            return new AgsJsonSimpleFillSymbol(logger, jsonObject);
        } else if (type.equalsIgnoreCase("esriTS")) {
            return new AgsJsonTextSymbol(logger, jsonObject);
        } else if (type.equalsIgnoreCase("pchMapFrame") || type.equalsIgnoreCase("pchTextElement") || type.equalsIgnoreCase("pchNorthArrow") || type.equalsIgnoreCase("pchScaleBar") || type.equalsIgnoreCase("pchScaleText") || type.equalsIgnoreCase("pchLegend")) {
            return null;
        } else {
            logger.warning(TAG, "convertJsonToAgsJsonSymbol.UNKNOWN-TYPE: " + jsonObject.toString());
            return null;
        }
    }

    static {
        AbstractAgsJsonSymbol.esriStyleMap = new HashMap<String, Integer>();
        AbstractAgsJsonSymbol.esriStyleMap.put("esriSMSCircle", esriSimpleMarkerStyle.esriSMSCircle);
        AbstractAgsJsonSymbol.esriStyleMap.put("esriSMSCross", esriSimpleMarkerStyle.esriSMSCross);
        AbstractAgsJsonSymbol.esriStyleMap.put("esriSMSDiamond", esriSimpleMarkerStyle.esriSMSDiamond);
        AbstractAgsJsonSymbol.esriStyleMap.put("esriSMSSquare", esriSimpleMarkerStyle.esriSMSSquare);
        AbstractAgsJsonSymbol.esriStyleMap.put("esriSMSX", esriSimpleMarkerStyle.esriSMSX);

        AbstractAgsJsonSymbol.esriStyleMap.put("esriSLSSolid", esriSimpleLineStyle.esriSLSSolid);
        AbstractAgsJsonSymbol.esriStyleMap.put("esriSLSDash", esriSimpleLineStyle.esriSLSDash);
        AbstractAgsJsonSymbol.esriStyleMap.put("esriSLSDot", esriSimpleLineStyle.esriSLSDot);
        AbstractAgsJsonSymbol.esriStyleMap.put("esriSLSDashDot", esriSimpleLineStyle.esriSLSDashDot);
        AbstractAgsJsonSymbol.esriStyleMap.put("esriSLSDashDotDot", esriSimpleLineStyle.esriSLSDashDotDot);
        AbstractAgsJsonSymbol.esriStyleMap.put("esriSLSNull", esriSimpleLineStyle.esriSLSNull);
        AbstractAgsJsonSymbol.esriStyleMap.put("esriSLSInsideFrame", esriSimpleLineStyle.esriSLSInsideFrame);

        AbstractAgsJsonSymbol.esriStyleMap.put("esriSFSSolid", esriSimpleFillStyle.esriSFSSolid);
        AbstractAgsJsonSymbol.esriStyleMap.put("esriSFSNull", esriSimpleFillStyle.esriSFSNull);
        AbstractAgsJsonSymbol.esriStyleMap.put("esriSFSHollow", esriSimpleFillStyle.esriSFSHollow);
        AbstractAgsJsonSymbol.esriStyleMap.put("esriSFSHorizontal", esriSimpleFillStyle.esriSFSHorizontal);
        AbstractAgsJsonSymbol.esriStyleMap.put("esriSFSVertical", esriSimpleFillStyle.esriSFSVertical);
        AbstractAgsJsonSymbol.esriStyleMap.put("esriSFSForwardDiagonal", esriSimpleFillStyle.esriSFSForwardDiagonal);
        AbstractAgsJsonSymbol.esriStyleMap.put("esriSFSBackwardDiagonal", esriSimpleFillStyle.esriSFSBackwardDiagonal);
        AbstractAgsJsonSymbol.esriStyleMap.put("esriSFSCross", esriSimpleFillStyle.esriSFSCross);
        AbstractAgsJsonSymbol.esriStyleMap.put("esriSFSDiagonalCross", esriSimpleFillStyle.esriSFSDiagonalCross);
    }
}
