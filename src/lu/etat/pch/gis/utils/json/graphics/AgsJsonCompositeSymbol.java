/*
 *
 *  *  PCHPrintSOE - Advanced printing SOE for ArcGIS Server
 *  *  Copyright (C) 2010-2012 Tom Schuller
 *  *
 *  *  This program is free software: you can redistribute it and/or modify
 *  *  it under the terms of the GNU Lesser General Public License as published by
 *  *  the Free Software Foundation, either version 3 of the License, or
 *  *  (at your option) any later version.
 *  *
 *  *  This program is distributed in the hope that it will be useful,
 *  *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  *  GNU Lesser General Public License for more details.
 *  *
 *  *  You should have received a copy of the GNU Lesser General Public License
 *  *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package lu.etat.pch.gis.utils.json.graphics;

import com.esri.arcgis.display.ISymbol;
import com.esri.arcgis.server.json.JSONException;
import com.esri.arcgis.server.json.JSONObject;
import lu.etat.pch.gis.utils.SOELogger;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * User: schullto
 * Date: 1/22/13
 * Time: 7:29 PM
 */
public class AgsJsonCompositeSymbol extends AbstractAgsJsonSymbol {
    List<AbstractAgsJsonSymbol> symbolList;

    public AgsJsonCompositeSymbol(SOELogger logger, List<AbstractAgsJsonSymbol> symbolList) {
        super(logger);
        this.symbolList = symbolList;
    }

    @Override
    public JSONObject toJSON() throws JSONException {
        //todo: need to implement
        return null;
    }

    @Override
    public List<ISymbol> toArcObject() throws IOException {
        List<ISymbol> retSymbols = new ArrayList<ISymbol>();
        for (AbstractAgsJsonSymbol abstractAgsJsonSymbol : symbolList) {
            retSymbols.addAll(abstractAgsJsonSymbol.toArcObject());
        }
        return retSymbols;
    }
}
