/*
 *
 *  *  PCHPrintSOE - Advanced printing SOE for ArcGIS Server
 *  *  Copyright (C) 2010-2012 Tom Schuller
 *  *
 *  *  This program is free software: you can redistribute it and/or modify
 *  *  it under the terms of the GNU Lesser General Public License as published by
 *  *  the Free Software Foundation, either version 3 of the License, or
 *  *  (at your option) any later version.
 *  *
 *  *  This program is distributed in the hope that it will be useful,
 *  *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  *  GNU Lesser General Public License for more details.
 *  *
 *  *  You should have received a copy of the GNU Lesser General Public License
 *  *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package lu.etat.pch.gis.utils.json.graphics;

import com.esri.arcgis.carto.IElement;
import com.esri.arcgis.carto.IPictureElement;
import com.esri.arcgis.carto.PngPictureElement;
import com.esri.arcgis.display.ISymbol;
import com.esri.arcgis.geometry.IGeometry;
import com.esri.arcgis.geometry.IPoint;
import com.esri.arcgis.server.json.JSONException;
import com.esri.arcgis.server.json.JSONObject;
import lu.etat.pch.gis.utils.GeometryUtils;
import lu.etat.pch.gis.utils.RESTUtils;
import lu.etat.pch.gis.utils.SOELogger;

import java.io.File;
import java.io.IOException;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: schullto
 * Date: Mar 6, 2010
 * Time: 8:20:16 AM
 */
public class AgsJsonPictureMarkerSymbol extends AbstractAgsJsonSymbol {
    private static final String TAG = "AgsJsonPictureMarkerSymbol";
    public static final String TYPE = "esriPMS";
    private String url;
    private double height, width, angle, xOffset, yOffset;

    public AgsJsonPictureMarkerSymbol(SOELogger logger) {
        super(logger);
        this.type = TYPE;
    }

    public AgsJsonPictureMarkerSymbol(SOELogger logger, String url, double height, double width) {
        this(logger);
        this.url = url;
        this.height = height;
        this.width = width;
    }

    public AgsJsonPictureMarkerSymbol(SOELogger logger, JSONObject jsonObject) {
        this(logger);
        try {
            url = jsonObject.getString("url");
            if (!jsonObject.isNull("height")) height = jsonObject.getDouble("height");
            if (!jsonObject.isNull("width")) width = jsonObject.getDouble("width");
            if (!jsonObject.isNull("angle")) angle = jsonObject.getDouble("angle");
            if (!jsonObject.isNull("xOffset")) xOffset = jsonObject.getDouble("xOffset");
            if (!jsonObject.isNull("yOffset")) yOffset = jsonObject.getDouble("yOffset");
        } catch (JSONException e) {
            logger.error(TAG, "AgsJsonPictureMarkerSymbol(JSONObject)", e);
        }
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public double getHeight() {
        return height;
    }

    public void setHeight(double height) {
        this.height = height;
    }

    public double getWidth() {
        return width;
    }

    public void setWidth(double width) {
        this.width = width;
    }

    public double getAngle() {
        return angle;
    }

    public void setAngle(double angle) {
        this.angle = angle;
    }

    public double getxOffset() {
        return xOffset;
    }

    public void setxOffset(double xOffset) {
        this.xOffset = xOffset;
    }

    public double getyOffset() {
        return yOffset;
    }

    public void setyOffset(double yOffset) {
        this.yOffset = yOffset;
    }

    public JSONObject toJSON() throws JSONException {
        JSONObject symbObject = new JSONObject();
        symbObject.put("type", type);
        symbObject.put("url", url);
        symbObject.put("height", height);
        symbObject.put("width", width);
        symbObject.put("angle", angle);
        symbObject.put("xOffset", xOffset);
        symbObject.put("yOffset", yOffset);
        return symbObject;
    }

    public List<ISymbol> toArcObject() throws IOException {
        //solved in AgsJsonElement
        return null;
    }

    public IElement toArcObjectsElement(IGeometry geom, double resolution) throws IOException {
        if (type.equalsIgnoreCase("esriPMS")) {
            IElement markerElement = null;
            if (url.toLowerCase().endsWith(".png")) {
                markerElement = new PngPictureElement();
                File tmpFile = File.createTempFile("esriPMS", ".png");
                RESTUtils.downloadFile(logger, url, tmpFile, null, null);
                ((IPictureElement) markerElement).importPictureFromFile(tmpFile.getAbsolutePath());
            } else {
                logger.error(TAG, "toArcObjectsElement unsupported fileType: " + url);
            }
            if (markerElement != null) {
                if (geom instanceof IPoint) {
                    double widthInches = width * resolution;
                    //double widthMapUnits = UnitConverter.convertValue(esriUnits.esriInches,mapUnits,widthInches);
                    markerElement.setGeometry(GeometryUtils.convertPoint2Envelope((IPoint) geom, widthInches));
                }
            }
            logger.debug(TAG, "toArcObjectsElement.markerElement", markerElement);
            return markerElement;

        }
        logger.error(TAG, "toArcObjectsElement unsupported type", type);
        return null;
    }

    /*
  {
"type" : "esriPFS",
"url" : "<pictureUrl>",
"color" : <color>,
"outline" : <simpleLineSymbol>, //if outline has been specified
"width" : <width>,
"height" : <height>,
   "angle" : <angle>,
   "xoffset" : <xoffset>,
   "yoffset" : <yoffset>,
"xscale": <xscale>,
"yscale": <yscale>
}
    */
}
