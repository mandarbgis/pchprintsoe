/*
 *
 *  *  PCHPrintSOE - Advanced printing SOE for ArcGIS Server
 *  *  Copyright (C) 2010-2012 Tom Schuller
 *  *
 *  *  This program is free software: you can redistribute it and/or modify
 *  *  it under the terms of the GNU Lesser General Public License as published by
 *  *  the Free Software Foundation, either version 3 of the License, or
 *  *  (at your option) any later version.
 *  *
 *  *  This program is distributed in the hope that it will be useful,
 *  *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  *  GNU Lesser General Public License for more details.
 *  *
 *  *  You should have received a copy of the GNU Lesser General Public License
 *  *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package lu.etat.pch.gis.utils.json.graphics;

import com.esri.arcgis.display.*;
import com.esri.arcgis.server.json.JSONException;
import com.esri.arcgis.server.json.JSONObject;
import lu.etat.pch.gis.utils.SOELogger;
import lu.etat.pch.gis.utils.json.AgsJsonRGBColor;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: schullto
 * Date: Mar 6, 2010
 * Time: 7:55:40 AM
 */
public class AgsJsonSimpleFillSymbol extends AbstractAgsJsonSymbol {
    private static final String TAG = "AgsJsonSimpleFillSymbol";
    public static final String esriSFSBackwardDiagonal = "esriSFSBackwardDiagonal";
    public static final String esriSFSCross = "esriSFSCross";
    public static final String esriSFSDiagonalCross = "esriSFSDiagonalCross";
    public static final String esriSFSForwardDiagonal = "esriSFSForwardDiagonal";
    public static final String esriSFSHorizontal = "esriSFSHorizontal";
    public static final String esriSFSNull = "esriSFSNull";
    public static final String esriSFSSolid = "esriSFSSolid";
    public static final String esriSFSVertical = "esriSFSVertical";

    private String style;
    private AgsJsonRGBColor color;
    private AgsJsonSimpleLineSymbol outline;

    public AgsJsonSimpleFillSymbol(SOELogger logger) {
        super(logger);
        this.type = "esriSFS";
    }

    public AgsJsonSimpleFillSymbol(SOELogger logger, String style, AgsJsonRGBColor color) {
        this(logger);
        this.color = color;
        this.style = style;
    }

    public AgsJsonSimpleFillSymbol(SOELogger logger, String style, AgsJsonRGBColor color, AgsJsonSimpleLineSymbol outline) {
        this(logger);
        this.color = color;
        this.style = style;
        this.outline = outline;
    }

    public AgsJsonSimpleFillSymbol(SOELogger logger, JSONObject jsonObject) {
        this(logger);
        try {
            if (!jsonObject.isNull("color")) {
                Object colorObj = jsonObject.get("color");
                color = new AgsJsonRGBColor(logger, colorObj);
            } else {
                color = new AgsJsonRGBColor(logger, 255, 0, 0, (byte) 0);
            }
            if (!jsonObject.isNull("outline"))
                outline = new AgsJsonSimpleLineSymbol(logger, jsonObject.getJSONObject("outline"));
            if (!jsonObject.isNull("style")) style = jsonObject.getString("style");
        } catch (JSONException e) {
            logger.error(TAG, "AgsJsonSimpleFillSymbol(JSONObject)", e);
        }
    }

    public void setOutline(AgsJsonSimpleLineSymbol outline) {
        this.outline = outline;
    }

    public JSONObject toJSON() throws JSONException {
        JSONObject symbObject = new JSONObject();
        symbObject.put("type", type);
        symbObject.put("style", style);
        symbObject.put("color", color.toJSON());
        if (outline != null) {
            symbObject.put("outline", outline.toJSON());
        }
        return symbObject;
    }

    public List<ISymbol> toArcObject() throws IOException {
        if (type.equalsIgnoreCase("esriSFS")) {
            if (style == null || style.equalsIgnoreCase(esriSFSSolid) || style.equalsIgnoreCase(esriSFSNull)) {
                //only SOLID style is suported (see Javadoc: SimpleFillSymbol.setStyle)
                SimpleFillSymbol simpleFillSymbol = new SimpleFillSymbol();
                if (style != null) {
                    if (style.equalsIgnoreCase(esriSFSSolid))
                        simpleFillSymbol.setStyle(esriSimpleFillStyle.esriSFSSolid);
                    else if (style.equalsIgnoreCase(esriSFSNull))
                        simpleFillSymbol.setStyle(esriSimpleFillStyle.esriSFSNull);
                }
                if (color != null)
                    simpleFillSymbol.setColor(color.toArcObject());
                if (outline != null)
                    simpleFillSymbol.setOutline((ILineSymbol) outline.toArcObject().get(0));
                List<ISymbol> retList = new ArrayList<ISymbol>();
                retList.add(simpleFillSymbol);
                return retList;
            } else {
                Double firstAngle = null;
                Double secondAngle = null;
                if (style.equalsIgnoreCase(esriSFSCross)) {
                    firstAngle = 0d;
                    secondAngle = 90d;
                } else if (style.equalsIgnoreCase(esriSFSDiagonalCross)) {
                    firstAngle = 45d;
                    secondAngle = 135d;
                } else if (style.equalsIgnoreCase(esriSFSForwardDiagonal))
                    firstAngle = 45d;
                else if (style.equalsIgnoreCase(esriSFSBackwardDiagonal))
                    firstAngle = 135d;
                else if (style.equalsIgnoreCase(esriSFSHorizontal))
                    firstAngle = 90d;
                else if (style.equalsIgnoreCase(esriSFSVertical))
                    firstAngle = 0d;

                MultiLayerFillSymbol multiLayerFillSymbol = new MultiLayerFillSymbol();
                if (secondAngle != null) {
                    LineFillSymbol lineFillSymbol1 = new LineFillSymbol();
                    lineFillSymbol1.setColor(color.toArcObject());
                    lineFillSymbol1.setAngle(secondAngle);
                    lineFillSymbol1.setSeparation(5);
                    ILineSymbol lineSymbol = (ILineSymbol) outline.toArcObject().get(0);
                    lineSymbol.setWidth(0);
                    lineFillSymbol1.setOutline(lineSymbol);
                    multiLayerFillSymbol.addLayer(lineFillSymbol1);
                }
                if (firstAngle != null) {
                    LineFillSymbol lineFillSymbol2 = new LineFillSymbol();
                    lineFillSymbol2.setColor(color.toArcObject());
                    lineFillSymbol2.setAngle(firstAngle);
                    lineFillSymbol2.setSeparation(5);
                    lineFillSymbol2.setOutline((ILineSymbol) outline.toArcObject().get(0));
                    multiLayerFillSymbol.addLayer(lineFillSymbol2);
                }
                List<ISymbol> retList = new ArrayList<ISymbol>();
                retList.add(multiLayerFillSymbol);
                return retList;
            }
        }
        List<ISymbol> retList = new ArrayList<ISymbol>();
        retList.add(new SimpleFillSymbol());
        return retList;
    }
}
