/*
 *
 *  *  PCHPrintSOE - Advanced printing SOE for ArcGIS Server
 *  *  Copyright (C) 2010-2012 Tom Schuller
 *  *
 *  *  This program is free software: you can redistribute it and/or modify
 *  *  it under the terms of the GNU Lesser General Public License as published by
 *  *  the Free Software Foundation, either version 3 of the License, or
 *  *  (at your option) any later version.
 *  *
 *  *  This program is distributed in the hope that it will be useful,
 *  *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  *  GNU Lesser General Public License for more details.
 *  *
 *  *  You should have received a copy of the GNU Lesser General Public License
 *  *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package lu.etat.pch.gis.utils.json.graphics;

import com.esri.arcgis.display.ILineSymbol;
import com.esri.arcgis.display.ISymbol;
import com.esri.arcgis.display.SimpleLineSymbol;
import com.esri.arcgis.server.json.JSONException;
import com.esri.arcgis.server.json.JSONObject;
import lu.etat.pch.gis.utils.SOELogger;
import lu.etat.pch.gis.utils.json.AgsJsonRGBColor;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: schullto
 * Date: Mar 6, 2010
 * Time: 6:56:20 AM
 */
public class AgsJsonSimpleLineSymbol extends AbstractAgsJsonSymbol implements AgsJsonLineSymbol {
    private static final String TAG = "AgsJsonSimpleLineSymbol";
    public static final String TYPE = "esriSLS";
    public static final String esriSLSDash = "esriSLSDash";
    public static final String esriSLSDashDotDot = "esriSLSDashDotDot";
    public static final String esriSLSDot = "esriSLSDot";
    public static final String esriSLSNull = "esriSLSNull";
    public static final String esriSLSSolid = "esriSLSSolid";

    private String style = esriSLSSolid;
    private AgsJsonRGBColor color = new AgsJsonRGBColor(logger, 255, 0, 0);
    private double width = 1;

    public AgsJsonSimpleLineSymbol(SOELogger logger) {
        super(logger);
        this.type = TYPE;
    }

    public AgsJsonSimpleLineSymbol(SOELogger logger, String style, AgsJsonRGBColor color, double width) {
        this(logger);
        this.style = style;
        this.color = color;
        this.width = width;
    }

    public AgsJsonSimpleLineSymbol(SOELogger logger, JSONObject jsonObject) {
        this(logger);
        try {
            if (!jsonObject.isNull("style")) style = jsonObject.getString("style");
            if (!jsonObject.isNull("color")) {
                color = new AgsJsonRGBColor(logger, jsonObject.get("color"));
            }
            if (!jsonObject.isNull("width")) width = jsonObject.getDouble("width");
        } catch (JSONException e) {
            logger.error(TAG, "AgsJsonSimpleLineSymbol(JSONObject)", e);
        }
    }

    public String getStyle() {
        return style;
    }

    public void setStyle(String style) {
        this.style = style;
    }

    public AgsJsonRGBColor getColor() {
        return color;
    }

    public void setColor(AgsJsonRGBColor color) {
        this.color = color;
    }

    public double getWidth() {
        return width;
    }

    public void setWidth(double width) {
        this.width = width;
    }

    public JSONObject toJSON() throws JSONException {
        JSONObject symbObject = new JSONObject();
        symbObject.put("type", type);
        symbObject.put("style", style);
        if (color != null) symbObject.put("color", color.toJSON());
        symbObject.put("width", width);
        return symbObject;
    }

    @Override
    public List<ISymbol> toArcObject() throws IOException, JSONException {
        List<ISymbol> retList = new ArrayList<ISymbol>();
        SimpleLineSymbol simpleLineSymbol = new SimpleLineSymbol();
        if (style != null)
            simpleLineSymbol.setStyle(getEsriStyleCode(style));
        if (color != null)
            simpleLineSymbol.setColor(color.toArcObject());
        if (width > 0)
            simpleLineSymbol.setWidth(width);
        retList.add(simpleLineSymbol);
        return retList;
    }

    @Override
    public List<ILineSymbol> toLineArcObject() throws IOException {
        List<ILineSymbol> retList = new ArrayList<ILineSymbol>();
        SimpleLineSymbol simpleLineSymbol = new SimpleLineSymbol();
        if (style != null)
            simpleLineSymbol.setStyle(getEsriStyleCode(style));
        if (color != null)
            simpleLineSymbol.setColor(color.toArcObject());
        if (width > 0)
            simpleLineSymbol.setWidth(width);
        retList.add(simpleLineSymbol);
        return retList;
    }
}
