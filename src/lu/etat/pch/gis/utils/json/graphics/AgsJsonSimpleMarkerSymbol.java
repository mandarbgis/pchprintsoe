/*
 *
 *  *  PCHPrintSOE - Advanced printing SOE for ArcGIS Server
 *  *  Copyright (C) 2010-2012 Tom Schuller
 *  *
 *  *  This program is free software: you can redistribute it and/or modify
 *  *  it under the terms of the GNU Lesser General Public License as published by
 *  *  the Free Software Foundation, either version 3 of the License, or
 *  *  (at your option) any later version.
 *  *
 *  *  This program is distributed in the hope that it will be useful,
 *  *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  *  GNU Lesser General Public License for more details.
 *  *
 *  *  You should have received a copy of the GNU Lesser General Public License
 *  *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package lu.etat.pch.gis.utils.json.graphics;

import com.esri.arcgis.display.*;
import com.esri.arcgis.server.json.JSONException;
import com.esri.arcgis.server.json.JSONObject;
import com.esri.arcgis.support.ms.stdole.StdFont;
import lu.etat.pch.gis.utils.SOELogger;
import lu.etat.pch.gis.utils.json.AgsJsonRGBColor;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: schullto
 * Date: Mar 4, 2010
 * Time: 6:02:03 PM
 */
public class AgsJsonSimpleMarkerSymbol extends AbstractAgsJsonSymbol implements AgsJsonMarkerSymbol {
    private static final String TAG = "AgsJsonSimpleMarkerSymbol";
    public static final String TYPE = "esriSMS";
    public static final String esriSMSCircle = "esriSMSCircle";
    public static final String esriSMSCross = "esriSMSCross";
    public static final String esriSMSDiamond = "esriSMSDiamond";
    public static final String esriSMSSquare = "esriSMSSquare";
    public static final String esriSMSX = "esriSMSX";
    public static final String esriSMSTriangle = "esriSMSTriangle";


    private String style;
    private AgsJsonRGBColor color;
    private double size = 12;
    private double angle = 0;
    private double xOffset = 0;
    private double yOffset = 0;
    private AgsJsonSimpleLineSymbol outline;

    public AgsJsonSimpleMarkerSymbol(SOELogger logger) {
        super(logger);
        this.type = TYPE;
        this.style = esriSMSX;
        this.color = new AgsJsonRGBColor(logger, 255, 0, 0);
    }

    public AgsJsonSimpleMarkerSymbol(SOELogger logger, String style, AgsJsonRGBColor color, double size) {
        this(logger);
        this.style = style;
        this.color = color;
        this.size = size;
    }

    public AgsJsonSimpleMarkerSymbol(SOELogger logger, JSONObject jsonObject) {
        this(logger);
        try {
            if (!jsonObject.isNull("style")) style = jsonObject.getString("style");
            if (!jsonObject.isNull("color")) {
                color = new AgsJsonRGBColor(logger, jsonObject.get("color"));
            }
            if (!jsonObject.isNull("size")) size = jsonObject.getDouble("size");
            if (!jsonObject.isNull("angle")) angle = jsonObject.getDouble("angle");
            if (!jsonObject.isNull("xOffset")) xOffset = jsonObject.getDouble("xOffset");
            if (!jsonObject.isNull("yOffset")) yOffset = jsonObject.getDouble("yOffset");
            if (!jsonObject.isNull("outline"))
                outline = new AgsJsonSimpleLineSymbol(logger, jsonObject.getJSONObject("outline"));
        } catch (JSONException e) {
            logger.error(TAG, "AgsJsonSimpleMarkerSymbol(JSONObject)", e);
        }
    }

    public void setSize(double size) {
        this.size = size;
    }

    public void setAngle(double angle) {
        this.angle = angle;
    }

    public void setXOffset(double xOffset) {
        this.xOffset = xOffset;
    }

    public void setYOffset(double yOffset) {
        this.yOffset = yOffset;
    }

    public AgsJsonRGBColor getColor() {
        return color;
    }

    public void setColor(AgsJsonRGBColor color) {
        this.color = color;
    }

    public AgsJsonSimpleLineSymbol getOutline() {
        return outline;
    }

    public void setOutline(AgsJsonSimpleLineSymbol outline) {
        this.outline = outline;
    }

    public JSONObject toJSON() throws JSONException {
        JSONObject symbObject = new JSONObject();
        symbObject.put("type", type);
        symbObject.put("style", style);
        symbObject.put("color", color.toJSON());
        symbObject.put("size", size);
        symbObject.put("angle", angle);
        symbObject.put("xOffset", xOffset);
        symbObject.put("yOffset", yOffset);
        if (outline != null) {
            symbObject.put("outline", outline);
        }
        return symbObject;
    }

    public List<ISymbol> toArcObject() throws IOException, JSONException {
        if (type == null) {
            logger.error(TAG, "toArcObject().type=NULL", this.toJSON().toString());
            return null;
        }
        List<ISymbol> retList = new ArrayList<ISymbol>();
        IMarkerSymbol markerSymbol;
        if (style != null && style.equalsIgnoreCase(esriSMSTriangle)) {
            markerSymbol = new CharacterMarkerSymbol();
            CharacterMarkerSymbol characterMarkerSymbol = (CharacterMarkerSymbol) markerSymbol;
            StdFont stdFont = new StdFont();
            stdFont.setName("ESRI Default Marker");
            characterMarkerSymbol.setFont(stdFont);
            characterMarkerSymbol.setCharacterIndex(35);
        } else {
            markerSymbol = new SimpleMarkerSymbol();
            if (style != null)
                ((SimpleMarkerSymbol) markerSymbol).setStyle(getEsriStyleCode(style));
        }
        if (color != null)
            markerSymbol.setColor(color.toArcObject());
        if (size > -1)
            markerSymbol.setSize(size);
        markerSymbol.setAngle(angle);
        if (outline != null) {
            if (markerSymbol instanceof ISimpleMarkerSymbol) {
                ISimpleMarkerSymbol simpleMarkerSymbol = (ISimpleMarkerSymbol) markerSymbol;
                simpleMarkerSymbol.setOutline(true);
                simpleMarkerSymbol.setOutlineColor(outline.getColor().toArcObject());
                simpleMarkerSymbol.setOutlineSize(outline.getWidth());
            }
        }
        /*
        if (alpha>=0) {
            IColor color = simpleMarkerSymbol.getColor();
            color.setTransparency((byte) (symbolObject.getDouble("alpha") * 255D));
            simpleMarkerSymbol.setColor(color);
        }
        */
        markerSymbol.setXOffset(xOffset);
        markerSymbol.setYOffset(yOffset);
        retList.add((ISymbol) markerSymbol);
        return retList;
    }
    /*
{
"type" : "esriSMS",
"style" : "< esriSMSCircle | esriSMSCross | esriSMSDiamond | esriSMSSquare | esriSMSX >",
"color" : <color>,
"size" : <size>,
"angle" : <angle>,
"xoffset" : <xoffset>,
"yoffset" : <yoffset>,
"outline" : { //if outline has been specified
 "color" : <color>,
 "width" : <width>
}
}
    */

    @Override
    public IMarkerSymbol toMarkerArcObject() throws IOException {
        return (IMarkerSymbol) toArcObject().get(0);
    }
}
