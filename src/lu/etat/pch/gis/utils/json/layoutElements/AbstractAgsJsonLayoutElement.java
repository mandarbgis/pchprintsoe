/*
 *
 *  *  PCHPrintSOE - Advanced printing SOE for ArcGIS Server
 *  *  Copyright (C) 2010-2012 Tom Schuller
 *  *
 *  *  This program is free software: you can redistribute it and/or modify
 *  *  it under the terms of the GNU Lesser General Public License as published by
 *  *  the Free Software Foundation, either version 3 of the License, or
 *  *  (at your option) any later version.
 *  *
 *  *  This program is distributed in the hope that it will be useful,
 *  *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  *  GNU Lesser General Public License for more details.
 *  *
 *  *  You should have received a copy of the GNU Lesser General Public License
 *  *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package lu.etat.pch.gis.utils.json.layoutElements;

import com.esri.arcgis.carto.IElement;
import com.esri.arcgis.carto.IMapFrame;
import com.esri.arcgis.carto.PageLayout;
import com.esri.arcgis.geometry.IGeometry;
import com.esri.arcgis.server.json.JSONException;
import com.esri.arcgis.server.json.JSONObject;
import lu.etat.pch.gis.utils.LayoutUtils;
import lu.etat.pch.gis.utils.SOELogger;
import lu.etat.pch.gis.utils.json.graphics.AgsJsonTextSymbol;

import java.io.IOException;

/**
 * Created by IntelliJ IDEA.
 * User: schullto
 * Date: 4/2/11
 * Time: 10:25 AM
 */
public abstract class AbstractAgsJsonLayoutElement {
    private static final String TAG = "AbstractAgsJsonLayoutElement";
    protected LayoutUtils layoutUtils = new LayoutUtils();
    protected String type;
    protected SOELogger logger;

    protected AbstractAgsJsonLayoutElement(String type, SOELogger logger) {
        this.type = type;
        this.logger = logger;
    }

    public static AbstractAgsJsonLayoutElement getFromJson(SOELogger logger, JSONObject jsonObject) {
        if (jsonObject.isNull("type")) {
            logger.error(TAG, "AbstractAgsJsonLayoutElement.getFromJson.ERROR no type defined: ", jsonObject.toString());
            return null;
        }
        String type = jsonObject.getString("type");
        AbstractAgsJsonLayoutElement layoutElement = null;
        if (type.equalsIgnoreCase(AgsJsonScaleText.TYPE)) {
            layoutElement = new AgsJsonScaleText(logger, jsonObject);
        } else if (type.equalsIgnoreCase(AgsJsonScaleBar.TYPE)) {
            layoutElement = new AgsJsonScaleBar(logger, jsonObject);
        } else if (type.equalsIgnoreCase(AgsJsonLegend.TYPE)) {
            layoutElement = new AgsJsonLegend(logger, jsonObject);
        } else if (type.equalsIgnoreCase(AgsJsonNorthArrow.TYPE)) {
            layoutElement = new AgsJsonNorthArrow(logger, jsonObject);
        } else if (type.equalsIgnoreCase(AgsJsonPictureElement.TYPE)) {
            layoutElement = new AgsJsonPictureElement(logger, jsonObject);
        } else if (type.equalsIgnoreCase(AgsJsonTextElement.TYPE) || type.equalsIgnoreCase(AgsJsonTextSymbol.TYPE)) {
            layoutElement = new AgsJsonTextElement(logger, jsonObject);
        } else if (type.equalsIgnoreCase(AgsJsonMapFrame.TYPE)) {
            layoutElement = new AgsJsonMapFrame(logger, jsonObject);
        }
        return layoutElement;
    }


    public abstract JSONObject toJSON() throws JSONException;

    public abstract String getType();

    public abstract IElement toArcObject(PageLayout pageLayout, IMapFrame mapFrame, IGeometry geom) throws IOException;
}
