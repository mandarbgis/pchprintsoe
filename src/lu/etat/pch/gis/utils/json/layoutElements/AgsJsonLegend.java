/*
 *
 *  *  PCHPrintSOE - Advanced printing SOE for ArcGIS Server
 *  *  Copyright (C) 2010-2012 Tom Schuller
 *  *
 *  *  This program is free software: you can redistribute it and/or modify
 *  *  it under the terms of the GNU Lesser General Public License as published by
 *  *  the Free Software Foundation, either version 3 of the License, or
 *  *  (at your option) any later version.
 *  *
 *  *  This program is distributed in the hope that it will be useful,
 *  *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  *  GNU Lesser General Public License for more details.
 *  *
 *  *  You should have received a copy of the GNU Lesser General Public License
 *  *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package lu.etat.pch.gis.utils.json.layoutElements;

import com.esri.arcgis.carto.*;
import com.esri.arcgis.display.IFillSymbol;
import com.esri.arcgis.display.ITextSymbol;
import com.esri.arcgis.display.TextSymbol;
import com.esri.arcgis.geometry.IGeometry;
import com.esri.arcgis.interop.AutomationException;
import com.esri.arcgis.server.json.JSONArray;
import com.esri.arcgis.server.json.JSONException;
import com.esri.arcgis.server.json.JSONObject;
import com.esri.arcgis.system.IUID;
import com.esri.arcgis.system.UID;
import lu.etat.pch.gis.utils.SOELogger;
import lu.etat.pch.gis.utils.json.format.AgsJsonLegendClassFormat;
import lu.etat.pch.gis.utils.json.format.AgsJsonLegendFormat;
import lu.etat.pch.gis.utils.json.graphics.AgsJsonSimpleFillSymbol;
import lu.etat.pch.gis.utils.json.graphics.AgsJsonTextSymbol;

import java.io.IOException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: schullto
 * Date: Jul 28, 2010
 * Time: 8:36:58 AM
 */
public class AgsJsonLegend extends AbstractAgsJsonLayoutElement {
    private static final String TAG = "AgsJsonLegend";
    public static final String TYPE = "pchLegend";
    private String legendTitle = "Dynamic legend:";
    private int legendColumns = 1;
    private boolean includeAllLayers = true;
    private String[] exceptionLayerNames = new String[0];
    private boolean showLayerNames = false;
    private boolean showLabels = true;
    private boolean showHeadings = false;
    private AgsJsonSimpleFillSymbol legendBackground;
    private AgsJsonLegendFormat legendFormat;
    private boolean rightToLeft = false;
    private AgsJsonLegendClassFormat defaultLegendClassFormat;
    private AgsJsonTextSymbol defaultHeadingSymbol;
    private AgsJsonTextSymbol defaultLayerNameSymbol;

    public AgsJsonLegend(SOELogger logger) {
        super(TYPE, logger);
    }

    public AgsJsonLegend(SOELogger logger, String legendTitle, int legendColumns, boolean includeAllLayers, String[] exceptionLayerNames, boolean showLayerNames, boolean showLabels, boolean showHeadings) {
        super(TYPE, logger);
        this.legendTitle = legendTitle;
        this.legendColumns = legendColumns;
        this.includeAllLayers = includeAllLayers;
        this.exceptionLayerNames = exceptionLayerNames;
        this.showLayerNames = showLayerNames;
        this.showLabels = showLabels;
        this.showHeadings = showHeadings;
    }

    public AgsJsonLegend(SOELogger logger, JSONObject jsonObj) {
        super(TYPE, logger);
        try {
            if (!jsonObj.isNull("legendTitle")) legendTitle = jsonObj.getString("legendTitle");
            if (!jsonObj.isNull("legendColumns")) legendColumns = jsonObj.getInt("legendColumns");
            if (!jsonObj.isNull("includeAllLayers")) includeAllLayers = jsonObj.getBoolean("includeAllLayers");
            if (!jsonObj.isNull("exceptionLayerNames")) {
                JSONArray tmpArray = jsonObj.getJSONArray("exceptionLayerNames");
                exceptionLayerNames = new String[tmpArray.length()];
                for (int i = 0; i < exceptionLayerNames.length; i++) {
                    exceptionLayerNames[i] = tmpArray.getString(i);
                }
            }
            if (!jsonObj.isNull("showLayerNames")) showLayerNames = jsonObj.getBoolean("showLayerNames");
            if (!jsonObj.isNull("showLabels")) showLabels = jsonObj.getBoolean("showLabels");
            if (!jsonObj.isNull("showHeadings")) showHeadings = jsonObj.getBoolean("showHeadings");
            if (!jsonObj.isNull("legendBackground"))
                legendBackground = new AgsJsonSimpleFillSymbol(logger, jsonObj.getJSONObject("legendBackground"));
            if (!jsonObj.isNull("legendFormat"))
                legendFormat = new AgsJsonLegendFormat(logger, jsonObj.getJSONObject("legendFormat"));
            if (!jsonObj.isNull("rightToLeft")) rightToLeft = jsonObj.getBoolean("rightToLeft");
            if (!jsonObj.isNull("defaultLegendClassFormat"))
                defaultLegendClassFormat = new AgsJsonLegendClassFormat(logger, jsonObj.getJSONObject("defaultLegendClassFormat"));
            if (!jsonObj.isNull("defaultHeadingSymbol")) {
                defaultHeadingSymbol = new AgsJsonTextSymbol(logger, jsonObj.getJSONObject("defaultHeadingSymbol"));
            }
            if (!jsonObj.isNull("defaultLayerNameSymbol")) {
                defaultLayerNameSymbol = new AgsJsonTextSymbol(logger, jsonObj.getJSONObject("defaultLayerNameSymbol"));
            }
        } catch (JSONException e) {
            logger.error(TAG, "AgsJsonLegend()", e);
        }
    }

    public String getLegendTitle() {
        return legendTitle;
    }

    public void setLegendTitle(String legendTitle) {
        this.legendTitle = legendTitle;
    }

    public boolean isIncludeAllLayers() {
        return includeAllLayers;
    }

    public void setIncludeAllLayers(boolean includeAllLayers) {
        this.includeAllLayers = includeAllLayers;
    }

    public String[] getExceptionLayerNames() {
        return exceptionLayerNames;
    }

    public void setExceptionLayerNames(String[] exceptionLayerNames) {
        this.exceptionLayerNames = exceptionLayerNames;
    }

    public boolean isShowLayerNames() {
        return showLayerNames;
    }

    public void setShowLayerNames(boolean showLayerNames) {
        this.showLayerNames = showLayerNames;
    }

    public boolean isShowLabels() {
        return showLabels;
    }

    public void setShowLabels(boolean showLabels) {
        this.showLabels = showLabels;
    }

    public boolean isShowHeadings() {
        return showHeadings;
    }

    public void setShowHeadings(boolean showHeadings) {
        this.showHeadings = showHeadings;
    }

    @Override
    public JSONObject toJSON() throws JSONException {
        JSONObject symbObject = new JSONObject();
        symbObject.put("type", type);
        symbObject.put("legendTitle", legendTitle);
        symbObject.put("legendColumns", legendColumns);
        symbObject.put("includeAllLayers", includeAllLayers);
        JSONArray execpionLayerNamesJsonArray = new JSONArray();
        for (String exceptionLayerName : exceptionLayerNames) {
            execpionLayerNamesJsonArray.put(exceptionLayerName);
        }
        symbObject.put("exceptionLayerNames", execpionLayerNamesJsonArray);
        symbObject.put("showLayerNames", showLayerNames);
        symbObject.put("showLabels", showLabels);
        symbObject.put("showHeadings", showHeadings);
        if (legendBackground != null) {
            symbObject.put("legendBackground", legendBackground.toJSON());
        }
        if (legendFormat != null) {
            symbObject.put("legendFormat", legendFormat.toJSON());
        }
        if (defaultLegendClassFormat != null) {
            symbObject.put("defaultLegendClassFormat", defaultLegendClassFormat.toJSON());
        }
        if (defaultHeadingSymbol != null) {
            symbObject.put("defaultHeadingSymbol", defaultHeadingSymbol.toJSON());
        }
        if (defaultLayerNameSymbol != null) {
            symbObject.put("defaultLayerNameSymbol", defaultLayerNameSymbol.toJSON());
        }
        return symbObject;
    }

    @Override
    public IElement toArcObject(PageLayout pageLayout, IMapFrame mapFrame, IGeometry geom) throws IOException {
        logger.debug(TAG, "AgsJsonElement.toArcObject...");
        List<String> exceptionLayerNamesList = new ArrayList<String>(Arrays.asList(exceptionLayerNames));

        //Create Legend
        IUID uidLegend = new UID();
        uidLegend.setValue(Legend.getClsid());

        Map map = (Map) mapFrame.getMap();

        MapSurroundFrame legendMapSurroundFrame = (MapSurroundFrame) mapFrame.createSurroundFrame(uidLegend, null);
        Legend legend = (Legend) legendMapSurroundFrame.getMapSurround();

        legend.setTitle(legendTitle);
        legend.setRightToLeft(rightToLeft);
        if (legendFormat != null) {
            legend.setFormatByRef(legendFormat.toArcObjects(legend));
        }
        legend.setMapByRef(map);

        legend.clearItems();

        List<ComparableLayer> addedLayersOnLegend = new ArrayList<ComparableLayer>();
        for (int i = 0; i < map.getLayerCount(); i++) {
            ILayer layer = map.getLayer(i);
            logger.debug(TAG, "AgsJsonLegend.toArcObject.layerLoop.layer: " + layer.getName() + " [" + layer.getClass() + "]");
            addLayerToLegend(layer, legend, addedLayersOnLegend, exceptionLayerNamesList);
        }
        logger.debug(TAG, "AgsJsonLegend.toArcObject.adjustColumns.legendColumns", legendColumns);
        legend.adjustColumns(legendColumns);
        if (legendBackground != null) {
            logger.debug(TAG, "AgsJsonLegend.toArcObject.legendBackground", legendBackground);
            SymbolBackground symbolBackground = new SymbolBackground();
            symbolBackground.setFillSymbol((IFillSymbol) legendBackground.toArcObject().get(0));
            legendMapSurroundFrame.setBackground(symbolBackground);
        }
        logger.debug(TAG, "AgsJsonLegend.toArcObject.END");
        return legendMapSurroundFrame;

        /*
Private Sub RefreshLegend(ByRef pElement As IElement, pAV As IActiveView, bRedraw As Boolean)
    Dim pMSF As IMapSurroundFrame
    Dim pMS As IMapSurround
    Dim pLegend As ILegend
    Dim pNewEnv As IEnvelope

    Set pMSF = pElement
    Set pLegend = pMSF.MapSurround
    Set pNewEnv = New Envelope
    Set pElement = pMSF
    Set pMS = pMSF.MapSurround
    pMS.Refresh
    pLegend.QueryBounds pAV.ScreenDisplay, pElement.Geometry.Envelope, pNewEnv
    'Assign the boundary to the map surround frame
    If bRedraw Then
      pAV.PartialRefresh esriViewGraphics, pElement, Nothing
      pElement.Draw pAV.ScreenDisplay, Nothing
    End If
    pElement.Geometry = pNewEnv

    pMS.FitToBounds pAV.ScreenDisplay, pElement.Geometry.Envelope, True
    pMS.Refresh
    If bRedraw Then
      pAV.PartialRefresh esriViewGraphics, pElement, Nothing
    End If
End Sub
         */
    }

    @Override
    public String getType() {
        return TYPE;
    }

    private void addLayerToLegend(ILayer layer, Legend legend, List<ComparableLayer> addedLayersOnLegend, List<String> exceptionLayerNamesList) {
        try {
            if (!layer.isVisible()) {
                logger.error(TAG, "addLayerToLegend[" + layer.getName() + "].isInVisible.");
                return;
            }
        } catch (IOException e) {
            logger.error(TAG, "addLayerToLegend.isVisible.IOException", e);
            return;
        }
        if (layer instanceof ICompositeLayer) {
            ICompositeLayer compositeLayer = (ICompositeLayer) layer;
            try {
                for (int i = 0; i < compositeLayer.getCount(); i++) {
                    ILayer subCompositeLayer = compositeLayer.getLayer(i);
                    logger.debug(TAG, "AgsJsonLegend.toArcObject.layerLoop.subCompositeLayer", subCompositeLayer);
                    addLayerToLegend(subCompositeLayer, legend, addedLayersOnLegend, exceptionLayerNamesList);
                }
            } catch (IOException e) {
                logger.error(TAG, "addLayerToLegend.ICompositeLayer", e);
            }
        } else if (layer instanceof MapServerIdentifySublayer) {
            MapServerIdentifySublayer mapServerIdentifySublayer = (MapServerIdentifySublayer) layer;
            try {
                if (mapServerIdentifySublayer.getCount() > 0) {
                    for (int i = 0; i < mapServerIdentifySublayer.getCount(); i++) {
                        ILayer subMapServerIdentifySublayer = mapServerIdentifySublayer.getLayer(i);
                        logger.debug(TAG, "AgsJsonLegend.toArcObject.layerLoop.subMapServerIdentifySublayer", subMapServerIdentifySublayer);
                        addLayerToLegend(subMapServerIdentifySublayer, legend, addedLayersOnLegend, exceptionLayerNamesList);
                    }
                } else {
                    putLayerIntoLegend(layer, legend, addedLayersOnLegend, exceptionLayerNamesList);
                }

            } catch (IOException e) {
                logger.error(TAG, "addLayerToLegend.MapServerIdentifySublayer", e);
            }
        } else {
            putLayerIntoLegend(layer, legend, addedLayersOnLegend, exceptionLayerNamesList);
        }
        logger.debug(TAG, "AgsJsonLegend.addLayerToLegend.DONE");
    }

    private void putLayerIntoLegend(ILayer layer, Legend legend, List<ComparableLayer> addedLayersOnLegend, List<String> exceptionLayerNamesList) {
        try {
            String layerName = layer.getName();
            if (layerName.length() > 12) {
                String cuttedLayerName = layerName.substring(0, 12).trim();
                logger.debug(TAG, "AgsJsonLegend.putLayerIntoLegend.cuttedLayerName", cuttedLayerName);
                layer.setName(cuttedLayerName);
            }
            boolean shouldBeAdded = includeAllLayers;
            if (exceptionLayerNamesList.contains(layerName)) shouldBeAdded = !shouldBeAdded;
            if (layerName.contains("Bing Maps ")) shouldBeAdded = false;
            if (layer instanceof WMSMapLayer) shouldBeAdded = false;
            logger.debug(TAG, "AgsJsonLegend.putLayerIntoLegend.layerName '" + layerName + "' shouldBeAdded=" + shouldBeAdded);
            if (shouldBeAdded) {
                ComparableLayer compLayer = new ComparableLayer(layer);
                if (!addedLayersOnLegend.contains(compLayer)) {
                    try {
                        HorizontalLegendItem horiLegendItem = new HorizontalLegendItem();
                        horiLegendItem.setLayerByRef(layer);
                        horiLegendItem.setShowLayerName(showLayerNames);
                        horiLegendItem.setShowLabels(showLabels);
                        horiLegendItem.setShowHeading(showHeadings);
                        if (defaultLayerNameSymbol != null) {
                            ITextSymbol layerNameSymbol = horiLegendItem.getLayerNameSymbol();
                            if (layerNameSymbol instanceof TextSymbol)
                                horiLegendItem.setLayerNameSymbol((ITextSymbol) defaultLayerNameSymbol.toArcObject((TextSymbol) layerNameSymbol));
                        }
                        if (defaultHeadingSymbol != null) {
                            horiLegendItem.setHeadingSymbol((ITextSymbol) defaultHeadingSymbol.toArcObject().get(0));
                        }
                        if (defaultLegendClassFormat != null) {
                            ILegendClassFormat legendClassFormat = horiLegendItem.getLegendClassFormat();
                            legendClassFormat = defaultLegendClassFormat.toArcObjects(legendClassFormat);
                            horiLegendItem.setLegendClassFormat(legendClassFormat);
                        }

                        addedLayersOnLegend.add(compLayer);
                        legend.addItem(horiLegendItem);
                        logger.debug(TAG, "AgsJsonLegend.putLayerIntoLegend.added.layerName", layerName);
                    } catch (AutomationException e) {
                        logger.error(TAG, "putLayerIntoLegend.AutomationException", e);
                    } catch (UnknownHostException e) {
                        logger.error(TAG, "putLayerIntoLegend.UnknownHostException", e);
                    } catch (IOException e) {
                        logger.error(TAG, "putLayerIntoLegend.IOException", e);
                    }
                } else {
                    logger.debug(TAG, "AgsJsonLegend.putLayerIntoLegend.NOT.added.alreadyIn.layerName", layer);
                }
            } else {
                logger.debug(TAG, "AgsJsonLegend.putLayerIntoLegend.exceptionLayerFound.layerName", layerName);
            }
            layer.setName(layerName);
        } catch (AutomationException e) {
            logger.error(TAG, "putLayerIntoLegend.AutomationException", e);
        } catch (IOException e) {
            logger.error(TAG, "putLayerIntoLegend.IOException ", e);
        }
    }

    private class ComparableLayer {
        private ILayer layer;

        private ComparableLayer(ILayer layer) {
            this.layer = layer;
        }

        @Override
        public boolean equals(Object obj) {
            if (obj instanceof ComparableLayer) {
                ComparableLayer compLayer2 = (ComparableLayer) obj;
                obj = compLayer2.layer;
            }
            if (obj instanceof ILayer) {
                ILayer layer2 = (ILayer) obj;
                try {
                    if (!layer.getClass().getCanonicalName().equals(layer2.getClass().getCanonicalName()))
                        return false;
                    if (!layer.getName().equals(layer2.getName()))
                        return false;
                    if (this instanceof ICompositeLayer && layer instanceof ICompositeLayer) {
                        ICompositeLayer compLayer1 = (ICompositeLayer) this;
                        ICompositeLayer compLayer2 = (ICompositeLayer) layer;
                        logger.debug(TAG, "AgsJsonLegend.ComparableLayer.equals.compLayer1", compLayer1);
                        logger.debug(TAG, "AgsJsonLegend.ComparableLayer.equals.compLayer2", compLayer2);
                        if (compLayer1.getCount() != compLayer2.getCount()) return false;
                        for (int i = 0; i < compLayer1.getCount(); i++) {
                            ILayer subLayer1 = compLayer1.getLayer(i);
                            ILayer subLayer2 = compLayer2.getLayer(i);
                            if (subLayer1.isVisible() != subLayer2.isVisible()) return false;
                            if (!subLayer1.getName().equals(subLayer2.getName())) return false;
                            logger.debug(TAG, "AgsJsonLegend.ComparableLayer.equals.subLayer1", subLayer1);
                            logger.debug(TAG, "AgsJsonLegend.ComparableLayer.equals.subLayer2", subLayer2);
                        }

                    }
                    logger.debug(TAG, "AgsJsonLegend.ComparableLayer.equals.layer1", this);
                    logger.debug(TAG, "AgsJsonLegend.ComparableLayer.equals.layer2", layer);
                    return true;
                } catch (AutomationException e) {
                    logger.error(TAG, "ComparableLayer.AutomationException", e);
                } catch (IOException e) {
                    logger.error(TAG, "ComparableLayer.IOException", e);
                }
            }
            logger.debug(TAG, "AgsJsonLegend.ComparableLayer.NOTequals.layer1", this);
            logger.debug(TAG, "AgsJsonLegend.ComparableLayer.NOTequals.layer2", layer);
            return false;
        }
    }
}