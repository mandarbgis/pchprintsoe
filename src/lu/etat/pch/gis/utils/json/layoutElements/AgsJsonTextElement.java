/*
 *
 *  *  PCHPrintSOE - Advanced printing SOE for ArcGIS Server
 *  *  Copyright (C) 2010-2012 Tom Schuller
 *  *
 *  *  This program is free software: you can redistribute it and/or modify
 *  *  it under the terms of the GNU Lesser General Public License as published by
 *  *  the Free Software Foundation, either version 3 of the License, or
 *  *  (at your option) any later version.
 *  *
 *  *  This program is distributed in the hope that it will be useful,
 *  *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  *  GNU Lesser General Public License for more details.
 *  *
 *  *  You should have received a copy of the GNU Lesser General Public License
 *  *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package lu.etat.pch.gis.utils.json.layoutElements;

import com.esri.arcgis.carto.*;
import com.esri.arcgis.display.ITextSymbol;
import com.esri.arcgis.geometry.Envelope;
import com.esri.arcgis.geometry.IGeometry;
import com.esri.arcgis.geometry.Point;
import com.esri.arcgis.geometry.Polygon;
import com.esri.arcgis.server.json.JSONException;
import com.esri.arcgis.server.json.JSONObject;
import lu.etat.pch.gis.utils.SOELogger;
import lu.etat.pch.gis.utils.json.graphics.AgsJsonTextSymbol;

import java.io.IOException;

/**
 * Created by IntelliJ IDEA.
 * User: schullto
 * Date: 4/1/11
 * Time: 4:22 PM
 */
public class AgsJsonTextElement extends AbstractAgsJsonLayoutElement {
    private static final String TAG = "AgsJsonTextElement";
    public static final String TYPE = "pchTextElement";
    private String text;
    private AgsJsonTextSymbol textSymbol;

    public AgsJsonTextElement(SOELogger logger, String text) {
        super(TYPE, logger);
        this.text = text;
    }

    public AgsJsonTextElement(SOELogger logger, JSONObject jsonObj) {
        super(TYPE, logger);
        try {
            if (!jsonObj.isNull("text")) this.text = jsonObj.getString("text");
            if (!jsonObj.isNull("type") && jsonObj.getString("type").equalsIgnoreCase("esriTS"))
                this.textSymbol = new AgsJsonTextSymbol(logger, jsonObj);
            else if (!jsonObj.isNull("textSymbol"))
                this.textSymbol = new AgsJsonTextSymbol(logger, jsonObj.getJSONObject("textSymbol"));
        } catch (JSONException e) {
            logger.error(TAG, "AgsJsonTextElement(JSONObject)", e);
        }
    }


    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public AgsJsonTextSymbol getTextSymbol() {
        return textSymbol;
    }

    public void setTextSymbol(AgsJsonTextSymbol textSymbol) {
        this.textSymbol = textSymbol;
    }

    @Override
    public JSONObject toJSON() throws JSONException {
        JSONObject symbObject = new JSONObject();
        symbObject.put("type", type);
        symbObject.put("text", text);
        if (textSymbol != null) symbObject.put("textSymbol", textSymbol);
        return symbObject;
    }

    @Override
    public IElement toArcObject(PageLayout pageLayout, IMapFrame mapFrame, IGeometry geom) throws IOException {
        if (geom instanceof Envelope || geom instanceof Polygon) {
            ParagraphTextElement paragraphTextElement = new ParagraphTextElement();
            paragraphTextElement.setText(text);
            if (textSymbol != null) {
                paragraphTextElement.setSymbol((ITextSymbol) textSymbol.toArcObject().get(0));
                paragraphTextElement.setColor(paragraphTextElement.getSymbol().getColor());
                paragraphTextElement.setFontName(paragraphTextElement.getSymbol().getFont().getName());
            }
            paragraphTextElement.setGeometry(geom);
            return paragraphTextElement;
        } else if (geom instanceof Point) {
            TextElement textElement = new TextElement();
            textElement.setText(text);
            if (textSymbol != null) {
                textElement.setSymbol((ITextSymbol) textSymbol.toArcObject().get(0));
                textElement.setColor(textElement.getSymbol().getColor());
                textElement.setFontName(textElement.getSymbol().getFont().getName());
            }
            textElement.setGeometry(geom);
            return textElement;
        } else {
            logger.error(TAG, "AgsJsonTextElement.toArcObject.invalidGeometry", geom);
            return null;
        }
    }

    @Override
    public String getType() {
        return TYPE;
    }
}
