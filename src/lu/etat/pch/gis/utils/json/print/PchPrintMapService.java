/*
 *
 *  *  PCHPrintSOE - Advanced printing SOE for ArcGIS Server
 *  *  Copyright (C) 2010-2012 Tom Schuller
 *  *
 *  *  This program is free software: you can redistribute it and/or modify
 *  *  it under the terms of the GNU Lesser General Public License as published by
 *  *  the Free Software Foundation, either version 3 of the License, or
 *  *  (at your option) any later version.
 *  *
 *  *  This program is distributed in the hope that it will be useful,
 *  *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  *  GNU Lesser General Public License for more details.
 *  *
 *  *  You should have received a copy of the GNU Lesser General Public License
 *  *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package lu.etat.pch.gis.utils.json.print;

import com.esri.arcgis.server.json.JSONArray;
import com.esri.arcgis.server.json.JSONException;
import com.esri.arcgis.server.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: schullto
 * Date: Mar 9, 2010
 * Time: 5:21:42 PM
 */
public class PchPrintMapService {
    private static final String TAG = PchPrintMapService.class.getName();
    public static String pchPrintSOEname = "/exts/PCHPrintSOE";
    public static final String TYPE_AGS_PCHPRINTSOE = "PCHPrintSOE";
    public static final String TYPE_AGS_MAPSERVICE = "MapService";
    static HashMap<String, String> mapServiceTypeMap = new HashMap<String, String>();


    private String url;
    private String type; //AGS, WMS, ...
    private String name;
    private String visibleIds;  // "*" or "0,1,5,7"
    private Double alpha = Double.valueOf(1d);
    private List<String> definitionExpression;
    private String token;
    private String imageFormat;
    private String proxyURL;

    public PchPrintMapService(String url, String type, String name, String visibleIds) {
        this.url = url;
        this.type = type;
        this.name = name;
        this.visibleIds = visibleIds;
        this.alpha = Double.valueOf(1d);
    }

    public PchPrintMapService(String url, String type, String name, String visibleIds, Double alpha) {
        this.url = url;
        this.type = type;
        this.name = name;
        this.visibleIds = visibleIds;
        this.alpha = alpha;
    }

    public PchPrintMapService(JSONObject msObject) throws JSONException {
        if (!msObject.isNull("url")) url = msObject.getString("url");
        if (!msObject.isNull("name")) name = msObject.getString("name");
        if (!msObject.isNull("type")) type = msObject.getString("type");
        if (!msObject.isNull("visibleIds")) visibleIds = msObject.getString("visibleIds");
        if (!msObject.isNull("alpha")) alpha = Double.valueOf(msObject.getDouble("alpha"));
        if (!msObject.isNull("definitionExpression")) {
            definitionExpression = new ArrayList<String>();
            Object defExp = msObject.get("definitionExpression");
            if (defExp instanceof JSONArray) {
                JSONArray defExpArray = (JSONArray) defExp;
                for (int i = 0; i < defExpArray.length(); i++) {
                    if (defExpArray.isNull(i)) definitionExpression.add(null);
                    else definitionExpression.add(defExpArray.getString(i));
                }
            } else {
                definitionExpression.add(defExp.toString());
            }
        }
        if (!msObject.isNull("token")) token = msObject.getString("token");
        if (!msObject.isNull("imageFormat")) imageFormat = msObject.getString("imageFormat");
        if (!msObject.isNull("proxyURL")) proxyURL = msObject.getString("proxyURL");
    }

    public String getUrl() {
        return url;
    }

    public String getType() {
        return type;
    }

    public String getName() {
        return name;
    }

    public String getVisibleIds() {
        return visibleIds;
    }

    public Double getAlpha() {
        return alpha;
    }

    public List<String> getDefinitionExpression() {
        return definitionExpression;
    }

    public void setDefinitionExpression(List<String> definitionExpression) {
        this.definitionExpression = definitionExpression;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getImageFormat() {
        return imageFormat;
    }

    public void setImageFormat(String imageFormat) {
        this.imageFormat = imageFormat;
    }

    public String getProxyURL() {
        return proxyURL;
    }

    public void setProxyURL(String proxyURL) {
        this.proxyURL = proxyURL;
    }

    public JSONObject toJSON() throws JSONException {
        JSONObject jsonObj = new JSONObject();
        jsonObj.put("url", url);
        jsonObj.put("type", type);
        jsonObj.put("name", name);
        jsonObj.put("visibleIds", visibleIds);
        jsonObj.put("alpha", alpha);
        if (definitionExpression != null) jsonObj.put("definitionExpression", definitionExpression);
        if (token != null && token.length() > 0)
            jsonObj.put("token", token);
        return jsonObj;
    }

    @Override
    public String toString() {
        return "PchPrintMapService{" + "url=" + url + ", type=" + type + ", name=" + name + ", visibleIds=" + visibleIds + ", alpha=" + alpha + ", definitionExpression=" + definitionExpression + '}';
    }
}
